<?php
class App {
    function __construct(){
        global $req, $body, $header, $file, $files;

        fwrite($file, $req['method']." -> ".$_SERVER['REMOTE_ADDR']."\n");
        fwrite($file, "BODY: ".json_encode($body)."\n");
        fwrite($file, "REQUEST: ".json_encode($req)."\n");
        fwrite($file, "FILES: ".json_encode($files)."\n");
        fwrite($file, "HEADERS: ".json_encode($header)."\n");

        switch($req['method']){

            case 'cronBilling' : new cronBilling(); break;
            case 'cronNotification' : new cronNotification(); break;

            case 'allRegions' : new allRegions(); break;
            case 'regions' : new regions(); break;
            case 'districts' : new districts(); break;
            case 'quarters' : new quarters(); break;
            case 'tariffs' : new tariffs(); break;
            case 'share' : new share(); break;

            case 'adminLogin' : new adminLogin(); break;
            case 'adminAllInfo' : new adminAllInfo(); break;
            case 'adminUsers' : new adminUsers(); break;
            case 'adminDeleteUser' : new adminDeleteUser(); break;
            case 'adminLicenseUser' : new adminLicenseUser(); break;
            case 'adminCanOwnerUser' : new adminCanOwnerUser(); break;
            case 'adminUnCanOwnerUser' : new adminUnCanOwnerUser(); break;
            case 'adminSetUserCompany' : new adminSetUserCompany(); break;
            case 'adminCanAddFromOwner' : new adminCanAddFromOwner(); break;
            case 'adminUnCanAddFromOwner' : new adminUnCanAddFromOwner(); break;
            case 'adminSetSettings' : new adminSetSettings(); break;
            case 'adminSettings' : new adminSettings(); break;
            case 'adminUnLicenseUser' : new adminUnLicenseUser(); break;
            case 'adminActiveUser' : new adminActiveUser(); break;
            case 'adminBlockUser' : new adminBlockUser(); break;
            case 'adminAddNotificationUser' : new adminAddNotificationUser(); break;
            case 'adminUserTransactions' : new adminUserTransactions(); break;
            case 'adminUserTransfer' : new adminUserTransfer(); break;
            case 'adminTariffs' : new adminTariffs(); break;
            case 'adminAddTariff' : new adminAddTariff(); break;
            case 'adminEditTariff' : new adminEditTariff(); break;
            case 'adminDeleteTariff' : new adminDeleteTariff(); break;
            case 'adminSendSMS' : new adminSendSMS(); break;
            case 'adminBalanceSMS' : new adminBalanceSMS(); break;
            case 'adminRegions' : new adminRegions(); break;
            case 'adminAddRegion' : new adminAddRegion(); break;
            case 'adminEditRegion' : new adminEditRegion(); break;
            case 'adminDeleteRegion' : new adminDeleteRegion(); break;
            case 'adminUndergrounds' : new adminUndergrounds(); break;
            case 'adminAddUnderground' : new adminAddUnderground(); break;
            case 'adminEditUnderground' : new adminEditUnderground(); break;
            case 'adminDeleteUnderground' : new adminDeleteUnderground(); break;
            case 'adminCompanies' : new adminCompanies(); break;
            case 'adminAddCompany' : new adminAddCompany(); break;
            case 'adminEditCompany' : new adminEditCompany(); break;
            case 'adminDeleteCompany' : new adminDeleteCompany(); break;
            case 'adminDistricts' : new adminDistricts(); break;
            case 'adminAddDistrict' : new adminAddDistrict(); break;
            case 'adminEditDistrict' : new adminEditDistrict(); break;
            case 'adminDeleteDistrict' : new adminDeleteDistrict(); break;
            case 'adminQuarters' : new adminQuarters(); break;
            case 'adminAddQuarter' : new adminAddQuarter(); break;
            case 'adminEditQuarter' : new adminEditQuarter(); break;
            case 'adminDeleteQuarter' : new adminDeleteQuarter(); break;
            case 'adminEstates' : new adminEstates(); break;
            case 'adminShowEstate' : new adminShowEstate(); break;
            case 'adminHideEstate' : new adminHideEstate(); break;
            case 'adminDeleteEstate' : new adminDeleteEstate(); break;
            case 'adminDeleteToken' : new adminDeleteToken(); break;
            case 'adminTokens' : new adminTokens(); break;
            case 'adminClients' : new adminClients(); break;
            case 'adminWhitelists' : new adminWhitelists(); break;
            case 'adminAddWhitelist' : new adminAddWhitelist(); break;
            case 'adminDeleteWhitelist' : new adminDeleteWhitelist(); break;
            case 'adminRequests' : new adminRequests(); break;
            case 'adminDeleteRequest' : new adminDeleteRequest(); break;

            case 'userCheckToken' : new userCheckToken(); break;
            case 'userCheckClientPhone' : new userCheckClientPhone(); break;
            case 'userLogout' : new userLogout(); break;
            case 'userLogin' : new userLogin(); break;
            case 'userVerify' : new userVerify(); break;
            case 'userEdit' : new userEdit(); break;
            case 'userDetails' : new userDetails(); break;
            case 'userShortDetails' : new userShortDetails(); break;
            case 'userFileUpload' : new userFileUpload(); break;
            case 'userRequests' : new userRequests(); break;
            case 'userFastRequests' : new userFastRequests(); break;
            case 'userFastRequest' : new userFastRequest(); break;
            case 'userAddFastRequest' : new userAddFastRequest(); break;
            case 'userDelFastRequest' : new userDelFastRequest(); break;
            case 'userAddClient' : new userAddClient(); break;
            case 'userEditClient' : new userEditClient(); break;
            case 'userDelClient' : new userDelClient(); break;
            case 'userClients' : new userClients(); break;
            case 'userClient' : new userClient(); break;
            case 'userEditClientRating' : new userEditClientRating(); break;
            case 'userEstates' : new userEstates(); break;
            case 'userOtherEstates' : new userOtherEstates(); break;
            case 'userEstateSetStatus' : new userEstateSetStatus(); break;
            case 'userEstate' : new userEstate(); break;
            case 'userEstateAddCustomer' : new userEstateAddCustomer(); break;
            case 'userEstateFavourite' : new userEstateFavourite(); break;
            case 'userEstateDelCustomer' : new userEstateDelCustomer(); break;
            case 'userEstateShare' : new userEstateShare(); break;
            case 'userEstateClearCustomer' : new userEstateClearCustomer(); break;
            case 'userOtherEstate' : new userOtherEstate(); break;
            case 'userAddEstate' : new userAddEstate(); break;
            case 'userEditEstate' : new userEditEstate(); break;
            case 'userNotification' : new userNotification(); break;
            case 'userNotifications' : new userNotifications(); break;
            case 'userPublicInfo' : new userPublicInfo(); break;
            case 'userSetFriend' : new userSetFriend(); break;
            case 'userGetFriend' : new userGetFriend(); break;
            case 'userDelFriend' : new userDelFriend(); break;
            case 'userFriends' : new userFriends(); break;
            case 'userFriend' : new userFriend(); break;
            case 'userSetTariff' : new userSetTariff(); break;
            case 'userStatistics' : new userStatistics(); break;

            case 'clientCheckToken' : new clientCheckToken(); break;
            case 'clientLogin' : new clientLogin(); break;
            case 'clientVerify' : new clientVerify(); break;
            case 'clientEdit' : new clientEdit(); break;
            case 'clientAddRequest' : new clientAddRequest(); break;
            case 'clientRequests' : new clientRequests(); break;

            default: new Errors('метод не найден');
        }

    }
}
