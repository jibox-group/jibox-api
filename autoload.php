<?php

$files = [];

$files = array_merge($files, glob($_SERVER['DOCUMENT_ROOT'].'/classes/*.php'));
$files = array_merge($files, glob($_SERVER['DOCUMENT_ROOT'].'/models/*.php'));
$files = array_merge($files, glob($_SERVER['DOCUMENT_ROOT'].'/methods/*.php'));
$files = array_merge($files, glob($_SERVER['DOCUMENT_ROOT'].'/methods/admin/*.php'));
$files = array_merge($files, glob($_SERVER['DOCUMENT_ROOT'].'/methods/client/*.php'));
$files = array_merge($files, glob($_SERVER['DOCUMENT_ROOT'].'/methods/user/*.php'));

foreach($files as $key => $file) require_once $file;
