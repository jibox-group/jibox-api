<?php

ini_set('display_errors', 0);

require_once 'autoload.php';
require_once 'app.php';

require_once 'globals.php';

$app = new App();
