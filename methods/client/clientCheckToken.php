<?php
class clientCheckToken {

    function __construct(){
        global $const, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $user = USER_CLIENT::getByToken($header['token']);
        if($user) return new Response(true);
        else new Errors($const['clientNotFound']);
    }
}
