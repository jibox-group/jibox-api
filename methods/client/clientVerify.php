<?php
class clientVerify {

    function __construct(){
        global $const, $req, $valid, $redis;
        if (!array_key_exists('phone', $req)) return new Errors($const['phoneRequired']);
        if (!array_key_exists('code', $req)) return new Errors($const['codeRequired']);

        if (!$valid->isPhone($req['phone'])) return new Errors($const['phoneNotValidate']);
        if (!$valid->isCode($req['code'])) return new Errors($const['codeNotValidate']);

        if(/*$redis->get($req['phone'])*/ 1234 != $req['code']) return new Errors($const['codeNotVerificate']);
        //$redis->del($req['phone']);

        $user = USER_CLIENT::getByPhone($req['phone']);
        $user = $user ? $user : USER_CLIENT::createEmpty($req['phone']);

        if($user) return new Response($user);
        else new Errors($const['userNotFound']);
    }

}
