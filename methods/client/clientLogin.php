<?php
class clientLogin {

    function __construct(){
        global $req, $valid, $const;
        if(!array_key_exists('phone', $req)) return new Errors($const['phoneRequired']);
        if(!$valid->isPhone($req['phone'])) return new Errors($const['phoneNotValidate']);
        if($this->sendCode($req['phone'])) new Response($const['codeSendSuccess']);
        else new Errors($const['codeSendFailed']);
    }

    private function sendCode($phone){
        global $fun, $redis;
        //$code = $fun->generateNumber();
        //$redis->set($phone, $code, 300);
        return true;
    }
}
