<?php
class clientEdit {

    function __construct(){
        global $const, $body, $valid, $req;
        if (!array_key_exists('token', $req)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('fullname', $req)) return new Errors($const['fullnameRequired']);
        if (!array_key_exists('gender', $req)) return new Errors($const['genderRequired']);

        if (!$valid->isToken($req['token'])) return new Errors($const['tokenNotValidate']);
        if (!$valid->isString($req['gender'])) return new Errors($const['genderNotValidate']);

        $user = USER_CLIENT::getByToken($req['token']);

        if($user){
            $res = USER_CLIENT::update($user['id']);
            if($res) new Response($res);
            else new Errors('clientNotUpdate');
        }
        else new Errors($const['clientNotFound']);
    }

}
