<?php
class clientAddRequest {

    function __construct(){
        global $const, $req, $valid;
        if (!array_key_exists('token', $req)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($req['token'])) return new Errors($const['tokenNotValidate']);

        $client = USER_CLIENT::getByToken($req['token']);

        if($client){
            $result = REQUEST::create($client['id']);
            if($result) return new Response($result);
            else new Errors($const['estateNotCreate']);
        }
        else new Errors($const['userNotFound']);
    }
}
