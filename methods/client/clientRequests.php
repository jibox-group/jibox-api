<?php
class clientRequests {

    function __construct(){
        global $const, $valid, $req;
        if (!array_key_exists('token', $req)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($req['token'])) return new Errors($const['tokenNotValidate']);

        $client = USER_CLIENT::getByToken($req['token']);

        if($client){
            $requests = REQUEST::getAll($client['id']);
            if($requests) return new Response($requests);
            else new Errors($const['clientNotFound']);
        }
        else new Errors($const['userNotFound']);
    }

}
