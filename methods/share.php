<?php
class share {

    function __construct(){
        global $const, $req, $valid;
        if (!array_key_exists('id', $req)) return new Errors('`id` должен быть введен!');

        if (!$valid->isNumber($req['id'])) return new Errors('`id` not validate');

        $share = $this->getShare($req['id']);
        if($share) return new Response($share);
        else new Errors($const['estateNotFound']);
    }
    
    function getShare($id) {
        global $db;
        $res = $db->query("SELECT
                e.*,
                (SELECT name FROM regions r WHERE e.region_id = r.id) AS regionName,
                (SELECT name FROM districts d WHERE e.district_id = d.id) AS districtName,
                (SELECT name FROM quarters q WHERE e.quarter_id = q.id) AS quarterName,
                sh.createdDate AS shareDate
              FROM estates e
              JOIN share sh ON e.id = sh.estateId
              WHERE e.id=? AND e.status<>'delete'", [$id]);
        return $res->fetch();
    }
}
