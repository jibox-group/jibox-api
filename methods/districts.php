<?php
class districts {

    function __construct(){
        global $const, $req, $valid;
        if (!array_key_exists('regionId', $req)) return new Errors($const['regionIdRequired']);

        if (!$valid->isNumber($req['regionId'])) return new Errors($const['regionIdNotValidate']);

        $districts = $this->Districts();
        if($districts) return new Response($districts);
        else new Errors($const['districtsNotFound']);
    }

    private function Districts(){
        global $db, $req;
        $res = $db->query(
            "SELECT id, name FROM districts WHERE region_id=?",
            [$req['regionId']]
        );
        if ($res) return $res->fetchAll();
        else return false;
    }
}
