<?php
class cronBilling {
    function __construct() {
        $this->userCalculate();
        // $this->cleanImages();
    }

    function userCalculate() {
        global $fun;
        $users = CRON::getActiveUsers();
    
        foreach ($users as $i => $user) {
            $engage = ENGAGE::getActiveByOwner($user['id']);
            $tariff = TARIFF::getById($user['tariff_id']);
            
            if($engage && $engage['status'] == 'active') {
                $time = time();
                $range = strtotime(date('Y-m-d', floor($engage['start_date'] / 1000)).' +1 Month');
                
                if($range - $time <= 0) {
    
                    ENGAGE::expire($engage['id']);
                    USER::changeStatus($user['id']);
                    if($tariff['status'] == 'active'
                            && $tariff['id'] != 1
                            && CRON::pay($user, -$tariff['price'], 'Ежемесячный платеж')
                    ) {
                        $engage['tariff_id'] = $tariff['id'];
                        $engage['status'] = 'active';
                        ENGAGE::createDefault($engage);
                        USER::changeStatus($user['id'], 'active');
                    }
                }
            }
            else {
                USER::changeStatus($user['id']);
                if($tariff['status'] == 'active'
                        && $tariff['id'] != 1
                        && CRON::pay($user, -$tariff['price'], 'Ежемесячный платеж')
                ) {
                    $engage['tariff_id'] = $tariff['id'];
                    $engage['owner'] = $user['id'];
                    if (ENGAGE::createDefault($engage)) USER::changeStatus($user['id'], 'active');
                }
            }
        }
    }
    
    function cleanImages() {
        global $db;
        $images = [];
        $images = array_merge($images, glob('file/*'));
        foreach($images as $i => $image) {
            $img = substr($image, 5);
            $res = $db->query("SELECT id FROM estates WHERE image=? OR image1=? OR image2=? OR image3=? OR image4=? OR image5=? OR image6=? OR image7=? OR image8=? OR image9=? OR image10=? OR image11=? LIMIT 0, 1", [$img,$img,$img,$img,$img,$img,$img,$img,$img,$img,$img,$img])->fetch();
            if(!$res){
                $res = $db->query("SELECT id FROM users WHERE image=? LIMIT 0, 1", [$img])->fetch();
                if(!$res){
                    $res = $db->query("SELECT id FROM clients WHERE image=? LIMIT 0, 1", [$img])->fetch();
                    if(!$res){
                        unlink($image);
                    }
                }
            };
        }
    }
}
