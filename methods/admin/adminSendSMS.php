<?php
class adminSendSMS {

    function __construct(){
        global $const, $valid, $header, $body;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $sms = SMS::send($body->to, $body->msg);
            if($sms) return new Response(json_decode($sms));
            else new Errors($const['smsNotSend']);
        }
        else new Errors('logout');
    }

}
