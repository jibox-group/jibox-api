<?php
class adminShowEstate {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('estateId', $req)) return new Errors($const['estateIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $res = ESTATE::show($req['estateId']);
            if($res) return new Response($res);
            else new Errors($const['estateNotFound']);
        }
        else new Errors('logout');
    }

}
