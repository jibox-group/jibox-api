<?php
class adminAllInfo {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $info = INFO::all();
            if($info) return new Response($info);
            else new Errors($const['notFound']);
        }
        else new Errors('logout');
    }

}
