<?php
class adminDeleteToken {

    function __construct() {
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('tokenId', $req)) return new Errors();

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin) {
            $res = TOKEN::deleteById($req['tokenId']);
            if($res) return new Response(true);
            else new Errors($const['notFound']);
        }
        else new Errors('logout');
    }

}
