<?php
class adminLogin {

    function __construct(){
        global $req, $valid, $const, $fun;
        if(!array_key_exists('phone', $req)) return new Errors($const['phoneRequired']);
        if(!array_key_exists('password', $req)) return new Errors($const['passwordRequired']);
        if(!$valid->isPhone($req['phone'])) return new Errors($const['phoneNotValidate']);

        $admin = ADMIN::login($req['phone'], $req['password']);

        $token = $fun->generateToken();
        if($admin) $setterToken = ADMIN::setToken($admin['id'], $token);

        if($admin && $setterToken) {
            $admin['token'] = $token;
            new Response($admin);
        }
        else new Errors($const['authError']);
    }

}
