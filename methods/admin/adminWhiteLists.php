<?php
class adminWhitelists {

    function __construct(){
        global $const, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
    
        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $whitelists = WHITELIST::getAll();
            if($whitelists) return new Response($whitelists);
            else new Errors('Whitelist not found!');
        }
        else new Errors('logout');
    }

}
