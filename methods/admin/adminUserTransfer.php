<?php
class adminUserTransfer {

    function __construct() {
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('userId', $req)) return new Errors($const['userIdRequired']);
        if (!array_key_exists('amount', $req)) return new Errors($const['amountRequired']);
        if (!array_key_exists('comment', $req)) return new Errors($const['commentRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin) {
            $user = USER::getById($req['userId']);
            if($user) {
                $transfer = CRON::pay($user, $req['amount'], $req['comment']);
                if($transfer) return new Response($transfer);
                else return new Errors($const['moneyTransferFailed']);
            }
            else return new Errors($const['userNotFound']);
        }
        else return new Errors('logout');
    }

}
