<?php
class adminEstates {

    function __construct(){
        global $const, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $estates = ESTATE::gets();
            if($estates) return new Response($estates);
            else new Errors($const['estateNotFound']);
        }
        else new Errors('logout');
    }

}
