<?php
class adminAddWhitelist {

    function __construct(){
        global $const, $valid, $header, $body;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!$body->phone) return new Errors($const['phoneRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $res = WHITELIST::create($body->phone, $body->comment);
            if($res) new Response($res);
            else new Errors('not created');
        }
        else new Errors('logout');
    }

}
