<?php
class adminDeleteCompany {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('id', $req)) return new Errors($const['idRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $id = COMPANY::delete($req['id']);
            if($id) new Response($id);
            else new Errors('not deleted');
        }
        else new Errors('logout');
    }

}
