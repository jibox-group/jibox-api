<?php
class adminSetSettings {

    function __construct(){
        global $const, $valid, $header, $req, $files;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            FILE::uploadAd($files['image']);
            $res = SETTING::update($req['last_version'], $req['max_login_count']);
            if($res) return new Response($res);
            else new Errors($const['notFound']);
        }
        else new Errors('logout');
    }

}
