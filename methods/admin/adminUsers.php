<?php
class adminUsers {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $users = USER::getAllUsers();
            if($users) return new Response($users);
            else new Errors($const['usersNotFound']);
        }
        else new Errors('logout');
    }

}
