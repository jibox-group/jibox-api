<?php
class adminDeleteUser {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('userId', $req)) return new Errors($const['userIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $res = USER::delete($req['userId']);
            if($res) return new Response($res);
            else new Errors($const['notFound']);
        }
        else new Errors('logout');
    }

}
