<?php
class adminDeleteRequest {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $res = REQUEST::delete($req['id'], $req['owner']);
            if($res) new Response($res);
            else new Errors('not created');
        }
        else new Errors('logout');
    }

}
