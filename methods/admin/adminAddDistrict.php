<?php
class adminAddDistrict {

    function __construct(){
        global $const, $valid, $header, $body;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('name', $body)) return new Errors($const['nameRequired']);
        if (!array_key_exists('region_id', $body)) return new Errors($const['regionIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $id = DISTRICT::create($body->name, $body->region_id);
            if($id) new Response($id);
            else new Errors('not created');
        }
        else new Errors('logout');
    }

}
