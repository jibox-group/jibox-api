<?php
class adminDeleteTariff {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('tariffId', $req)) return new Errors($const['tariffIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $id = TARIFF::delete($req['tariffId']);
            if($id) new Response($id);
            else new Errors('not deleted');
        }
        else new Errors('logout');
    }

}
