<?php
class adminEditTariff {

    function __construct(){
        global $const, $valid, $header, $body;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('name', $body)) return new Errors($const['nameRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $id = TARIFF::update($body->tariffId, $body->name, $body->price, $body->estates, $body->archives, $body->requests, $body->clients);
            if($id) new Response($id);
            else new Errors('not updated');
        }
        else new Errors('logout');
    }

}
