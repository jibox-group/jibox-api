<?php
class adminAddNotificationUser {

    function __construct(){
        global $const, $valid, $header, $body;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('userIds', $body)) return new Errors($const['userIdsRequired']);
        if (!array_key_exists('text', $body)) return new Errors($const['textRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            foreach($body->userIds as $i => $v) {
                NOTIFICATION::create($v, 'Администрация', $body->text);
            }
            new Response(true);
        }
        else new Errors('logout');
    }

}
