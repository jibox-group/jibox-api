<?php
class adminUserTransactions {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('userId', $req)) return new Errors($const['userIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $transactions = TRANSACTION::getByOwner($req['userId']);
            if($transactions) return new Response($transactions);
            else new Errors($const['notFound']);
        }
        else new Errors('logout');
    }

}
