<?php
class adminRequests {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $requests = REQUEST::getRequests();
            if($requests) return new Response($requests);
            else new Errors('Requests not found!');
        }
        else new Errors('logout');
    }

}
