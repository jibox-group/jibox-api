<?php
class adminQuarters {

    function __construct(){
        global $const, $valid, $header, $body;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $quarters = QUARTER::getAll();
            if($quarters) new Response($quarters);
            else new Errors('not found');
        }
        else new Errors('logout');
    }

}
