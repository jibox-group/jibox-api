<?php
class adminUndergrounds {

    function __construct(){
        global $const, $valid, $header, $body;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $admin = ADMIN::getByToken($header['token']);

        if($admin){
            $undergrounds = UNDERGROUND::getAll();
            if($undergrounds) new Response($undergrounds);
            else new Errors('not found');
        }
        else new Errors('logout');
    }

}
