<?php
class userEditClient {

    function __construct(){
        global $const, $body, $req, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
        if (!$valid->isNumber($req['clientId'])) return new Errors($const['clientIdNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $res = CLIENT::update($req['clientId'], $token['owner']);
            if($res) return new Response($res);
            else new Errors($const['clientNotUpdate']);
        }
        else new Errors($const['userNotFound']);
    }

}
