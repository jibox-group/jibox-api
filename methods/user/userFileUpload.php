<?php
class userFileUpload {

    function __construct(){
        global $const, $valid, $header, $files;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('image', $files)) return new Errors($const['imageRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $filename = FILE::upload($files['image']);
            if($filename) return new Response($filename);
            else new Errors($const['fileNotUploaded']);
        }
        else new Errors($const['userNotFound']);
    }
}
