<?php
class userCheckToken {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);
        if($token) {
            TOKEN::setFCMToken($token['id'], $req['fcm']);
        }
        if($token) return new Response(true);
        else new Errors($const['userNotFound']);
    }
}
