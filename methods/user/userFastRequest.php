<?php
class userFastRequest {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $request = REQUEST::get($token['owner'], $req['fastRequestId']);
            if($request) return new Response($request);
            else new Errors('FastRequest not found!');
        }
        else new Errors($const['userNotFound']);
    }

}
