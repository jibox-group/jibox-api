<?php
class userAddEstate {

    function __construct(){
        global $const, $body, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!$body->owner) return new Errors($const['ownerRequired']);
        if (!$body->regionId) return new Errors($const['regionIdRequired']);
        if (!$body->districtId) return new Errors($const['districtIdRequired']);
        if (!$body->type) return new Errors($const['estateTypeRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $user = USER::getById($token['owner']);
            if(USER::isBlocked($user)) return new Errors('Ваш статус заблокирован, пожалуйста, сначала активируйте свой аккаунт');
            if(USER::isLimited($user)) return new Errors('Ваш статус ограничен, пожалуйста, сначала активируйте свой аккаунт');
            if(!USER::checkLimitByEstate($user)) return new Errors('Ошибка 101. Пожалуйста, обратитесь в службу техподдержки.');
            $result = ESTATE::create($user['id']);
            if($result) return new Response($result);
            else new Errors($const['estateNotCreate']);
        }
        else new Errors($const['userNotFound']);
    }
}
