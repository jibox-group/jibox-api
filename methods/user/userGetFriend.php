<?php
class userGetFriend {

    function __construct(){
        global $req, $valid, $header, $const;

        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if(!array_key_exists('userId', $req)) return new Errors($const['userIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
        
        $token = TOKEN::getByToken($header['token']);
        $user = USER::getById($token['owner']);
        $user1 = USER::getById($req['userId']);
        if($user1 && $user['id'] != $user1['id']){
            $friend = FRIEND::get($user['id'], $user1['id']);
            if($friend){
                $res = FRIEND::confirm($user['id'], $user1['id']);
                return $res ? new Response() : new Response(false);
            } else {
                new Response(null);
            }
        } else {
            new Response(null);
        }

    }

}
