<?php
class userVerify {

    function __construct(){
        global $const, $req, $valid, $redis, $fun;
        if (!array_key_exists('phone', $req)) return new Errors($const['phoneRequired']);
        if (!array_key_exists('code', $req)) return new Errors($const['codeRequired']);

        if (!$valid->isPhone($req['phone'])) return new Errors($const['phoneNotValidate']);
        if (!$valid->isCode($req['code'])) return new Errors($const['codeNotValidate']);

        if($redis->get($req['phone']) != $req['code']) return new Errors($const['codeNotVerificate']);
        $redis->del($req['phone']);

        $user = USER::getByPhone($req['phone']);
        $user = $user ? $user : USER::createEmpty($req['phone']);
        $token = TOKEN::create($user['id']);
        $user['token'] = $token['auth'];
        $user['fcm_token'] = $req['fcm'];
        
        TOKEN::setFCMToken($token['id'], $req['fcm']);

        if($user) return new Response($user);
        else new Errors($const['userNotFound']);
    }

}
