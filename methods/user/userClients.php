<?php
class userClients {

    function __construct(){
        global $const, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $clients = CLIENT::getAll($token['owner']);
            if($clients) return new Response($clients);
            else new Errors($const['clientNotFound']);
        }
        else new Errors($const['userNotFound']);
    }

}
