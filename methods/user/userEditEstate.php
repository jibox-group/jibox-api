<?php
class userEditEstate {

    function __construct(){
        global $const, $valid, $header, $body, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('estateId', $req)) return new Errors($const['estateIdRequired']);

        $token = TOKEN::getByToken($header['token']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
        if ($body->owner && !CLIENT::checkByUserId($body->owner, $token['owner'])) return new Errors($const['clientNotFound']);
        if (!ESTATE::checkByUserId($req['estateId'], $token['owner'])) return new Errors($const['estateNotFound']);

        if($token){
            $result = ESTATE::update();
            if($result) return new Response($result);
            else new Errors($const['estateNotUpdate']);
        }
        else new Errors($const['userNotFound']);
    }

}
