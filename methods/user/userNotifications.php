<?php
class userNotifications {

    function __construct(){
        global $const, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $notifications = NOTIFICATION::getAll($token['owner']);
            if($notifications) return new Response($notifications);
            else new Errors($const['notificationNotFound']);
        }
        else new Errors($const['userNotFound']);
    }

}
