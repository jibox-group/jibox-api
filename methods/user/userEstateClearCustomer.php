<?php
class userEstateClearCustomer {

    function __construct() {
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('estateId', $req)) return new Errors($const['estateIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);
        if($token) {
            $result = ESTATE::clearCustomer($req['estateId']);
            if($result) return new Response($result);
            else new Errors($const['estateNotUpdate']);
        }
        else new Errors($const['userNotFound']);
    }

}
