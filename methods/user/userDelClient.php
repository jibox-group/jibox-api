<?php
class userDelClient {

    function __construct(){
        global $const, $body, $req, $valid, $header;
        print_r($req->clientId);
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
        if (!$valid->isNumber($req['clientId'])) return new Errors($const['clientIdNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $res = CLIENT::delete($req['clientId'], $token['owner']);
            if($res){
                ESTATE::deleteByClientId($req['clientId']);
                return new Response($res);
            }
            else new Errors($const['clientNotUpdate']);
        }
        else new Errors($const['userNotFound']);
    }

}
