<?php
class userAddClient {

    function __construct(){
        global $const, $body, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!$body->fullname) return new Errors($const['fullnameRequired']);
        if (!$body->phone) return new Errors($const['phoneRequired']);
        if (!$body->gender) return new Errors($const['genderRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
        if (!$valid->isString($body->gender)) return new Errors($const['genderNotValidate']);
        if (!$valid->isPhone($body->phone)) return new Errors($const['phoneNotValidate']);
        if ($body->phone1 && !$valid->isPhone($body->phone1)) return new Errors($const['phoneNotValidate']);
        if ($body->phone2 && !$valid->isPhone($body->phone2)) return new Errors($const['phoneNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $user = USER::getById($token['owner']);
            if(CLIENT::checkByPhoneAndUserId($body->phone, $token['owner'])){
                return new Errors('У вас уже есть этот телефонный клиент');
            }
            if(USER::isBlocked($user)) return new Errors('Ваш статус заблокирован, пожалуйста, сначала активируйте свой аккаунт');
            if(USER::isLimited($user)) return new Errors('Ваш статус ограничен, пожалуйста, сначала активируйте свой аккаунт');
            if(!USER::checkLimitByClient($user)) return new Errors('Ошибка 101. Пожалуйста, обратитесь в службу техподдержки');
            $client = CLIENT::create($token['owner']);
            if($client) return new Response($client);
            else new Errors($const['clientNotCreate']);
        }
        else new Errors($const['userNotFound']);
    }

}
