<?php
class userStatistics {

    function __construct(){
        global $const, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $res = [
                "newClients" => CLIENT::getStatistics($token['owner']),
                "newObjects" => ESTATE::getStatistics($token['owner']),
                "rent" => RENT::getStatistics($token['owner']),
                "sell" => SELL::getStatistics($token['owner'])
            ];

            new Response($res);

        }
        else new Errors($const['userNotFound']);
    }

}
