<?php
class userCheckClientPhone {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('phone', $req)) return new Errors($const['phoneRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
        if (!$valid->isPhone($req['phone'])) return new Errors($const['phoneNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $res = CLIENT::checkByPhoneAndUserId($req['phone'], $token['owner']);
            if($res) return new Response($res);
            else new Errors($const['clientNotFound']);
        }
        else new Errors($const['userNotFound']);
    }

}
