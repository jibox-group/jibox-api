<?php
class userShortDetails {

    function __construct(){
        global $req, $valid, $header, $const;

        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
    
        $token = TOKEN::getByToken($header['token']);
        if($token){
            $user = USER::getById($token['owner']);
            $tariff = TARIFF::getById($user['tariff_id']);
            new Response([
                'id' => $user['id'],
                'status' => $user['status'],
                'can_add_from_owner' => $user['can_add_from_owner'] ? true : false,
                'fullname' => $user['fullname'],
                'phone' => $user['phone'],
                'image' => $user['image'],
                'balance' => $user['balance'],
                'tariff' => $tariff['title'],
                'tariff_id' => $tariff['id']
            ]);
        } else new Errors($const['userNotFound']);
    }

}
