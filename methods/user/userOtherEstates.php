<?php
class userOtherEstates {

    function __construct(){
        global $const, $valid, $body, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
        if (!$valid->isAccessType($body->accessType)) return new Errors($const['accessTypeNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $estates = ESTATE::getOthers($token['owner']);
            if($estates) return new Response($estates);
            else new Errors($const['estateNotFound']);
        }
        else new Errors($const['userNotFound']);
    }

}
