<?php
class userClient {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('clientId', $req)) return new Errors($const['clientIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $clients = CLIENT::get($token['owner'], $req['clientId']);
            if($clients) return new Response($clients);
            else new Errors($const['clientNotFound']);
        }
        else new Errors($const['userNotFound']);
    }

}
