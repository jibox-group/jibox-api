<?php
class userDelFastRequest {

    function __construct(){
        global $const, $req, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!$req['requestId']) return new Errors($const['requestIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $result = REQUEST::delete($req['requestId'], $token['owner']);
            if($result) return new Response($result);
            else new Errors($const['requestNotDelete']);
        }
        else new Errors($const['userNotFound']);
    }
}
