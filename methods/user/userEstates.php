<?php
class userEstates {

    function __construct(){
        global $const, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $estates = ESTATE::getAll($token['owner']);
            if($estates) return new Response($estates);
            else new Errors($const['estateNotFound']);
        }
        else new Errors($const['userNotFound']);
    }

}
