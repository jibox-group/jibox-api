<?php
class userEstateShare {

    function __construct(){
        global $const, $valid, $header, $body, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('estateId', $req)) return new Errors($const['estateIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);
        if($token){
            $result = ESTATE::share($req['estateId']);
            if($result) {
                $res = [
                    "url" => 'https://share.jibox.uz/'.$req['estateId']
                ];
                return new Response($res);
            }
            else new Errors($const['estateNotUpdate']);
        }
        else new Errors($const['userNotFound']);
    }

}
