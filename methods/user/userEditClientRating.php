<?php
class userEditClientRating {

    function __construct(){
        global $const, $valid, $header, $req, $body;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('clientId', $req)) return new Errors($const['clientIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $clients = CLIENT::changeRating($req['clientId'], $token['owner'], $req['rating']);
            if($clients) return new Response(true);
            else new Errors($const['notCompleted']);
        }
        else new Errors($const['userNotFound']);
    }

}
