<?php
class userSetTariff {

    function __construct(){
        global $req, $valid, $header, $const;

        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if(!array_key_exists('tariffId', $req)) return new Errors($const['tariffIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);
        if($token){
            $user = USER::getById($token['owner']);
    
            if(USER::isBlocked($user))
                return new Errors('Ваш статус заблокирован, пожалуйста, сначала активируйте свой аккаунт');
            
            $engage = ENGAGE::getActiveByOwner($user['id']);
            $tariff = TARIFF::getById($req['tariffId']);
            if($tariff['status'] == 'active'
                    && $tariff['id'] != 1
                    && CRON::pay($user, -$tariff['price'], 'Оплата нового тарифного плана')) {
                $engage['tariff_id'] = $tariff['id'];
                $engage['owner'] = $user['id'];
                ENGAGE::createDefault($engage);
                USER::changeStatus($user['id'], 'active');
                USER::changeTariff($user['id'], $tariff['id']);
                if($engage) ENGAGE::expire($engage['id']);
                return new Response(true);
            }
            return new Errors('Не удалось перейти на новый тариф!');
        }
        return new Errors($const['userNotFound']);

    }

}
