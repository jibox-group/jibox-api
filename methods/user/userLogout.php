<?php
class userLogout {

    function __construct() {
        global $const, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
    
        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
    
        $token = TOKEN::getByToken($header['token']);
    
        if($token){
            TOKEN::deleteById($token['id']);
            new Response(true);
        }
        else new Errors($const['userNotFound']);
    }
    
}
