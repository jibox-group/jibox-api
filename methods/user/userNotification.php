<?php
class userNotification {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('notificationId', $req)) return new Errors($const['notificationIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $notification = NOTIFICATION::get($req['notificationId'], $token['owner']);
            if($notification) return new Response($notification);
            else new Errors($const['notificationNotFound']);
        }
        else new Errors($const['userNotFound']);
    }

}
