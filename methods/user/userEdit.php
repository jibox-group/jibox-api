<?php
class userEdit {

    function __construct(){
        global $const, $body, $valid, $header;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $res = USER::update($token['owner']);
            if($res) new Response($res);
            else new Errors('userNotUpdate');
        }
        else new Errors($const['userNotFound']);
    }

}
