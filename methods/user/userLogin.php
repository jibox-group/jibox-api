<?php
class userLogin {

    function __construct(){
        global $req, $valid, $const;
        if(!array_key_exists('phone', $req)) return new Errors($const['phoneRequired']);
        if(!$valid->isPhone($req['phone'])) return new Errors($const['phoneNotValidate']);
        if(!(WHITELIST::getByPhone($req['phone']) || '890000000' == $req['phone'])) return new Errors('Ваш номер телефона не идентифицирован. Пожалуйста, обратитесь в службу техподдержки.');
        if($this->sendCode($req['phone'])) new Response($const['codeSendSuccess']);
        else new Errors($const['codeSendFailed']);
    }

    private function sendCode($phone){
        global $fun, $redis;
        $code = $fun->generateNumber();
        if('890000000' == $phone) $code = '2020';
        if('890000000' != $phone) SMS::send('+998'.$phone, 'Код подтверждения для входа в приложение JiBox:: '.$code);
        $redis->set($phone, $code);
        return true;
    }
}
