<?php
class userPublicInfo {

    function __construct(){
        global $req, $valid, $header, $const;

        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if(!array_key_exists('phone', $req)) return new Errors($const['phoneRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
        if(!$valid->isPhone($req['phone'])) return new Errors($const['phoneNotValidate']);
        
        $token = TOKEN::getByToken($header['token']);
    
        $user = USER::getById($token['owner']);
        $user1 = USER::getByPhone($req['phone']);
        if($user1 && $user['id'] != $user1['id']){
            $friend = FRIEND::get($user['id'], $user1['id']);
            $status = '';
            $type = '';
            $owner = false;
            $id = 0;
            if($friend){
                $id = $friend['id'];
                $status = $friend['status'];
                $type = $friend['type'];
                $owner = $friend['owner'] == $user['id'];
            }
            return $user1['registered'] ? new Response(
                [
                    "id" => $id,
                    "friendId" => $user1['id'],
                    "fullname" => $user1['fullname'],
                    "image" => $user1['image'],
                    "owner" => $owner,
                    "status" => $status,
                    "type" => $type
                ]
            ) : new Response(null);
        } else {
            new Response(null);
        }

    }

}
