<?php
class userDetails {

    function __construct(){
        global $req, $valid, $header, $const;

        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
        
        $token = TOKEN::getByToken($header['token']);
        if($token){
            $user = USER::getById($token['owner']);
            new Response($user);
        } else new Errors($const['userNotFound']);
    }

}
