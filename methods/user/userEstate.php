<?php
class userEstate {

    function __construct(){
        global $const, $valid, $header, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('estateId', $req)) return new Errors($const['estateIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);

        if($token){
            $estates = ESTATE::get($token['owner'], $req['estateId']);
            if($estates) return new Response($estates);
            else new Errors($const['estateNotFound']);
        }
        else new Errors($const['userNotFound']);
    }

}
