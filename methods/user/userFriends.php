<?php
class userFriends {

    function __construct(){
        global $req, $valid, $header, $const;

        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
        
        $token = TOKEN::getByToken($header['token']);
        if($token){
            $type = $req['type'] ? $req['type'] : 'all';
            $status = $req['status'] ? $req['status'] : 'all';
            $friends = FRIEND::getAll($token['owner'], $type, $status);
            if($friends){
                return new Response($friends);
            } else {
                new Response([]);
            }
        } else {
            new Response(null);
        }

    }

}
