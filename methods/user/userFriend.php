<?php
class userFriend {

    function __construct(){
        global $req, $valid, $header, $const;

        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);
        
        $token = TOKEN::getByToken($header['token']);
        if($token){
            $friend = FRIEND::detail($token['owner'], $req['friendId']);
            if($friend){
                return new Response($friend);
            } else {
                new Response(null);
            }
        } else {
            new Response(null);
        }

    }

}
