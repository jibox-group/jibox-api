<?php
class userEstateFavourite {

    function __construct(){
        global $const, $valid, $header, $body, $req;
        if (!array_key_exists('token', $header)) return new Errors($const['tokenRequired']);
        if (!array_key_exists('estateId', $req)) return new Errors($const['estateIdRequired']);

        if (!$valid->isToken($header['token'])) return new Errors($const['tokenNotValidate']);

        $token = TOKEN::getByToken($header['token']);
        if($token){
            $result = ESTATE::changeFavourite($req['estateId'], $req['isFavourite'] == 'true');
            if($result) return new Response($result);
            else new Errors($const['estateNotUpdate']);
        }
        else new Errors($const['userNotFound']);
    }

}
