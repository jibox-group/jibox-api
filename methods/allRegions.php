<?php
class allRegions {

    function __construct(){
        global $const;

        $regions = $this->Regions();
        $districts = $this->Districts();
        $quarters = $this->Quarters();
        $undergrounds = $this->Undergrounds();

        if($regions || $districts || $quarters || $undergrounds) return new Response([
            "regions" => $regions,
            "districts" => $districts,
            "quarters" => $quarters,
            "undergrounds" => $undergrounds
        ]);
        else new Errors($const['regionsNotFound']);
    }

    private function Regions(){
        global $db;
        $res = $db->query(
            "SELECT * FROM regions"
        );
        if ($res) return $res->fetchAll();
        else return false;
    }

    private function Districts(){
        global $db;
        $res = $db->query(
            "SELECT * FROM districts"
        );
        if ($res) return $res->fetchAll();
        else return false;
    }

    private function Quarters(){
        global $db;
        $res = $db->query(
            "SELECT * FROM quarters"
        );
        if ($res) return $res->fetchAll();
        else return false;
    }

    private function Undergrounds(){
        global $db;
        $res = $db->query(
            "SELECT * FROM undergrounds ORDER BY `name` ASC"
        );
        if ($res) return $res->fetchAll();
        else return false;
    }
}
