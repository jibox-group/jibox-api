<?php
class regions {

    function __construct(){
        global $const;

        $regions = $this->Regions();
        if($regions) return new Response($regions);
        else new Errors($const['regionsNotFound']);
    }

    private function Regions(){
        global $db;
        $res = $db->query(
            "SELECT * FROM regions"
        );
        if ($res) return $res->fetchAll();
        else return false;
    }
}
