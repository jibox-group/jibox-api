<?php
class cronNotification {
    function __construct() {
        $this->userNotification();
    }

    function userNotification(){
        $estates = ESTATE::getRenters();
        foreach ($estates as $i => $estate) {
            if($estate['stopTime'] == 0) {
                ESTATE::changeStatus($estate['id'], 'free');
            } else {
                $cY = date('Y');
                $cm = date('m');
                $cd = date('d');
                $tY = date('Y', floor($estate['stopTime']/1000));
                $tm = date('m', floor($estate['stopTime']/1000));
                $td = date('d', floor($estate['stopTime']/1000));
                if($cY == $tY && $cm == $tm && $cd == $td){
                    $owner = CLIENT::getOwner($estate['owner']);
                    if($owner) {
                        ESTATE::changeStatus($estate['id'], 'free');
                        NOTIFICATION::create($owner, 'Объект стал доступным!', '№'.$estate['id'], 'estate', $estate['id']);
                    }
                }
            }
        }

        $users = USER::getAll();
        foreach ($users as $i => $user) {
            if($user['status'] == 'active'){
                if($user['date_birth']){
                    $time = strtotime($user['date_birth']);
                    if(date('m') == date('m', $time) && date('d') == date('d', $time)) {
                        NOTIFICATION::create($user['id'], 'Happy birthday!', 'Сотрудники JiBox с радостью спешат поздравить Вас с днем рождения и пожелать благополучия, крепкого здоровья, успехов в любых начинаниях и делах! Надеемся быть Вам полезными и в будущем!');
                    }
                }
                $clients = CLIENT::getAllClients($user['id']);
                $sendNotification = false;
                $birthCount = 0;
                foreach ($clients as $i => $client) {
                    if($client['date_birth']){
                        $time = strtotime($client['date_birth']);
                        if(date('m') == date('m', $time) && date('d') == date('d', $time)) {
                            $sendNotification = true;
                            $birthCount++;
                        }
                    }
                }
                if($sendNotification) {
                    NOTIFICATION::create($user['id'], 'День рождения!!!', 'Количество клиентов: '.$birthCount.'.', 'client');
                }
            }
        }



    }

}
