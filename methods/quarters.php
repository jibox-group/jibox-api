<?php
class quarters {

    function __construct(){
        global $const, $req, $valid;
        if (!array_key_exists('districtId', $req)) return new Errors($const['districtIdRequired']);

        if (!$valid->isNumber($req['districtId'])) return new Errors($const['districtIdNotValidate']);

        $quarters = $this->Quarters();
        if($quarters) return new Response($quarters);
        else new Errors($const['quartersNotFound']);
    }

    private function Quarters(){
        global $db, $req;
        $res = $db->query(
            "SELECT id, name FROM quarters WHERE district_id=?",
            [$req['districtId']]
        );
        if ($res) return $res->fetchAll();
        else return false;
    }
}
