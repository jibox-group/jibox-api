<?php
class Response {

    function __construct($response = true){
        global $file;
        $result['error'] = false;
        $result['result'] = $response;
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
        header("Content-Type: application/json");
        echo json_encode($result);
        
        // fwrite($file, "RESPONSE: ".json_encode($result)."\n");
    }

}
