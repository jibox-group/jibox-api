<?php
class Validate {

    public function isToken($token){
        return preg_match('/^[0-9A-Za-z]*$/', $token) && strlen($token) == 32;
    }

    public function isAccessType($type){
        return $type == 'friend' || $type == 'group' || $type == 'public' || $type == '';
    }

    public function isString($string){
        return preg_match('/^[\'0-9A-Za-zА-Яа-я ]*$/', $string);
    }

    public function isNumber($number){
        return preg_match('/^[0-9]*$/', $number);
    }

    public function isPhone($phone){
        return preg_match('/^[0-9]*$/', $phone) && strlen($phone) == 9;
    }

    public function isCode($code){
        return preg_match('/^[0-9]*$/', $code) && strlen($code) == 4;
    }

}
