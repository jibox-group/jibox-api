<?php
class Errors {

    function __construct($text = ""){
        global $file;
        $result['error'] = true;
        if($text) $result['text'] = $text;
        $result['result'] = null;
        header("Content-Type: application/json");
        echo json_encode($result);

        // fwrite($file, "ERROR: ".json_encode($result)."\n");
        fclose($file);
    }

}
