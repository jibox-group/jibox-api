<?php
class Functions {

    public function generateToken($size = 32){
        $chars = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ0123456789";
        $length = strlen($chars);
        $code = "";
        for($i = 0; $i < $size; $i++) $code .= $chars[rand(0, $length - 1)];
        return $code;
    }

    public function generateNumber($size = 4){
        $chars = "0123456789";
        $length = strlen($chars);
        $code = "";
        for($i = 0; $i < $size; $i++) $code .= $chars[rand(0, $length - 1)];
        return $code;
    }

    public function time() {
        return round(microtime(true) * 1000);
    }

    public function createOffset($limit, $page){
        return $limit * ($page - 1);
    }

    public function createLimit($limit, $page){
        return $limit * $page;
    }
}
