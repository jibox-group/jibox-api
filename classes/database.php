<?php
class Database {

    public
        $pdo = null,
        $res = null,
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
    function __construct($database, $username = "root", $password = "", $hostname = "localhost", $type = "mysql", $charset = "utf8mb4"){
        $this->database = $database;
        $this->username = $username;
        $this->password = $password;
        $this->hostname = $hostname;
        $this->type = $type;
        $this->charset = $charset;
    }
    private function connect(){
        $dsn = "$this->type:host=$this->hostname;dbname=$this->database;charset=$this->charset";
        $this->pdo = new PDO($dsn, $this->username, $this->password, $this->options);
    }
    public function query($sql, $params = []){
        if(!$this->pdo) $this->connect();
        $res = $this->pdo->prepare($sql);
        $res->execute($params);
        return $res;
    }
}
