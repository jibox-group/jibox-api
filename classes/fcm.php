<?php
class FCM {

    static public
        $url = 'https://fcm.googleapis.com/fcm/send',
        $headers = array(
        'Authorization: key=AAAA-DmDsnw:APA91bHoE8Lnx4lcvzR_q5IvCAxY220IXRDj-5bgty_PeDheyA10meGMUiSZwfpIYbterUyHkm0VxQf5eydIX4jYXjOsYz5IxcsugqxYgYAftUp06Ev8HdDA4Io0I_6aEklVV4uyBQ-O',
        'Content-Type: application/json'
    );
    
    static public function send($to, $notification, $data) {
        $fields = [
            "to" => $to,
            "notification" => $notification,
            "data" => $data,
        ];
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, self::$url);
        curl_setopt( $ch,CURLOPT_POST,true);
        curl_setopt( $ch,CURLOPT_HTTPHEADER, self::$headers);
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
    }
}
