<?php
class DISTRICT {

    static public function create($name, $region_id) {
        global $db;
        $res = $db->query(
            "INSERT INTO districts SET
                `name`=?, `region_id`=?",
            [$name, $region_id]
        );
        if($res) {
            $r = $db->query(
                "SELECT d.id
            FROM districts d
            ORDER BY id DESC LIMIT 0, 1"
            );
            $district = $r->fetch();
            return $district['id'];
        }
        else return false;
    }

    static public function getAll() {
        global $db, $fun, $req;
        $where = " WHERE 1=1";
        $pagination = "";

        if($req['keyword']){
            $where .= " AND d.name LIKE '%".$req['keyword']."%'";
        }
        if($req['region_id']){
            $where .= " AND d.region_id=".$req['region_id'];
        }
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }
        $resCount = $db->query("SELECT COUNT(id) as count FROM districts d".$where);
        $count = $resCount->fetch();
        $res = $db->query("SELECT d.* FROM districts d".$where.$pagination);
        $districts = $res->fetchAll();
        foreach ($districts as $i => $district) {
            $districts[$i]['regionName'] = REGION::getName($district['region_id']);
        }
        $result = [
            "count" => $count['count'],
            "items" => $districts
        ];
        return $result;
    }

    static public function get($id) {
        global $db;
        $res = $db->query(
            "SELECT *
            FROM districts d
            WHERE d.id=?",
            [$id]
        );
        $district = $res->fetch();
        if($district) {
            return $district;
        }
        else return null;
    }

    static public function getName($id) {
        global $db;
        $res = $db->query(
            "SELECT d.name
            FROM districts d
            WHERE d.id=?",
            [$id]
        );
        $district = $res->fetch();
        if($district) {
            return $district['name'];
        }
        else return null;
    }

    static public function delete($id) {
        global $db;
        $res = $db->query(
            "DELETE FROM districts
            WHERE id=?",
            [$id]
        );
        return $res ? true : false;
    }

    static public function update($id, $regionId, $name) {
        global $db;
        $res = $db->query(
            "UPDATE districts SET name=?, region_id=?
            WHERE id=?",
            [$name, $regionId, $id]
        );
        return $res ? true : false;
    }

}
