<?php
class TARIFF {

    static public function getById($id) {
        global $db;
        $res = $db->query("SELECT * FROM tariffs WHERE id=?", [$id]);
        return $res->fetch();
    }

    static public function getActives() {
        global $db;
        $res = $db->query("SELECT * FROM tariffs WHERE status='active'");
        return $res->fetchAll();
    }

    static public function getAll() {
        global $db, $fun, $req;
        $where = " WHERE 1=1";
        $pagination = "";

        if($req['keyword']){
            $where .= " AND t.name LIKE '%".$req['keyword']."%'";
        }
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }
        $resCount = $db->query("SELECT COUNT(id) as count FROM tariffs t".$where);
        $count = $resCount->fetch();
        $res = $db->query("SELECT * FROM tariffs t".$where.$pagination);
        $result = [
            "count" => $count['count'],
            "items" => $res->fetchAll()
        ];
        return $result;
    }

    static public function create($name, $price, $estates, $archives, $requests, $clients, $status = 'active') {
        global $db;
        $res = $db->query(
            "INSERT INTO tariffs SET
                `name`=?, `title`=?, `price`=?, `estates`=?, `archives`=?, `requests`=?, `clients`=?, `status`=?",
            [$name, ucfirst($name), $price, $estates, $archives, $requests, $clients, $status]
        );
        if($res) {
            $r = $db->query(
                "SELECT t.id
            FROM tariffs t
            ORDER BY id DESC LIMIT 0, 1"
            );
            $tariff = $r->fetch();
            return $tariff['id'];
        }
        else return false;
    }

    static public function delete($id) {
        global $db;
        $res = $db->query(
            "DELETE FROM tariffs
            WHERE id=?",
            [$id]
        );
        return $res ? true : false;
    }

    static public function update($id, $name, $price = NULL, $estates = NULL, $archives = NULL, $requests = NULL, $clients = NULL, $status = 'active') {
        global $db;
        $res = $db->query(
            "UPDATE tariffs SET `name`=?, `price`=?, `estates`=?, `archives`=?, `requests`=?, `clients`=?, `status`=?
            WHERE id=?",
            [$name, $price, $estates, $archives, $requests, $clients, $status, $id]
        );
        return $res ? true : false;
    }
}
