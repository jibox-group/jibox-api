<?php
class CLIENT {

    static public function getName($id) {
        global $db;
        $res = $db->query(
            "SELECT c.fullname
            FROM clients c
            WHERE c.id=?",
            [$id]
        );
        $client = $res->fetch();
        if($client) {
            return $client['fullname'];
        }
        else return null;
    }

    static public function getPhone($id) {
        global $db;
        $res = $db->query(
            "SELECT c.phone
            FROM clients c
            WHERE c.id=?",
            [$id]
        );
        $client = $res->fetch();
        if($client) {
            return $client['phone'];
        }
        else return null;
    }

    static public function getUserName($id) {
        global $db;
        $res = $db->query(
            "SELECT u.fullname
            FROM clients c 
            JOIN users u ON u.id = c.user_id
            WHERE c.id=?",
            [$id]
        );
        $user = $res->fetch();
        if($user) {
            return $user['fullname'];
        }
        else return null;
    }

    static public function getStatistics($owner) {
        global $db, $statistics;
        $res = $statistics;

        $temp = [12][31];
        $r = self::getAll($owner);
        $clients = $r['items'];
        foreach($clients as $i => $client) {
            $time = floor($client['date_create'] / 1000);
            $date = explode(" ", date("Y m d", $time));
            if($date[0] == date("Y")){
                $m = $date[1] * 1;
                $d = $date[2] * 1;
                $temp[$m][$d] += 1;
            }
        }
        foreach ($temp as $i => $item) {
            foreach ($item as $j => $value) {
                switch ($i) {
                    case 1 : $res['januaryCount'] += $value; $res['january'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 2 : $res['februaryCount'] += $value; $res['february'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 3 : $res['marchCount'] += $value; $res['march'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 4 : $res['aprilCount'] += $value; $res['april'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 5 : $res['mayCount'] += $value; $res['may'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 6 : $res['juneCount'] += $value; $res['june'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 7 : $res['julyCount'] += $value; $res['july'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 8 : $res['augustCount'] += $value; $res['august'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 9 : $res['septemberCount'] += $value; $res['september'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 10 : $res['octoberCount'] += $value; $res['october'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 11 : $res['novemberCount'] += $value; $res['november'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 12 : $res['decemberCount'] += $value; $res['december'][$j-1] = ["day" => $j, "count" => $value]; break;
                }
            }
        }
        return $res;
    }

    static public function getOwner($id) {
        global $db;
        $id = $id * 1;
        $res = $db->query(
            "SELECT
                    c.user_id
                 FROM clients c
                 WHERE c.id=?",
            [$id]
        );
        $client = $res->fetch();
        if ($client) {
            return $client['user_id'];
        } else return false;
    }
    
    static public function getById($id) {
        global $db;
        $res = $db->query(
            "SELECT c.*
            FROM clients c
            WHERE c.id=?",
            [$id * 1]
        );
        $client = $res->fetch();
        if($client) {
            return $client;
        }
        else return false;
    }
    
    
    static public function get($userId, $id) {
        global $db, $fun;
        $id = $id * 1;
        $parameters = array();

        array_push($parameters, $id);
        array_push($parameters, $userId);

        $res = $db->query(
            "SELECT
                    c.*
                 FROM clients c
                 WHERE c.id=? AND c.user_id=?",
            $parameters
        );

        $client = $res->fetch();

        if ($client) {
            $res = $db->query(
                "SELECT
                    COUNT(id) AS count
                 FROM estates
                 WHERE owner=? AND status<>'delete'",
                [$client['id']]
            );
            $objectCount = $res->fetch();
            $preferences = [
                "vendor" => $client['vendor'] ? true : false,
                "renter" => $client['renter'] ? true : false,
                "give_renter" => $client['give_renter'] ? true : false,
                "buyer" => $client['buyer'] ? true : false,
                "barter" => $client['barter'] ? true : false
            ];
            $client['favorite'] = $client['favorite'] ? true : false;
            $client['preferences'] = $preferences;
            $client['objectCount'] = $objectCount['count'];

            return $client;
        } else return false;
    }
    
    static public function getAllClients($userId) {
        global $db;
        $res = $db->query("SELECT c.* FROM clients c WHERE c.user_id=?", [$userId]);
        return $res ? $res->fetchAll() : false;
    }

    static public function getAll($userId) {
        global $db, $req, $fun, $body;
        $parameters = array();
        $pagination = '';
        $filterIds = '';

        if($body->sort && $body->direction) {
            $sort = " ORDER BY ".$body->sort." ".$body->direction;
        } else {
            $sort = " ORDER BY date_update DESC";
        }
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }
        if($body->ids) {
            $filterIds .= " AND c.id IN(".$body->ids.")";
        }
        array_push($parameters, $userId);
        
        $res = $db->query(
            "SELECT
                    COUNT(c.id) AS count
                 FROM clients c WHERE user_id=?" .
            ($body->favorite ? " AND c.favorite = 1" : "") .
            $filterIds .
            ($body->date_birth ? " AND DATE_FORMAT(c.date_birth,'%m-%d') = DATE_FORMAT(STR_TO_DATE('".$body->date_birth."', '%Y-%m-%d'),'%m-%d')" : "") .
            ($body->todayBirthDay ? " AND DATE_FORMAT(c.date_birth,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d')" : "") .
            ($body->preferences->barter ? " AND c.barter = 1" : "") .
            ($body->preferences->vendor ? " AND c.vendor = 1" : "") .
            ($body->preferences->renter ? " AND c.renter = 1" : "") .
            ($body->preferences->give_renter ? " AND c.give_renter = 1" : "") .
            ($body->preferences->buyer ? " AND c.buyer = 1" : "") .
            ($body->gender ? " AND c.gender = '".$body->gender."'" : "") .
            ($body->rating ? " AND c.rating = ".$body->rating : "") .
            ($body->keyword ? " AND (c.fullname LIKE '%".$body->keyword."%' OR c.comment LIKE '%".$body->keyword."%' OR c.phone LIKE '%".$body->keyword."%' OR c.phone1 LIKE '%".$body->keyword."%' OR c.phone2 LIKE '%".$body->keyword."%')" : ""),
            $parameters
        );

        $count = $res->fetch();

        $res = $db->query(
            "SELECT
                    c.*,
                    DATE_FORMAT(c.date_birth,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d'),
                    (SELECT COUNT(e.id) FROM estates e WHERE e.owner=c.id AND e.status<>'delete') AS objectCount
                 FROM clients c WHERE user_id=?" .
            ($body->favorite ? " AND c.favorite = 1" : "") .
            $filterIds .
            ($body->date_birth ? " AND DATE_FORMAT(c.date_birth,'%m-%d') = DATE_FORMAT(STR_TO_DATE('".$body->date_birth."', '%Y-%m-%d'),'%m-%d')" : "") .
            ($body->todayBirthDay ? " AND DATE_FORMAT(c.date_birth,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d')" : "") .
            ($body->preferences->barter ? " AND c.barter = 1" : "") .
            ($body->preferences->vendor ? " AND c.vendor = 1" : "") .
            ($body->preferences->renter ? " AND c.renter = 1" : "") .
            ($body->preferences->give_renter ? " AND c.give_renter = 1" : "") .
            ($body->preferences->buyer ? " AND c.buyer = 1" : "") .
            ($body->gender ? " AND c.gender = '".$body->gender."'" : "") .
            ($body->rating ? " AND c.rating = ".$body->rating : "") .
            ($body->keyword ? " AND (c.fullname LIKE '%".$body->keyword."%' OR c.comment LIKE '%".$body->keyword."%' OR c.phone LIKE '%".$body->keyword."%' OR c.phone1 LIKE '%".$body->keyword."%' OR c.phone2 LIKE '%".$body->keyword."%')" : "") .
            $sort .
            $pagination,
            $parameters
        );

        if ($res) {
            $clients = [];
            while ($client = $res->fetch()) {
                $preferences = [
                    "vendor" => $client['vendor'] ? true : false,
                    "renter" => $client['renter'] ? true : false,
                    "give_renter" => $client['give_renter'] ? true : false,
                    "buyer" => $client['buyer'] ? true : false,
                    "barter" => $client['barter'] ? true : false
                ];
                $client['favorite'] = $client['favorite'] ? true : false;
                $client['preferences'] = $preferences;
                array_push($clients, $client);
            }
            $result['count'] = $count['count'];
            $result['items'] = $clients;
            return $result;
        } else return false;
    }

    static public function getByAdmin() {
        global $db, $req, $fun, $body;
        $pagination = '';
    
        if ($body->sort && $body->direction) {
            $sort = " ORDER BY " . $body->sort . " " . $body->direction;
        } else {
            $sort = " ORDER BY date_update DESC";
        }
        if ($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT ' . $offset . ', ' . $req['limit'];
        }
    
        $res = $db->query(
            "SELECT
                    COUNT(c.id) AS count
                 FROM clients c WHERE 1" .
            ($body->userId ? " AND c.user_id = ".$body->userId : "") .
            ($body->favorite ? " AND c.favorite = 1" : "") .
            ($body->preferences->barter ? " AND c.barter = 1" : "") .
            ($body->preferences->vendor ? " AND c.vendor = 1" : "") .
            ($body->preferences->renter ? " AND c.renter = 1" : "") .
            ($body->preferences->give_renter ? " AND c.give_renter = 1" : "") .
            ($body->preferences->buyer ? " AND c.buyer = 1" : "") .
            ($body->gender ? " AND c.gender = '" . $body->gender . "'" : "") .
            ($body->rating ? " AND c.rating = '" . $body->rating . "'" : "") .
            ($req['keyword'] ? " AND (c.fullname LIKE '%" . $req['keyword'] . "%' OR c.comment LIKE '%" . $req['keyword'] . "%' OR c.phone LIKE '%" . $req['keyword'] . "%' OR c.phone1 LIKE '%" . $req['keyword'] . "%' OR c.phone2 LIKE '%" . $req['keyword'] . "%')" : "")
        );
    
        $count = $res->fetch();
    
        $res = $db->query(
            "SELECT
                    c.*,
                    (SELECT COUNT(e.id) FROM estates e WHERE e.owner=c.id AND e.status<>'delete') AS objectCount
                 FROM clients c WHERE 1" .
            ($body->userId ? " AND c.user_id = ".$body->userId : "") .
            ($body->favorite ? " AND c.favorite = 1" : "") .
            ($body->preferences->barter ? " AND c.barter = 1" : "") .
            ($body->preferences->vendor ? " AND c.vendor = 1" : "") .
            ($body->preferences->renter ? " AND c.renter = 1" : "") .
            ($body->preferences->give_renter ? " AND c.give_renter = 1" : "") .
            ($body->preferences->buyer ? " AND c.buyer = 1" : "") .
            ($body->gender ? " AND c.gender = '" . $body->gender . "'" : "") .
            ($req['keyword'] ? " AND (c.fullname LIKE '%" . $req['keyword'] . "%' OR c.comment LIKE '%" . $req['keyword'] . "%' OR c.phone LIKE '%" . $req['keyword'] . "%' OR c.phone1 LIKE '%" . $req['keyword'] . "%' OR c.phone2 LIKE '%" . $req['keyword'] . "%')" : "") .
            $sort .
            $pagination
        );
    
        if ($res) {
            $clients = [];
            while ($client = $res->fetch()) {
                $preferences = [
                    "vendor" => $client['vendor'] ? true : false,
                    "renter" => $client['renter'] ? true : false,
                    "give_renter" => $client['give_renter'] ? true : false,
                    "buyer" => $client['buyer'] ? true : false,
                    "barter" => $client['barter'] ? true : false
                ];
                $client['favorite'] = $client['favorite'] ? true : false;
                $client['realtor'] = USER::getById($client['user_id']);
                $client['preferences'] = $preferences;
                array_push($clients, $client);
            }
            $result['count'] = $count['count'];
            $result['items'] = $clients;
            return $result;
        } else return false;
    }

    static public function create($userId) {
        global $db, $body, $files, $const, $fun, $file;
        $time = $fun->time();
        $parameters = array($body->fullname, $body->gender, $userId, $time, $time);
        if ($body->date_birth) array_push($parameters, $body->date_birth);
        if ($body->phone) array_push($parameters, $body->phone);
        if ($body->phone1) array_push($parameters, $body->phone1);
        if ($body->phone2) array_push($parameters, $body->phone2);
        if ($body->comment) array_push($parameters, $body->comment);
        if ($body->rating) array_push($parameters, $body->rating);

        if ($files['image']){
            $filename = FILE::upload($files['image']);
            if($filename){
                array_push($parameters, $filename);
            } else new Errors($const['fileNotUploaded']);
        }

        $res = $db->query(
            "INSERT INTO clients SET
                        fullname=?,
                        gender=?,
                        user_id=?,
                        date_create=?,
                        date_update=?" .
            ($body->date_birth ? ", date_birth=?" : "") .
            ($body->phone ? ", phone=?" : "") .
            ($body->phone1 ? ", phone1=?" : "") .
            ($body->phone2 ? ", phone2=?" : "") .
            ($body->comment ? ", comment=?" : "") .
            ($body->rating ? ", rating=?" : "") .
            ($body->preferences->vendor ? ", vendor=1" : "") .
            ($body->preferences->renter ? ", renter=1" : "") .
            ($body->preferences->give_renter ? ", give_renter=1" : "") .
            ($body->preferences->buyer ? ", buyer=1" : "") .
            ($body->preferences->barter ? ", barter=1" : "") .
            ($files['image'] ? ", image=?" : ""),
            $parameters
        );

        if ($res) $res = $db->query("SELECT id FROM clients ORDER BY id DESC LIMIT 1");
        $client = $res->fetch();
        if($client) return $client['id'];
        else return false;
    }

    static public function update($id, $userId) {
        global $db, $body, $files, $const, $fun;
        $time = $fun->time();
        $parameters = array($time);
        if ($body->fullname) array_push($parameters, $body->fullname);
        else array_push($parameters, null);
        if ($body->gender) array_push($parameters, $body->gender);
        else array_push($parameters, null);
        if ($body->date_birth) array_push($parameters, $body->date_birth);
        else array_push($parameters, null);
        if ($body->phone) array_push($parameters, $body->phone);
        else array_push($parameters, null);
        if ($body->phone1) array_push($parameters, $body->phone1);
        else array_push($parameters, null);
        if ($body->phone2) array_push($parameters, $body->phone2);
        else array_push($parameters, null);
        if ($body->comment) array_push($parameters, $body->comment);
        else array_push($parameters, null);
        if ($body->rating) array_push($parameters, $body->rating);
        else array_push($parameters, 0);
        if ($body->image) array_push($parameters, $body->image);
        else array_push($parameters, null);

        array_push($parameters, $id);
        array_push($parameters, $userId);

        $res = $db->query(
            "UPDATE clients SET
                date_update=?, fullname=?, gender=?, date_birth=?,
                phone=?, phone1=?, phone2=?, comment=?, rating=?" .
            ($body->preferences->vendor ? ", vendor=1" : ", vendor=0") .
            ($body->preferences->renter ? ", renter=1" : ", renter=0") .
            ($body->preferences->give_renter ? ", give_renter=1" : ", give_renter=0") .
            ($body->preferences->buyer ? ", buyer=1" : ", buyer=0") .
            ($body->preferences->barter ? ", barter=1" : ", barter=0") .
            ", image=?" .
            " WHERE id=? AND user_id=?",
            $parameters
        );
        return $res ? $id * 1 : false;
    }

    static public function changeRating($id, $userId, $rating) {
        global $db, $fun;
        $time = $fun->time();
        $parameters = array($time, $rating, $id, $userId);

        $res = $db->query(
            "UPDATE clients SET
                date_update=?, rating=? WHERE id=? AND user_id=?",
            $parameters
        );
        return $res ? true : false;
    }

    static public function delete($id, $userId) {
        global $db, $body, $files, $const, $fun;
        $time = $fun->time();
        $parameters = array($id, $userId);

        $res = $db->query(
            "DELETE FROM clients WHERE id=? AND user_id=?",
            $parameters
        );
        return $res ? true : false;
    }

    static public function checkByUserId($id, $userId) {
        global $db;
        $res = $db->query(
            "SELECT c.id
            FROM clients c
            WHERE c.id=? AND c.user_id=?",
            [$id, $userId]
        );
        return $res->fetch() ? true : false;
    }

    static public function checkByPhoneAndUserId($phone, $userId) {
        global $db;
        $res = $db->query(
            "SELECT c.id FROM clients c WHERE c.phone=? AND c.user_id=?",
            [$phone, $userId]
        );
        $client = $res->fetch();
        return $client ? $client['id'] : false;
    }
    static public function getCount($userId) {
        global $db;
        $res = $db->query(
            "SELECT COUNT(c.id) AS count FROM clients c WHERE c.user_id=?",
            [$userId]
        );
        $clients = $res->fetch();
        return $clients ? $clients['count'] : 0;
    }

    static public function removeImage($id, $image) {
        global $db;
        $res = $db->query("SELECT * FROM clients WHERE id=?", [$id]);
        $file = $res->fetch();
        //unlink('../file/'. $file[$image]);
    }

}
