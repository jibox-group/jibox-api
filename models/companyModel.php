<?php
class COMPANY {
    
    static public function create($name) {
        global $db;
        $res = $db->query(
            "INSERT INTO companies SET
                `name`=?",
            [$name]
        );
        if($res) {
            $r = $db->query(
                "SELECT c.id
            FROM companies c
            ORDER BY id DESC LIMIT 0, 1"
            );
            $company = $r->fetch();
            return $company['id'];
        }
        else return false;
    }
    
    static public function getAll() {
        global $db, $fun, $req;
        $where = " WHERE 1=1";
        $pagination = "";
        
        if($req['keyword']){
            $where .= " AND c.name LIKE '%".$req['keyword']."%'";
        }
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }
        $resCount = $db->query("SELECT COUNT(id) as count FROM companies c".$where);
        $count = $resCount->fetch();
        $res = $db->query("SELECT * FROM companies c".$where.$pagination);
        $result = [
            "count" => $count['count'],
            "items" => $res->fetchAll()
        ];
        return $result;
    }
    
    static public function get($id) {
        global $db;
        $res = $db->query(
            "SELECT *
            FROM companies c
            WHERE c.id=?",
            [$id]
        );
        $res = $res->fetch();
        return $res ? $res : false;
    }
    
    static public function delete($id) {
        global $db;
        $res = $db->query(
            "DELETE FROM companies
            WHERE id=?",
            [$id]
        );
        return $res ? true : false;
    }
    
    static public function update($id, $name) {
        global $db;
        $res = $db->query(
            "UPDATE companies SET name=?
            WHERE id=?",
            [$name, $id]
        );
        return $res ? true : false;
    }

    static public function getName($id) {
        global $db;
        $res = $db->query(
            "SELECT c.name
            FROM companies c
            WHERE c.id=?",
            [$id]
        );
        $company = $res->fetch();
        if($company) {
            return $company['name'];
        }
        else return null;
    }

}
