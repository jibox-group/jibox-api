<?php
class REGION {

    static public function create($name) {
        global $db;
        $res = $db->query(
            "INSERT INTO regions SET
                `name`=?",
            [$name]
        );
        if($res) {
            $r = $db->query(
                "SELECT r.id
            FROM regions r
            ORDER BY id DESC LIMIT 0, 1"
            );
            $region = $r->fetch();
            return $region['id'];
        }
        else return false;
    }

    static public function getAll() {
        global $db, $fun, $req;
        $where = " WHERE 1=1";
        $pagination = "";

        if($req['keyword']){
            $where .= " AND r.name LIKE '%".$req['keyword']."%'";
        }
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }
        $resCount = $db->query("SELECT COUNT(id) as count FROM regions r".$where);
        $count = $resCount->fetch();
        $res = $db->query("SELECT * FROM regions r".$where.$pagination);
        $result = [
            "count" => $count['count'],
            "items" => $res->fetchAll()
        ];
        return $result;
    }

    static public function get($id) {
        global $db;
        $res = $db->query(
            "SELECT *
            FROM regions r
            WHERE r.id=?",
            [$id]
        );
        $region = $res->fetch();
        if($region) {
            return $region;
        }
        else return null;
    }

    static public function getName($id) {
        global $db;
        $res = $db->query(
            "SELECT r.name
            FROM regions r
            WHERE r.id=?",
            [$id]
        );
        $region = $res->fetch();
        if($region) {
            return $region['name'];
        }
        else return null;
    }

    static public function delete($id) {
        global $db;
        $res = $db->query(
            "DELETE FROM regions
            WHERE id=?",
            [$id]
        );
        return $res ? true : false;
    }

    static public function update($id, $name) {
        global $db;
        $res = $db->query(
            "UPDATE regions SET name=?
            WHERE id=?",
            [$name, $id]
        );
        return $res ? true : false;
    }

}
