<?php
class ESTATE {

    static public function getStatistics($owner) {
        global $db, $statistics;
        $res = $statistics;

        $temp = [12][31];
        $r = $db->query(
            "SELECT
                    e.*
                 FROM estates e
                  JOIN clients c ON c.id = e.owner
                  WHERE c.user_id=?", [$owner]
        );
        $estates = $r->fetchAll();
        foreach($estates as $i => $estate) {
            $time = floor($estate['date_create'] / 1000);
            $date = explode(" ", date("Y m d", $time));
            if($date[0] == date("Y")){
                $m = $date[1] * 1;
                $d = $date[2] * 1;
                $temp[$m][$d] += 1;
            }
        }
        foreach ($temp as $i => $item) {
            foreach ($item as $j => $value) {
                switch ($i) {
                    case 1 : $res['januaryCount'] += $value; $res['january'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 2 : $res['februaryCount'] += $value; $res['february'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 3 : $res['marchCount'] += $value; $res['march'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 4 : $res['aprilCount'] += $value; $res['april'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 5 : $res['mayCount'] += $value; $res['may'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 6 : $res['juneCount'] += $value; $res['june'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 7 : $res['julyCount'] += $value; $res['july'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 8 : $res['augustCount'] += $value; $res['august'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 9 : $res['septemberCount'] += $value; $res['september'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 10 : $res['octoberCount'] += $value; $res['october'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 11 : $res['novemberCount'] += $value; $res['november'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 12 : $res['decemberCount'] += $value; $res['december'][$j-1] = ["day" => $j, "count" => $value]; break;
                }
            }
        }
        return $res;
    }

    static public function deleteByClientId($clientId) {
        global $db;
        $res = $db->query("DELETE FROM estates WHERE owner=?", [$clientId]);
        return $res ? true : false;
    }
    
    static public function delete($id) {
        global $db;
        $res = $db->query("DELETE FROM estates WHERE id=?", [$id]);
        return $res ? true : false;
    }

    static public function getCount($userId) {
        global $db;
        $res = $db->query(
            "SELECT id FROM clients c WHERE c.user_id=?",
            [$userId]
        );
        $clients = $res->fetchAll();
        $clientIds = [];
        foreach ($clients as $i => $client) {
            array_push($clientIds, $client['id']);
        }
        $res = $db->query(
            "SELECT COUNT(e.id) AS count FROM estates e WHERE e.owner IN (".implode(',', $clientIds).") AND e.status<>'delete'"
        );
        $resCount = $res->fetch();
        return $resCount['count'];
    }

    static public function changeStatus($id, $status, $owner = 0) {
        global $db, $req, $fun;
        $time = $fun->time();
        $cust = '';
        $image = '';
        $params = [$status];
        if($req['customer']){
            $cust = ", customer=?";
            array_push($params, $req['customer']);
        }
        if($req['stopTime']){
            $stopTime = ", stopTime=?";
            array_push($params, $req['stopTime']);
        }
        if($status == "delete" || $status == "archive"){
            $image = ", image=?, image1=?, image2=?, image3=?, image4=?, image5=?, image6=?, image7=?, image8=?, image9=?, image10=?, image11=?, access_type=?";
            array_push($params, NULL);
            array_push($params, NULL);
            array_push($params, NULL);
            array_push($params, NULL);
            array_push($params, NULL);
            array_push($params, NULL);
            array_push($params, NULL);
            array_push($params, NULL);
            array_push($params, NULL);
            array_push($params, NULL);
            array_push($params, NULL);
            array_push($params, NULL);
            array_push($params, 'private');
        } else if ($status == "rent") {
            RENT::create($id, $owner, $req['customer'], $req['stopTime']);
        } else if ($status == "sell") {
            SELL::create($id, $owner, $req['customer']);
        }
        array_push($params, $time);
        array_push($params, $id);
        $res = $db->query(
            "UPDATE estates SET status=?".$cust.$stopTime.$image.", date_update=? WHERE id=?",
            $params
        );
        return $res ? true : false;
    }
    static public function addCustomer($id, $customer) {
        global $db, $req, $fun;
        $time = $fun->time();
        $res = $db->query(
            "SELECT interesting_clients FROM estates WHERE id=?",
            [$id]
        );
        $estate = $res->fetch();
        $customers = [];
        if($estate['interesting_clients']) {
            $customers = explode(',', $estate['interesting_clients']);
            foreach($customers as $key => $cust) {
                if($cust == $customer){
                    return false;
                }
            }
        }

        array_push($customers, $customer);

        $res = $db->query(
            "UPDATE estates SET interesting_clients=?, date_update=? WHERE id=?",
            [implode(',', $customers), $time, $id]
        );
        return $res ? true : false;
    }
    static public function changeFavourite($id, $favorite = false) {
        global $db, $fun;
        $time = $fun->time();

        $res = $db->query(
            "UPDATE estates SET".
            ($favorite ? " favorite=1," : " favorite=0,").
            " date_update=? WHERE id=?",
            [$time, $id]
        );
        return $res ? true : false;
    }
    
    static public function delCustomer($id, $customer) {
        global $db, $req, $fun;
        $time = $fun->time();
        $res = $db->query(
            "SELECT interesting_clients FROM estates WHERE id=?",
            [$id]
        );
        $estate = $res->fetch();
        $customers = explode(',', $estate['interesting_clients']);

        foreach($customers as $key => $cust){
            if($cust == $customer || $cust*1 == 0){
                array_splice($customers, $key, 1);
            }
        }

        $res = $db->query(
            "UPDATE estates SET interesting_clients=?, date_update=? WHERE id=?",
            [implode(',', $customers), $time, $id]
        );
        return $res ? true : false;
    }
    
    static public function clearCustomer($id) {
        global $db, $req, $fun;
        $time = $fun->time();

        $res = $db->query(
            "UPDATE estates SET interesting_clients=?, date_update=? WHERE id=?",
            ['', $time, $id]
        );
        return $res ? true : false;
    }

    static public function get($userId, $id) {
        global $db, $fun;
        $parameters = array();

        array_push($parameters, $userId);
        array_push($parameters, $id);

        $res = $db->query(
            "SELECT
                    e.*,
                    c.id AS clientId,
                    c.image AS clientImage,
                    c.fullname AS clientName,
                    c.phone,
                    c.phone1,
                    c.phone2
                 FROM estates e
                  JOIN clients c ON c.id = e.owner
                  WHERE c.user_id=? AND e.id=?",
            $parameters
        );

        $estate = $res->fetch();

        if ($estate) {
            $preference = [
                "buy" => $estate['buy'] ? true : false,
                "buy_price" => $estate['buy_price'],
                "buy_type" => $estate['buy_type'],
                "renter_long" => $estate['renter_long'] ? true : false,
                "renter_long_price" => $estate['renter_long_price'],
                "renter_long_type" => $estate['renter_long_type'],
                "renter_short" => $estate['renter_short'] ? true : false,
                "renter_short_price" => $estate['renter_short_price'],
                "renter_short_type" => $estate['renter_short_type'],
                "barter" => $estate['barter'] ? true : false
            ];
            $advenced = [
                "adven1" => $estate['adven1'] ? true : false,
                "adven2" => $estate['adven2'] ? true : false,
                "adven3" => $estate['adven3'] ? true : false,
                "adven4" => $estate['adven4'] ? true : false,
                "adven5" => $estate['adven5'] ? true : false,
                "adven6" => $estate['adven6'] ? true : false,
                "adven7" => $estate['adven7'] ? true : false,
                "adven8" => $estate['adven8'] ? true : false,
                "adven9" => $estate['adven9'] ? true : false,
                "adven10" => $estate['adven10'] ? true : false,
                "adven11" => $estate['adven11'] ? true : false,
                "adven12" => $estate['adven12'] ? true : false,
                "adven13" => $estate['adven13'] ? true : false,
                "adven14" => $estate['adven14'] ? true : false,
                "adven15" => $estate['adven15'] ? true : false,
                "adven16" => $estate['adven16'] ? true : false,
                "adven17" => $estate['adven17'] ? true : false,
                "adven18" => $estate['adven18'] ? true : false,
                "adven19" => $estate['adven19'] ? true : false,
                "adven20" => $estate['adven20'] ? true : false,
                "adven21" => $estate['adven21'] ? true : false,
                "adven22" => $estate['adven22'] ? true : false,
                "adven23" => $estate['adven23'] ? true : false
            ];
            $images = [
                "image" => $estate['image'] && $estate['image'] != "deleted" ? $estate['image'] : null,
                "image1" => $estate['image1'] && $estate['image1'] != "deleted" ? $estate['image1'] : null,
                "image2" => $estate['image2'] && $estate['image2'] != "deleted" ? $estate['image2'] : null,
                "image3" => $estate['image3'] && $estate['image3'] != "deleted" ? $estate['image3'] : null,
                "image4" => $estate['image4'] && $estate['image4'] != "deleted" ? $estate['image4'] : null,
                "image5" => $estate['image5'] && $estate['image5'] != "deleted" ? $estate['image5'] : null,
                "image6" => $estate['image6'] && $estate['image6'] != "deleted" ? $estate['image6'] : null,
                "image7" => $estate['image7'] && $estate['image7'] != "deleted" ? $estate['image7'] : null,
                "image8" => $estate['image8'] && $estate['image8'] != "deleted" ? $estate['image8'] : null,
                "image9" => $estate['image9'] && $estate['image9'] != "deleted" ? $estate['image9'] : null,
                "image10" => $estate['image10'] && $estate['image10'] != "deleted" ? $estate['image10'] : null,
                "image11" => $estate['image11'] && $estate['image11'] != "deleted" ? $estate['image11'] : null
            ];
            $estate['favorite'] = $estate['favorite'] ? true : false;
            $estate['object_from_owner'] = $estate['object_from_owner'] ? true : false;
            $estate['commerceBase'] = $estate['commerce_base'] ? true : false;
            $estate['commerceBuilding'] = $estate['commerce_building'] ? true : false;
            $estate['commerceButik'] = $estate['commerce_butik'] ? true : false;
            $estate['commerceMake'] = $estate['commerce_make'] ? true : false;
            $estate['commerceOffice'] = $estate['commerce_office'] ? true : false;
            $estate['commerceOther'] = $estate['commerce_other'] ? true : false;
            $estate['commercePublicKitchen'] = $estate['commerce_public_kitchen'] ? true : false;
            $estate['commerceSalon'] = $estate['commerce_salon'] ? true : false;
            $estate['commerceTemporaryBuilding'] = $estate['commerce_temporary_building'] ? true : false;
            $estate['regionId'] = $estate['region_id'];
            $estate['districtId'] = $estate['district_id'];
            $estate['quarterId'] = $estate['quarter_id'];
            $estate['metroId'] = $estate['underground_id'];
            $estate['roomCount'] = $estate['room_count'];
            $estate['objectTimeType'] = $estate['object_time_type'];
            $estate['objectMaterialType'] = $estate['object_material_type'];
            $estate['objectCondition'] = $estate['object_condition'];
            $estate['accessType'] = $estate['access_type'];
            $estate['areaHome'] = $estate['area_home'];
            $estate['areaTotal'] = $estate['area_total'];
            $estate['buildMaterial'] = $estate['build_material'];
            $estate['buildPlan'] = $estate['build_plan'];
            $estate['buildYear'] = $estate['build_year'];
            $estate['currencyId'] = $estate['currency_id'];
            $estate['doneArea'] = $estate['done_area'];
            $estate['floorCount'] = $estate['floor_count'];
            $estate['landArea'] = $estate['land_area'];
            $estate['maxPage'] = $estate['max_page'];
            $estate['region'] = REGION::getName($estate['region_id']);
            $estate['district'] = DISTRICT::getName($estate['district_id']);
            $estate['quarter'] = QUARTER::getName($estate['quarter_id']);
            $estate['underground'] = UNDERGROUND::getName($estate['underground_id']);
            $estate['advenced'] = $advenced;
            $estate['preference'] = $preference;
            $estate['images'] = $images;
            $customer = CLIENT::get($userId, $estate['customer']);
            $estate['customerName'] = $customer['fullname'];
            $estate['customerId'] = $customer['id'];
            $estate['customerPhoto'] = $customer['image'];
            $interestingClients = explode(',', $estate['interesting_clients']);
            $estate['interestingCount'] = count($interestingClients);
            foreach($interestingClients as $i => $clientId){
                if($clientId * 1){
                    $interestingClients[$i] = $clientId * 1;
                } else {
                    $estate['interestingCount']--;
                }
            }
            return $estate;
        } else return false;
    }

    static public function getOther($userId, $id) {
        global $db, $fun;
        $whereIds = " WHERE e.id=? AND NOT e.access_type='private'";
        $res = $db->query(
            "SELECT
                    e.*,
                    u.id AS userId,
                    u.image AS userImage,
                    u.fullname AS userName,
                    u.phone AS userPhone
                 FROM estates e
                  JOIN clients c ON c.id = e.owner
                  JOIN users u ON c.user_id = u.id" .
            $whereIds,
            [$id]
        );

        $estate = $res->fetch();

        if ($estate) {
            $preference = [
                "buy" => $estate['buy'] ? true : false,
                "buy_price" => $estate['buy_price'],
                "buy_type" => $estate['buy_type'],
                "renter_long" => $estate['renter_long'] ? true : false,
                "renter_long_price" => $estate['renter_long_price'],
                "renter_long_type" => $estate['renter_long_type'],
                "renter_short" => $estate['renter_short'] ? true : false,
                "renter_short_price" => $estate['renter_short_price'],
                "renter_short_type" => $estate['renter_short_type'],
                "barter" => $estate['barter'] ? true : false
            ];
            $advenced = [
                "adven1" => $estate['adven1'] ? true : false,
                "adven2" => $estate['adven2'] ? true : false,
                "adven3" => $estate['adven3'] ? true : false,
                "adven4" => $estate['adven4'] ? true : false,
                "adven5" => $estate['adven5'] ? true : false,
                "adven6" => $estate['adven6'] ? true : false,
                "adven7" => $estate['adven7'] ? true : false,
                "adven8" => $estate['adven8'] ? true : false,
                "adven9" => $estate['adven9'] ? true : false,
                "adven10" => $estate['adven10'] ? true : false,
                "adven11" => $estate['adven11'] ? true : false,
                "adven12" => $estate['adven12'] ? true : false,
                "adven13" => $estate['adven13'] ? true : false,
                "adven14" => $estate['adven14'] ? true : false,
                "adven15" => $estate['adven15'] ? true : false,
                "adven16" => $estate['adven16'] ? true : false,
                "adven17" => $estate['adven17'] ? true : false,
                "adven18" => $estate['adven18'] ? true : false,
                "adven19" => $estate['adven19'] ? true : false,
                "adven20" => $estate['adven20'] ? true : false,
                "adven21" => $estate['adven21'] ? true : false,
                "adven22" => $estate['adven22'] ? true : false,
                "adven23" => $estate['adven23'] ? true : false
            ];
            $images = [
                "image" => null,
                "image1" => null,
                "image2" => $estate['image2'] && $estate['image2'] != "deleted" ? $estate['image2'] : null,
                "image3" => $estate['image3'] && $estate['image3'] != "deleted" ? $estate['image3'] : null,
                "image4" => $estate['image4'] && $estate['image4'] != "deleted" ? $estate['image4'] : null,
                "image5" => $estate['image5'] && $estate['image5'] != "deleted" ? $estate['image5'] : null,
                "image6" => $estate['image6'] && $estate['image6'] != "deleted" ? $estate['image6'] : null,
                "image7" => $estate['image7'] && $estate['image7'] != "deleted" ? $estate['image7'] : null,
                "image8" => $estate['image8'] && $estate['image8'] != "deleted" ? $estate['image8'] : null,
                "image9" => $estate['image9'] && $estate['image9'] != "deleted" ? $estate['image9'] : null,
                "image10" => $estate['image10'] && $estate['image10'] != "deleted" ? $estate['image10'] : null,
                "image11" => $estate['image11'] && $estate['image11'] != "deleted" ? $estate['image11'] : null
            ];
            $estate['favorite'] = $estate['favorite'] ? true : false;
            $estate['object_from_owner'] = $estate['object_from_owner'] ? true : false;
            $estate['commerceBase'] = $estate['commerce_base'] ? true : false;
            $estate['commerceBuilding'] = $estate['commerce_building'] ? true : false;
            $estate['commerceButik'] = $estate['commerce_butik'] ? true : false;
            $estate['commerceMake'] = $estate['commerce_make'] ? true : false;
            $estate['commerceOffice'] = $estate['commerce_office'] ? true : false;
            $estate['commerceOther'] = $estate['commerce_other'] ? true : false;
            $estate['commercePublicKitchen'] = $estate['commerce_public_kitchen'] ? true : false;
            $estate['commerceSalon'] = $estate['commerce_salon'] ? true : false;
            $estate['commerceTemporaryBuilding'] = $estate['commerce_temporary_building'] ? true : false;
            $estate['regionId'] = $estate['region_id'];
            $estate['districtId'] = $estate['district_id'];
            $estate['quarterId'] = $estate['quarter_id'];
            $estate['metroId'] = $estate['underground_id'];
            $estate['roomCount'] = $estate['room_count'];
            $estate['objectTimeType'] = $estate['object_time_type'];
            $estate['objectMaterialType'] = $estate['object_material_type'];
            $estate['objectCondition'] = $estate['object_condition'];
            $estate['accessType'] = $estate['access_type'];
            $estate['areaHome'] = $estate['area_home'];
            $estate['areaTotal'] = $estate['area_total'];
            $estate['buildMaterial'] = $estate['build_material'];
            $estate['buildPlan'] = $estate['build_plan'];
            $estate['buildYear'] = $estate['build_year'];
            $estate['currencyId'] = $estate['currency_id'];
            $estate['doneArea'] = $estate['done_area'];
            $estate['floorCount'] = $estate['floor_count'];
            $estate['landArea'] = $estate['land_area'];
            $estate['maxPage'] = $estate['max_page'];
            $estate['region'] = REGION::getName($estate['region_id']);
            $estate['district'] = DISTRICT::getName($estate['district_id']);
            $estate['quarter'] = QUARTER::getName($estate['quarter_id']);
            $estate['underground'] = UNDERGROUND::getName($estate['underground_id']);
            $friend = FRIEND::detail($estate['userId'], $userId);
            $estate['userRelation'] = $friend ? $friend['type'] : 'other';
            $estate['advenced'] = $advenced;
            $estate['preference'] = $preference;
            if($estate['object_from_owner']) {
                $estate['clientName'] = CLIENT::getName($estate['owner']);
                $estate['clientPhone'] = CLIENT::getPhone($estate['owner']);
            }
            $estate['owner'] = null;
            $estate['address'] = null;
            $estate['image'] = null;
            $estate['image1'] = null;
            $estate['images'] = $images;
            // $estate['interesting_clients'] = explode(',', $estate['interesting_clients']);
            return $estate;
        } else return false;
    }

    /**
     * @param $userId
     * @return bool
     */
    static public function getAll($userId) {
        global $db, $req, $fun, $body;
        $parameters = array();
        $pagination = '';
        $filterPrice = '';
        $filterDistrict = '';
        $filterQuarter = '';
        $filterStatus = '';
        $filterCommerce = '';
    
        if($body->sort && $body->direction) {
            $sort = " ORDER BY ".$body->sort." ".$body->direction;
            if($body->sort == 'price') {
                if ($body->preference->renter_long || $body->preference->renter_short) {
                    $sort = " ORDER BY e.renter_long_price ".$body->direction;
                } else {
                    $sort = " ORDER BY e.buy_price ".$body->direction;
                }
            };
        } else {
            $sort = " ORDER BY date_update DESC";
        }
        
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }
        array_push($parameters, $userId);


        if($body->priceFrom || $body->priceTo){
            $currencyLocaleId = $body->currencyId;
            $currencyId = $currencyLocaleId == 1 ? 2 : 1;
            $ratio = CURRENCY::getRatio(2);
            $body->priceFrom = $body->priceFrom ? $body->priceFrom : 0;
            $priceLocaleFrom = $body->priceFrom;
            $body->priceTo = $body->priceTo ? $body->priceTo : 99999999999;
            $priceLocaleTo = $body->priceTo;
            if($currencyLocaleId == 2) {
                $priceFrom = ceil($body->priceFrom / $ratio);
                $priceTo = ceil($body->priceTo / $ratio);
            } else {
                $priceFrom = ceil($body->priceFrom * $ratio);
                $priceTo = ceil($body->priceTo * $ratio);
            }
            $pf = $priceFrom ? $priceFrom * 1 > 0 : false;
            $pt = $priceTo ? $priceTo * 1 > 0 : false;
            $plf = $priceLocaleFrom ? $priceLocaleFrom * 1 > 0 : false;
            $plt = $priceLocaleTo ? $priceLocaleTo * 1 > 0 : false;
            if($body->preference->buy == true){
                $filterPrice = " AND ((".
                    ($plf && $plt ? "e.buy_price >= ".$priceLocaleFrom." AND e.buy_price <= ".$priceLocaleTo : (
                    $plf ? "e.buy_price >= ".$priceLocaleFrom." AND e.buy_price <= 99999999999" : (
                    $plt ? "e.buy_price <= ".$priceLocaleTo." AND e.buy_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                    ($pf && $pt ? "e.buy_price >= ".$priceFrom." AND e.buy_price <= ".$priceTo : (
                    $pf ? "e.buy_price >= ".$priceFrom." AND e.buy_price <= 99999999999" : (
                    $pt ? "e.buy_price <= ".$priceTo." AND e.buy_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyId."))";
            } else if($body->preference->renter_long == true || $body->preference->renter_short == true) {
                $filterPrice = " AND ((".
                    ($plf && $plt ? "e.renter_long_price >= ".$priceLocaleFrom." AND e.renter_long_price <= ".$priceLocaleTo : (
                    $plf ? "e.renter_long_price >= ".$priceLocaleFrom." AND e.renter_long_price <= 99999999999" : (
                    $plt ? "e.renter_long_price <= ".$priceLocaleTo." AND e.renter_long_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                    ($pf && $pt ? "e.renter_long_price >= ".$priceFrom." AND e.renter_long_price <= ".$priceTo : (
                    $pf ? "e.renter_long_price >= ".$priceFrom." AND e.renter_long_price <= 99999999999" : (
                    $pt ? "e.renter_long_price <= ".$priceTo." AND e.renter_long_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyId.") OR (".
                    ($plf && $plt ? "e.renter_short_price >= ".$priceLocaleFrom." AND e.renter_short_price <= ".$priceLocaleTo : (
                    $plf ? "e.renter_short_price >= ".$priceLocaleFrom." AND e.renter_short_price <= 99999999999" : (
                    $plt ? "e.renter_short_price <= ".$priceLocaleTo." AND e.renter_short_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                    ($pf && $pt ? "e.renter_short_price >= ".$priceFrom." AND e.renter_short_price <= ".$priceTo : (
                    $pf ? "e.renter_short_price >= ".$priceFrom." AND e.renter_short_price <= 99999999999" : (
                    $pt ? "e.renter_short_price <= ".$priceTo." AND e.renter_short_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyId."))";
            } else {
                $filterPrice = " AND ((".
                    ($plf && $plt ? "e.buy_price >= ".$priceLocaleFrom." AND e.buy_price <= ".$priceLocaleTo : (
                    $plf ? "e.buy_price >= ".$priceLocaleFrom." AND e.buy_price <= 99999999999" : (
                    $plt ? "e.buy_price <= ".$priceLocaleTo." AND e.buy_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                    ($pf && $pt ? "e.buy_price >= ".$priceFrom." AND e.buy_price <= ".$priceTo : (
                    $pf ? "e.buy_price >= ".$priceFrom." AND e.buy_price <= 99999999999" : (
                    $pt ? "e.buy_price <= ".$priceTo." AND e.buy_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyId.") OR (".
                    ($plf && $plt ? "e.renter_long_price >= ".$priceLocaleFrom." AND e.renter_long_price <= ".$priceLocaleTo : (
                    $plf ? "e.renter_long_price >= ".$priceLocaleFrom." AND e.renter_long_price <= 99999999999" : (
                    $plt ? "e.renter_long_price <= ".$priceLocaleTo." AND e.renter_long_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                    ($pf && $pt ? "e.renter_long_price >= ".$priceFrom." AND e.renter_long_price <= ".$priceTo : (
                    $pf ? "e.renter_long_price >= ".$priceFrom." AND e.renter_long_price <= 99999999999" : (
                    $pt ? "e.renter_long_price <= ".$priceTo." AND e.renter_long_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyId.") OR (".
                    ($plf && $plt ? "e.renter_short_price >= ".$priceLocaleFrom." AND e.renter_short_price <= ".$priceLocaleTo : (
                    $plf ? "e.renter_short_price >= ".$priceLocaleFrom." AND e.renter_short_price <= 99999999999" : (
                    $plt ? "e.renter_short_price <= ".$priceLocaleTo." AND e.renter_short_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                    ($pf && $pt ? "e.renter_short_price >= ".$priceFrom." AND e.renter_short_price <= ".$priceTo : (
                    $pf ? "e.renter_short_price >= ".$priceFrom." AND e.renter_short_price <= 99999999999" : (
                    $pt ? "e.renter_short_price <= ".$priceTo." AND e.renter_short_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyId."))";
            }
        }

        if(count($body->quarterId) > 0){
            $filterQuarter = " AND (";
            foreach($body->quarterId as $key => $quarterId){
                $filterQuarter .= " e.quarter_id=".$quarterId;
                if(count($body->quarterId) != $key + 1){
                    $filterQuarter .= " OR";
                }
            }
            $filterQuarter .= ")";
        }
        if(count($body->districtId) > 0 && $filterQuarter == ''){
            $filterDistrict = " AND (";
            foreach($body->districtId as $key => $districtId){
                $filterDistrict .= " e.district_id=".$districtId;
                if(count($body->districtId) != $key + 1){
                    $filterDistrict .= " OR";
                }
            }
            $filterDistrict .= ")";
        }
        switch($body->objectStatus){
            case 'active' : $filterStatus = " AND (e.status='free') AND e.status<>'delete'"; break;
            case 'archive' : $filterStatus = " AND (e.status='archive') AND e.status<>'delete'"; break;
            case 'busy' : $filterStatus = " AND (e.status='rent' OR e.status='sell')"; break;
            default: $filterStatus = " AND e.status<>'delete'"; break;
        }

        if($body->commerceBase || $body->commerceBuilding || $body->commerceButik || $body->commerceMake || $body->commerceOffice || $body->commerceOther || $body->commercePublicKitchen || $body->commerceSalon || $body->commerceTemporaryBuilding){
            $filterCommerce = " AND (false" .
                ($body->commerceBase ? " OR e.commerce_base = 1" : "") .
                ($body->commerceBuilding ? " OR e.commerce_building = 1" : "") .
                ($body->commerceButik ? " OR e.commerce_butik = 1" : "") .
                ($body->commerceMake ? " OR e.commerce_make = 1" : "") .
                ($body->commerceOffice ? " OR e.commerce_office = 1" : "") .
                ($body->commerceOther ? " OR e.commerce_other = 1" : "") .
                ($body->commercePublicKitchen ? " OR e.commerce_public_kitchen = 1" : "") .
                ($body->commerceSalon ? " OR e.commerce_salon = 1" : "") .
                ($body->commerceTemporaryBuilding ? " OR e.commerce_temporary_building = 1" : "") .
                ")";
        }

        $filter = ($body->favorite ? " AND e.favorite = 1" : "") .
            ($body->objectId ? " AND e.id = ".$body->objectId : "") .
            ($body->type ? " AND e.type = '".$body->type."'" : "") .
            ($body->regionId ? " AND e.region_id = '".$body->regionId."'" : "") .
            ($body->metroId ? " AND e.underground_id = '".$body->metroId."'" : "") .
            ($body->status ? " AND e.status = '".$body->status."'" : "") .
            ($body->owner ? " AND e.owner = '".$body->owner."'" : "") .
            ($body->accessType ? " AND e.access_type = '".$body->accessType."'" : "") .
            ($body->landAreaFrom*1 > 0 && $body->landAreaTo*1 > 0 ? " AND (e.land_area >= ".($body->landAreaFrom*1)." AND e.land_area <= ".($body->landAreaTo*1).")" : (
            $body->landAreaFrom*1 > 0 ? " AND (e.land_area >= ".($body->landAreaFrom*1)." AND e.land_area <= 999999999)" : (
            $body->landAreaTo*1 > 0 ? " AND (e.land_area >= 1 AND e.land_area <= ".($body->landAreaTo*1).")" : ""
            )
            )) .
            ($body->totalAreaFrom*1 > 0 && $body->totalAreaTo*1 > 0 ? " AND (e.area_total >= ".($body->totalAreaFrom*1)." AND e.area_total <= ".($body->totalAreaTo*1).")" : (
            $body->totalAreaFrom*1 > 0 ? " AND (e.area_total >= ".($body->totalAreaFrom*1)." AND e.area_total <= 999999999)" : (
            $body->totalAreaTo*1 > 0 ? " AND (e.area_total >= 1 AND e.area_total <= ".($body->totalAreaTo*1).")" : ""
            )
            )) .
            ($body->floorFrom*1 > 0 && $body->floorTo*1 > 0 ? " AND (e.floor >= ".($body->floorFrom*1)." AND e.floor <= ".($body->floorTo*1).")" : (
            $body->floorFrom*1 > 0 ? " AND (e.floor >= ".($body->floorFrom*1)." AND e.floor <= 999999999)" : (
            $body->floorTo*1 > 0 ? " AND (e.floor >= 1 AND e.floor <= ".($body->floorTo*1).")" : ""
            )
            )) .
            ($body->floorCountFrom*1 > 0 && $body->floorCountTo*1 > 0 ? " AND (e.floor_count >= ".($body->floorCountFrom*1)." AND e.floor_count <= ".($body->floorCountTo*1).")" : (
            $body->floorCountFrom*1 > 0 ? " AND (e.floor_count >= ".($body->floorCountFrom*1)." AND e.floor_count <= 999999999)" : (
            $body->floorCountTo*1 > 0 ? " AND (e.floor_count >= 1 AND e.floor_count <= ".($body->floorCountTo*1).")" : ""
            )
            )) .
            ($body->roomCountFrom*1 > 0 && $body->roomCountTo*1 > 0 ? " AND (e.room_count >= ".($body->roomCountFrom*1)." AND e.room_count <= ".($body->roomCountTo*1).")" : (
            $body->roomCountFrom*1 > 0 ? " AND (e.room_count >= ".($body->roomCountFrom*1)." AND e.room_count <= 999999999)" : (
            $body->roomCountTo*1 > 0 ? " AND (e.room_count >= 1 AND e.room_count <= ".($body->roomCountTo*1).")" : ""
            )
            )) .
            ($body->objectTimeType ? " AND e.object_time_type = '".$body->objectTimeType."'" : "") .
            ($body->objectCondition ? " AND e.object_condition = '".$body->objectCondition."'" : "") .
            ($body->preference->buy == true ? " AND e.buy = 1" : "") .
            ($body->preference->renter_long == true || $body->preference->renter_short == true ? " AND (e.renter_long = 1 OR e.renter_short = 1)" : "") .
            ($body->preference->barter == true ? " AND e.barter = 1" : "") .
            ($body->goal > 0 ? " AND e.goal = '".($body->goal - 1)."'" : "") .
            ($body->onlyWithoutImage ? " AND (e.image2 IS NOT NULL OR e.image3 IS NOT NULL OR e.image4 IS NOT NULL OR e.image5 IS NOT NULL OR e.image6 IS NOT NULL OR e.image7 IS NOT NULL OR e.image8 IS NOT NULL OR e.image9 IS NOT NULL OR e.image10 IS NOT NULL OR e.image11 IS NOT NULL) " : "") .
            ($body->keyword ? " AND (e.address LIKE '%".$body->keyword."%' OR e.comment LIKE '%".$body->keyword."%' OR e.id LIKE '%".$body->keyword."%' OR c.phone LIKE '%".$body->keyword."%' OR c.phone1 LIKE '%".$body->keyword."%' OR c.phone2 LIKE '%".$body->keyword."%' OR c.fullname LIKE '%".$body->keyword."%')" : "") .
            $filterQuarter .
            $filterDistrict .
            $filterCommerce .
            $filterStatus .
            $filterPrice;
    
    
        if($body->onlyInterested) {
            $filter = " AND e.interesting_clients LIKE '%".$body->owner."%' AND e.interesting_clients IS NOT NULL";
        }
    
        if($body->object_from_owner) {
            $filter = " AND e.object_from_owner = 1";
        }

        $countRes = $db->query(
            "SELECT
                    COUNT(e.id) AS count
                 FROM estates e
                  JOIN clients c ON c.id = e.owner
                  WHERE c.user_id=? AND e.status<>'delete'".
            $filter,
            $parameters
        );

        $count = $countRes->fetch();

        $res = $db->query(
            "SELECT
                    e.*
                 FROM estates e
                  JOIN clients c ON c.id = e.owner
                  WHERE c.user_id=? AND e.status<>'delete'" .
            $filter .
            $sort .
            $pagination,
            $parameters
        );

        if ($res) {
            $estates = [];
            while ($estate = $res->fetch()) {
                $preference = [
                    "buy" => $estate['buy'] ? true : false,
                    "buy_price" => $estate['buy_price'],
                    "buy_type" => $estate['buy_type'],
                    "renter_long" => $estate['renter_long'] ? true : false,
                    "renter_long_price" => $estate['renter_long_price'],
                    "renter_long_type" => $estate['renter_long_type'],
                    "renter_short" => $estate['renter_short'] ? true : false,
                    "renter_short_price" => $estate['renter_short_price'],
                    "renter_short_type" => $estate['renter_short_type'],
                    "barter" => $estate['barter'] ? true : false
                ];
                $advenced = [
                    "adven1" => $estate['adven1'] ? true : false,
                    "adven2" => $estate['adven2'] ? true : false,
                    "adven3" => $estate['adven3'] ? true : false,
                    "adven4" => $estate['adven4'] ? true : false,
                    "adven5" => $estate['adven5'] ? true : false,
                    "adven6" => $estate['adven6'] ? true : false,
                    "adven7" => $estate['adven7'] ? true : false,
                    "adven8" => $estate['adven8'] ? true : false,
                    "adven9" => $estate['adven9'] ? true : false,
                    "adven10" => $estate['adven10'] ? true : false,
                    "adven11" => $estate['adven11'] ? true : false,
                    "adven12" => $estate['adven12'] ? true : false,
                    "adven13" => $estate['adven13'] ? true : false,
                    "adven14" => $estate['adven14'] ? true : false,
                    "adven15" => $estate['adven15'] ? true : false,
                    "adven16" => $estate['adven16'] ? true : false,
                    "adven17" => $estate['adven17'] ? true : false,
                    "adven18" => $estate['adven18'] ? true : false,
                    "adven19" => $estate['adven19'] ? true : false,
                    "adven20" => $estate['adven20'] ? true : false,
                    "adven21" => $estate['adven21'] ? true : false,
                    "adven22" => $estate['adven22'] ? true : false,
                    "adven23" => $estate['adven23'] ? true : false
                ];
                $images = [
                    "image" => $estate['image'] && $estate['image'] != "deleted" ? $estate['image'] : null,
                    "image1" => $estate['image1'] && $estate['image1'] != "deleted" ? $estate['image1'] : null,
                    "image2" => $estate['image2'] && $estate['image2'] != "deleted" ? $estate['image2'] : null,
                    "image3" => $estate['image3'] && $estate['image3'] != "deleted" ? $estate['image3'] : null,
                    "image4" => $estate['image4'] && $estate['image4'] != "deleted" ? $estate['image4'] : null,
                    "image5" => $estate['image5'] && $estate['image5'] != "deleted" ? $estate['image5'] : null,
                    "image6" => $estate['image6'] && $estate['image6'] != "deleted" ? $estate['image6'] : null,
                    "image7" => $estate['image7'] && $estate['image7'] != "deleted" ? $estate['image7'] : null,
                    "image8" => $estate['image8'] && $estate['image8'] != "deleted" ? $estate['image8'] : null,
                    "image9" => $estate['image9'] && $estate['image9'] != "deleted" ? $estate['image9'] : null,
                    "image10" => $estate['image10'] && $estate['image10'] != "deleted" ? $estate['image10'] : null,
                    "image11" => $estate['image11'] && $estate['image11'] != "deleted" ? $estate['image11'] : null
                ];
                $estate['favorite'] = $estate['favorite'] ? true : false;
                $estate['object_from_owner'] = $estate['object_from_owner'] ? true : false;
                $estate['commerceBase'] = $estate['commerce_base'] ? true : false;
                $estate['commerceBuilding'] = $estate['commerce_building'] ? true : false;
                $estate['commerceButik'] = $estate['commerce_butik'] ? true : false;
                $estate['commerceMake'] = $estate['commerce_make'] ? true : false;
                $estate['commerceOffice'] = $estate['commerce_office'] ? true : false;
                $estate['commerceOther'] = $estate['commerce_other'] ? true : false;
                $estate['commercePublicKitchen'] = $estate['commerce_public_kitchen'] ? true : false;
                $estate['commerceSalon'] = $estate['commerce_salon'] ? true : false;
                $estate['commerceTemporaryBuilding'] = $estate['commerce_temporary_building'] ? true : false;
                $estate['regionId'] = $estate['region_id'];
                $estate['districtId'] = $estate['district_id'];
                $estate['quarterId'] = $estate['quarter_id'];
                $estate['metroId'] = $estate['underground_id'];
                $estate['roomCount'] = $estate['room_count'];
                $estate['objectTimeType'] = $estate['object_time_type'];
                $estate['objectMaterialType'] = $estate['object_material_type'];
                $estate['objectCondition'] = $estate['object_condition'];
                $estate['accessType'] = $estate['access_type'];
                $estate['areaHome'] = $estate['area_home'];
                $estate['buildMaterial'] = $estate['build_material'];
                $estate['buildPlan'] = $estate['build_plan'];
                $estate['buildYear'] = $estate['build_year'];
                $estate['currencyId'] = $estate['currency_id'];
                $estate['doneArea'] = $estate['done_area'];
                $estate['areaTotal'] = $estate['area_total'];
                $estate['floorCount'] = $estate['floor_count'];
                $estate['landArea'] = $estate['land_area'];
                $estate['maxPage'] = $estate['max_page'];
                $estate['region'] = REGION::getName($estate['region_id']);
                $estate['district'] = DISTRICT::getName($estate['district_id']);
                $estate['quarter'] = QUARTER::getName($estate['quarter_id']);
                $estate['underground'] = UNDERGROUND::getName($estate['underground_id']);
                $estate['advenced'] = $advenced;
                $estate['preference'] = $preference;
                $estate['images'] = $images;
                // $estate['interesting_clients'] = explode(',', $estate['interesting_clients']);
                array_push($estates, $estate);
            }
            $result['count'] = $count['count'];
            $result['items'] = $estates;
            return $result;
        } else return false;
    }

    /**
     * @return bool
     */
    static public function gets() {
        global $db, $req, $fun, $body, $header;
        $parameters = array();
        $pagination = '';
        $filterPrice = '';
        $filterDistrict = '';
        $filterQuarter = '';
        $filterStatus = '';
        $filterCommerce = '';

        if($body->sort && $body->direction) {
            $sort = " ORDER BY ".$body->sort." ".$body->direction;
        } else {
            $sort = " ORDER BY date_update DESC";
        }
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }


        if($body->priceFrom || $body->priceTo){
            $currencyLocaleId = $body->currencyId;
            $currencyId = $currencyLocaleId == 1 ? 2 : 1;
            $ratio = CURRENCY::getRatio(2);
            $priceLocaleFrom = $body->priceFrom;
            $priceLocaleTo = $body->priceTo;
            if($currencyLocaleId == 2) {
                $priceFrom = ceil($body->priceFrom / $ratio);
                $priceTo = ceil($body->priceTo / $ratio);
            } else {
                $priceFrom = ceil($body->priceFrom * $ratio);
                $priceTo = ceil($body->priceTo * $ratio);
            }
            $pf = $priceFrom ? $priceFrom * 1 > 0 : false;
            $pt = $priceTo ? $priceTo * 1 > 0 : false;
            $plf = $priceLocaleFrom ? $priceLocaleFrom * 1 > 0 : false;
            $plt = $priceLocaleTo ? $priceLocaleTo * 1 > 0 : false;
            if($body->preference->buy == true){
                $filterPrice = " AND ((".
                    ($plf && $plt ? "e.buy_price >= ".$priceLocaleFrom." AND e.buy_price <= ".$priceLocaleTo : (
                    $plf ? "e.buy_price >= ".$priceLocaleFrom." AND e.buy_price <= 99999999999" : (
                    $plt ? "e.buy_price <= ".$priceLocaleTo." AND e.buy_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                    ($pf && $pt ? "e.buy_price >= ".$priceFrom." AND e.buy_price <= ".$priceTo : (
                    $pf ? "e.buy_price >= ".$priceFrom." AND e.buy_price <= 99999999999" : (
                    $pt ? "e.buy_price <= ".$priceTo." AND e.buy_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyId."))";
            } else if($body->preference->renter_long == true || $body->preference->renter_short == true) {
                $filterPrice = " AND ((".
                    ($plf && $plt ? "e.renter_long_price >= ".$priceLocaleFrom." AND e.renter_long_price <= ".$priceLocaleTo : (
                    $plf ? "e.renter_long_price >= ".$priceLocaleFrom." AND e.renter_long_price <= 99999999999" : (
                    $plt ? "e.renter_long_price <= ".$priceLocaleTo." AND e.renter_long_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                    ($pf && $pt ? "e.renter_long_price >= ".$priceFrom." AND e.renter_long_price <= ".$priceTo : (
                    $pf ? "e.renter_long_price >= ".$priceFrom." AND e.renter_long_price <= 99999999999" : (
                    $pt ? "e.renter_long_price <= ".$priceTo." AND e.renter_long_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyId.") OR (".
                    ($plf && $plt ? "e.renter_short_price >= ".$priceLocaleFrom." AND e.renter_short_price <= ".$priceLocaleTo : (
                    $plf ? "e.renter_short_price >= ".$priceLocaleFrom." AND e.renter_short_price <= 99999999999" : (
                    $plt ? "e.renter_short_price <= ".$priceLocaleTo." AND e.renter_short_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                    ($pf && $pt ? "e.renter_short_price >= ".$priceFrom." AND e.renter_short_price <= ".$priceTo : (
                    $pf ? "e.renter_short_price >= ".$priceFrom." AND e.renter_short_price <= 99999999999" : (
                    $pt ? "e.renter_short_price <= ".$priceTo." AND e.renter_short_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyId."))";
            } else {
                $filterPrice = " AND ((".
                    ($plf && $plt ? "e.buy_price >= ".$priceLocaleFrom." AND e.buy_price <= ".$priceLocaleTo : (
                    $plf ? "e.buy_price >= ".$priceLocaleFrom." AND e.buy_price <= 99999999999" : (
                    $plt ? "e.buy_price <= ".$priceLocaleTo." AND e.buy_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                    ($pf && $pt ? "e.buy_price >= ".$priceFrom." AND e.buy_price <= ".$priceTo : (
                    $pf ? "e.buy_price >= ".$priceFrom." AND e.buy_price <= 99999999999" : (
                    $pt ? "e.buy_price <= ".$priceTo." AND e.buy_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyId.") OR (".
                    ($plf && $plt ? "e.renter_long_price >= ".$priceLocaleFrom." AND e.renter_long_price <= ".$priceLocaleTo : (
                    $plf ? "e.renter_long_price >= ".$priceLocaleFrom." AND e.renter_long_price <= 99999999999" : (
                    $plt ? "e.renter_long_price <= ".$priceLocaleTo." AND e.renter_long_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                    ($pf && $pt ? "e.renter_long_price >= ".$priceFrom." AND e.renter_long_price <= ".$priceTo : (
                    $pf ? "e.renter_long_price >= ".$priceFrom." AND e.renter_long_price <= 99999999999" : (
                    $pt ? "e.renter_long_price <= ".$priceTo." AND e.renter_long_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyId.") OR (".
                    ($plf && $plt ? "e.renter_short_price >= ".$priceLocaleFrom." AND e.renter_short_price <= ".$priceLocaleTo : (
                    $plf ? "e.renter_short_price >= ".$priceLocaleFrom." AND e.renter_short_price <= 99999999999" : (
                    $plt ? "e.renter_short_price <= ".$priceLocaleTo." AND e.renter_short_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                    ($pf && $pt ? "e.renter_short_price >= ".$priceFrom." AND e.renter_short_price <= ".$priceTo : (
                    $pf ? "e.renter_short_price >= ".$priceFrom." AND e.renter_short_price <= 99999999999" : (
                    $pt ? "e.renter_short_price <= ".$priceTo." AND e.renter_short_price >= 1" : ""
                    )
                    )) ." AND e.currency_id = ".$currencyId."))";
            }
        }

        if($body->quarterId) {
            if(count($body->quarterId) > 0){
                $filterQuarter = " AND (";
                foreach($body->quarterId as $key => $quarterId){
                    $filterQuarter .= " e.quarter_id=".$quarterId;
                    if(count($body->quarterId) != $key + 1){
                        $filterQuarter .= " OR";
                    }
                }
                $filterQuarter .= ")";
            }
        }
        if($body->districtId) {
            if(count($body->districtId) > 0 && $filterQuarter == ''){
                $filterDistrict = " AND (";
                foreach($body->districtId as $key => $districtId){
                    $filterDistrict .= " e.district_id=".$districtId;
                    if(count($body->districtId) != $key + 1){
                        $filterDistrict .= " OR";
                    }
                }
                $filterDistrict .= ")";
            }
        }

        switch($body->objectStatus){
            case 'active' : $filterStatus = " AND (e.status='free') AND e.status<>'delete'"; break;
            case 'archive' : $filterStatus = " AND (e.status='archive') AND e.status<>'delete'"; break;
            case 'busy' : $filterStatus = " AND (e.status='rent' OR e.status='sell')"; break;
            default: $filterStatus = " AND e.status<>'delete'"; break;
        }

        if($body->commerceBase || $body->commerceBuilding || $body->commerceButik || $body->commerceMake || $body->commerceOffice || $body->commerceOther || $body->commercePublicKitchen || $body->commerceSalon || $body->commerceTemporaryBuilding){
            $filterCommerce = " AND (false" .
                ($body->commerceBase ? " OR e.commerce_base = 1" : "") .
                ($body->commerceBuilding ? " OR e.commerce_building = 1" : "") .
                ($body->commerceButik ? " OR e.commerce_butik = 1" : "") .
                ($body->commerceMake ? " OR e.commerce_make = 1" : "") .
                ($body->commerceOffice ? " OR e.commerce_office = 1" : "") .
                ($body->commerceOther ? " OR e.commerce_other = 1" : "") .
                ($body->commercePublicKitchen ? " OR e.commerce_public_kitchen = 1" : "") .
                ($body->commerceSalon ? " OR e.commerce_salon = 1" : "") .
                ($body->commerceTemporaryBuilding ? " OR e.commerce_temporary_building = 1" : "") .
                ")";
        }

        $filter = ($body->favorite ? " AND e.favorite = 1" : "") .
            ($body->type ? " AND e.type = '".$body->type."'" : "") .
            ($body->regionId ? " AND e.region_id = '".$body->regionId."'" : "") .
            ($body->metroId ? " AND e.underground_id = '".$body->metroId."'" : "") .
            ($body->status ? " AND e.status = '".$body->status."'" : "") .
            ($body->owner ? " AND e.owner = '".$body->owner."'" : "") .
            ($body->accessType ? " AND e.access_type = '".$body->accessType."'" : "") .
            ($body->landAreaFrom*1 > 0 && $body->landAreaTo*1 > 0 ? " AND (e.land_area >= ".($body->landAreaFrom*1)." AND e.land_area <= ".($body->landAreaTo*1).")" : (
            $body->landAreaFrom*1 > 0 ? " AND (e.land_area >= ".($body->landAreaFrom*1)." AND e.land_area <= 999999999)" : (
            $body->landAreaTo*1 > 0 ? " AND (e.land_area >= 1 AND e.land_area <= ".($body->landAreaTo*1).")" : ""
            )
            )) .
            ($body->totalAreaFrom*1 > 0 && $body->totalAreaTo*1 > 0 ? " AND (e.area_total >= ".($body->totalAreaFrom*1)." AND e.area_total <= ".($body->totalAreaTo*1).")" : (
            $body->totalAreaFrom*1 > 0 ? " AND (e.area_total >= ".($body->totalAreaFrom*1)." AND e.area_total <= 999999999)" : (
            $body->totalAreaTo*1 > 0 ? " AND (e.area_total >= 1 AND e.area_total <= ".($body->totalAreaTo*1).")" : ""
            )
            )) .
            ($body->floorFrom*1 > 0 && $body->floorTo*1 > 0 ? " AND (e.floor >= ".($body->floorFrom*1)." AND e.floor <= ".($body->floorTo*1).")" : (
            $body->floorFrom*1 > 0 ? " AND (e.floor >= ".($body->floorFrom*1)." AND e.floor <= 999999999)" : (
            $body->floorTo*1 > 0 ? " AND (e.floor >= 1 AND e.floor <= ".($body->floorTo*1).")" : ""
            )
            )) .
            ($body->floorCountFrom*1 > 0 && $body->floorCountTo*1 > 0 ? " AND (e.floor_count >= ".($body->floorCountFrom*1)." AND e.floor_count <= ".($body->floorCountTo*1).")" : (
            $body->floorCountFrom*1 > 0 ? " AND (e.floor_count >= ".($body->floorCountFrom*1)." AND e.floor_count <= 999999999)" : (
            $body->floorCountTo*1 > 0 ? " AND (e.floor_count >= 1 AND e.floor_count <= ".($body->floorCountTo*1).")" : ""
            )
            )) .
            ($body->roomCountFrom*1 > 0 && $body->roomCountTo*1 > 0 ? " AND (e.room_count >= ".($body->roomCountFrom*1)." AND e.room_count <= ".($body->roomCountTo*1).")" : (
            $body->roomCountFrom*1 > 0 ? " AND (e.room_count >= ".($body->roomCountFrom*1)." AND e.room_count <= 999999999)" : (
            $body->roomCountTo*1 > 0 ? " AND (e.room_count >= 1 AND e.room_count <= ".($body->roomCountTo*1).")" : ""
            )
            )) .
            ($body->objectTimeType ? " AND e.object_time_type = '".$body->objectTimeType."'" : "") .
            ($body->objectCondition ? " AND e.object_condition = '".$body->objectCondition."'" : "") .
            ($body->preference->buy == true ? " AND e.buy = 1" : "") .
            ($body->preference->renter_long == true || $body->preference->renter_short == true ? " AND (e.renter_long = 1 OR e.renter_short = 1)" : "") .
            ($body->preference->barter == true ? " AND e.barter = 1" : "") .
            ($body->goal > 0 ? " AND e.goal = '".($body->goal - 1)."'" : "") .
            ($body->show == true ? " AND e.show = 1" : " AND e.show = 0") .
            ($body->keyword ? " AND (e.address LIKE '%".$body->keyword."%' OR e.comment LIKE '%".$body->keyword."%' OR e.id LIKE '%".$body->keyword."%' OR c.phone LIKE '%".$body->keyword."%' OR c.phone1 LIKE '%".$body->keyword."%' OR c.phone2 LIKE '%".$body->keyword."%' OR c.fullname LIKE '%".$body->keyword."%')" : "") .
            $filterQuarter .
            $filterDistrict .
            $filterCommerce .
            $filterStatus .
            $filterPrice;

        $countRes = $db->query(
            "SELECT
                    COUNT(e.id) AS count
                 FROM estates e
                  JOIN clients c ON c.id = e.owner
                  WHERE 1=1".
            $filter,
            $parameters
        );

        $count = $countRes->fetch();

        $res = $db->query(
            "SELECT
                    e.*
                 FROM estates e
                  JOIN clients c ON c.id = e.owner
                  WHERE 1=1" .
            $filter .
            $sort .
            $pagination,
            $parameters
        );

        if ($res) {
            $estates = [];
            while ($estate = $res->fetch()) {
                $preference = [
                    "buy" => $estate['buy'] ? true : false,
                    "buy_price" => $estate['buy_price'],
                    "buy_type" => $estate['buy_type'],
                    "renter_long" => $estate['renter_long'] ? true : false,
                    "renter_long_price" => $estate['renter_long_price'],
                    "renter_long_type" => $estate['renter_long_type'],
                    "renter_short" => $estate['renter_short'] ? true : false,
                    "renter_short_price" => $estate['renter_short_price'],
                    "renter_short_type" => $estate['renter_short_type'],
                    "barter" => $estate['barter'] ? true : false
                ];
                $advenced = [
                    "adven1" => $estate['adven1'] ? true : false,
                    "adven2" => $estate['adven2'] ? true : false,
                    "adven3" => $estate['adven3'] ? true : false,
                    "adven4" => $estate['adven4'] ? true : false,
                    "adven5" => $estate['adven5'] ? true : false,
                    "adven6" => $estate['adven6'] ? true : false,
                    "adven7" => $estate['adven7'] ? true : false,
                    "adven8" => $estate['adven8'] ? true : false,
                    "adven9" => $estate['adven9'] ? true : false,
                    "adven10" => $estate['adven10'] ? true : false,
                    "adven11" => $estate['adven11'] ? true : false,
                    "adven12" => $estate['adven12'] ? true : false,
                    "adven13" => $estate['adven13'] ? true : false,
                    "adven14" => $estate['adven14'] ? true : false,
                    "adven15" => $estate['adven15'] ? true : false,
                    "adven16" => $estate['adven16'] ? true : false,
                    "adven17" => $estate['adven17'] ? true : false,
                    "adven18" => $estate['adven18'] ? true : false,
                    "adven19" => $estate['adven19'] ? true : false,
                    "adven20" => $estate['adven20'] ? true : false,
                    "adven21" => $estate['adven21'] ? true : false,
                    "adven22" => $estate['adven22'] ? true : false,
                    "adven23" => $estate['adven23'] ? true : false
                ];
                $images = [
                    "image" => $estate['image'] && $estate['image'] != "deleted" ? $estate['image'] : null,
                    "image1" => $estate['image1'] && $estate['image1'] != "deleted" ? $estate['image1'] : null,
                    "image2" => $estate['image2'] && $estate['image2'] != "deleted" ? $estate['image2'] : null,
                    "image3" => $estate['image3'] && $estate['image3'] != "deleted" ? $estate['image3'] : null,
                    "image4" => $estate['image4'] && $estate['image4'] != "deleted" ? $estate['image4'] : null,
                    "image5" => $estate['image5'] && $estate['image5'] != "deleted" ? $estate['image5'] : null,
                    "image6" => $estate['image6'] && $estate['image6'] != "deleted" ? $estate['image6'] : null,
                    "image7" => $estate['image7'] && $estate['image7'] != "deleted" ? $estate['image7'] : null,
                    "image8" => $estate['image8'] && $estate['image8'] != "deleted" ? $estate['image8'] : null,
                    "image9" => $estate['image9'] && $estate['image9'] != "deleted" ? $estate['image9'] : null,
                    "image10" => $estate['image10'] && $estate['image10'] != "deleted" ? $estate['image10'] : null,
                    "image11" => $estate['image11'] && $estate['image11'] != "deleted" ? $estate['image11'] : null
                ];
                $estate['favorite'] = $estate['favorite'] ? true : false;
                $estate['object_from_owner'] = $estate['object_from_owner'] ? true : false;
                $estate['commerceBase'] = $estate['commerce_base'] ? true : false;
                $estate['commerceBuilding'] = $estate['commerce_building'] ? true : false;
                $estate['commerceButik'] = $estate['commerce_butik'] ? true : false;
                $estate['commerceMake'] = $estate['commerce_make'] ? true : false;
                $estate['commerceOffice'] = $estate['commerce_office'] ? true : false;
                $estate['commerceOther'] = $estate['commerce_other'] ? true : false;
                $estate['commercePublicKitchen'] = $estate['commerce_public_kitchen'] ? true : false;
                $estate['commerceSalon'] = $estate['commerce_salon'] ? true : false;
                $estate['commerceTemporaryBuilding'] = $estate['commerce_temporary_building'] ? true : false;
                $estate['regionId'] = $estate['region_id'];
                $estate['districtId'] = $estate['district_id'];
                $estate['quarterId'] = $estate['quarter_id'];
                $estate['metroId'] = $estate['underground_id'];
                $estate['roomCount'] = $estate['room_count'];
                $estate['objectTimeType'] = $estate['object_time_type'];
                $estate['objectMaterialType'] = $estate['object_material_type'];
                $estate['objectCondition'] = $estate['object_condition'];
                $estate['accessType'] = $estate['access_type'];
                $estate['areaHome'] = $estate['area_home'];
                $estate['buildMaterial'] = $estate['build_material'];
                $estate['buildPlan'] = $estate['build_plan'];
                $estate['buildYear'] = $estate['build_year'];
                $estate['currencyId'] = $estate['currency_id'];
                $estate['doneArea'] = $estate['done_area'];
                $estate['areaTotal'] = $estate['area_total'];
                $estate['floorCount'] = $estate['floor_count'];
                $estate['landArea'] = $estate['land_area'];
                $estate['maxPage'] = $estate['max_page'];
                $estate['realtor'] = CLIENT::getUserName($estate['owner']);
                $estate['client'] = CLIENT::getName($estate['owner']);
                $estate['clientObject'] = CLIENT::getById($estate['owner']);
                $estate['realtorObject'] = USER::getById($estate['clientObject']['user_id']*1);
                $estate['realtorObject']['token'] = null;
                $estate['region'] = REGION::getName($estate['region_id']);
                $estate['district'] = DISTRICT::getName($estate['district_id']);
                $estate['quarter'] = QUARTER::getName($estate['quarter_id']);
                $estate['underground'] = UNDERGROUND::getName($estate['underground_id']);
                $estate['advenced'] = $advenced;
                $estate['preference'] = $preference;
                $estate['images'] = $images;
                // $estate['interesting_clients'] = explode(',', $estate['interesting_clients']);
                array_push($estates, $estate);
            }
            $result['count'] = $count['count'];
            $result['items'] = $estates;
            return $result;
        } else return false;
    }

    static public function getOthers($userId) {
        global $db, $req, $fun, $body, $file;
        $parameters = array();
        $pagination = '';
        $filterAccessType = '';
        $filterPrice = '';
        $filterDistrict = '';
        $filterQuarter = '';
        $filterCommerce = '';
        $whereIds = '';
        $friendIds = [];


        if(count($body->quarterId) > 0){
            $filterQuarter = " AND (";
            foreach($body->quarterId as $key => $quarterId){
                $filterQuarter .= " e.quarter_id=".$quarterId;
                if(count($body->quarterId) != $key + 1){
                    $filterQuarter .= " OR";
                }
            }
            $filterQuarter .= ")";
        }
        if(count($body->districtId) > 0 && $filterQuarter == ''){
            $filterDistrict = " AND (";
            foreach($body->districtId as $key => $districtId){
                $filterDistrict .= " e.district_id=".$districtId;
                if(count($body->districtId) != $key + 1){
                    $filterDistrict .= " OR";
                }
            }
            $filterDistrict .= ")";
        }

        if($body->accessType == 'group'){
            $rs = $db->query("SELECT owner, friend FROM friends WHERE (owner=? OR friend=?) AND `type`=? AND `status`='confirm'",
                [$userId, $userId, $body->accessType]);
            while($friend = $rs->fetch()){
                $id = $friend['owner'] == $userId ? $friend['friend'] : $friend['owner'];
                array_push($friendIds, $id);
            }
            $ids = implode(', ', $friendIds);
            $whereIds = " WHERE c.user_id IN (".($ids ? $ids : '0').")";
            $filterAccessType = " AND (e.access_type = 'public' OR e.access_type = 'friend' OR e.access_type = 'group')";
        } else if($body->accessType == 'friend') {
            $rs = $db->query("SELECT owner, friend FROM friends WHERE (owner=? OR friend=?) AND `type`=? AND `status`='confirm'",
                [$userId, $userId, $body->accessType]);
            while($friend = $rs->fetch()){
                $id = $friend['owner'] == $userId ? $friend['friend'] : $friend['owner'];
                array_push($friendIds, $id);
            }
            $ids = implode(', ', $friendIds);
            $whereIds = " WHERE c.user_id IN (".($ids ? $ids : '0').")";
            $filterAccessType = " AND (e.access_type = 'public' OR e.access_type = 'friend')";
        } else if($body->accessType == 'public') {
            $rs = $db->query("SELECT owner, friend FROM friends WHERE (owner=? OR friend=?) AND `status`='confirm'",
                [$userId, $userId]);
            while($friend = $rs->fetch()){
                $id = $friend['owner'] == $userId ? $friend['friend'] : $friend['owner'];
                array_push($friendIds, $id);
            }
            $ids = implode(', ', $friendIds);
            $whereIds = " WHERE c.user_id NOT IN (".($ids ? $ids : '0').")";
            $filterAccessType = " AND (e.access_type = 'public')";
        } else {
            $rs = $db->query("SELECT owner, friend FROM friends WHERE (owner=? OR friend=?) AND `status`='confirm'",
                [$userId, $userId]);
            while($friend = $rs->fetch()){
                $id = $friend['owner'] == $userId ? $friend['friend'] : $friend['owner'];
                array_push($friendIds, $id);
            }
            $ids = implode(', ', $friendIds);
            $whereIds = " WHERE (c.user_id IN (".($ids ? $ids : '0').") OR e.access_type='public') AND NOT e.access_type='private'";
            $filterAccessType = " AND (e.access_type = 'public' OR e.access_type = 'friend' OR e.access_type = 'group')";
        }

        if($body->sort && $body->direction) {
            $sort = " ORDER BY ".$body->sort." ".$body->direction;
            if($body->sort == 'price') {
                if ($body->preference->renter_long || $body->preference->renter_short) {
                    $sort = " ORDER BY e.renter_long_price ".$body->direction;
                } else {
                    $sort = " ORDER BY e.buy_price ".$body->direction;
                }
            };
        } else {
            $sort = " ORDER BY date_update DESC";
        }
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }

        if($body->priceFrom || $body->priceTo){
            $currencyLocaleId = $body->currencyId;
            $currencyId = $currencyLocaleId == 1 ? 2 : 1;
            $ratio = CURRENCY::getRatio(2);
            $body->priceFrom = $body->priceFrom ? $body->priceFrom : 0;
            $priceLocaleFrom = $body->priceFrom;
            $body->priceTo = $body->priceTo ? $body->priceTo : 99999999999;
            $priceLocaleTo = $body->priceTo;
            if($currencyLocaleId == 2) {
                $priceFrom = ceil($body->priceFrom / $ratio);
                $priceTo = ceil($body->priceTo / $ratio);
            } else {
                $priceFrom = ceil($body->priceFrom * $ratio);
                $priceTo = ceil($body->priceTo * $ratio);
            }
            $pf = $priceFrom ? $priceFrom * 1 > 0 : false;
            $pt = $priceTo ? $priceTo * 1 > 0 : false;
            $plf = $priceLocaleFrom ? $priceLocaleFrom * 1 > 0 : false;
            $plt = $priceLocaleTo ? $priceLocaleTo * 1 > 0 : false;
            $filterPrice = " AND ((".
                ($plf && $plt ? "e.buy_price >= ".$priceLocaleFrom." AND e.buy_price <= ".$priceLocaleTo : (
                $plf ? "e.buy_price >= ".$priceLocaleFrom." AND e.buy_price <= 99999999999" : (
                $plt ? "e.buy_price <= ".$priceLocaleTo." AND e.buy_price >= 1" : ""
                )
                )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                ($pf && $pt ? "e.buy_price >= ".$priceFrom." AND e.buy_price <= ".$priceTo : (
                $pf ? "e.buy_price >= ".$priceFrom." AND e.buy_price <= 99999999999" : (
                $pt ? "e.buy_price <= ".$priceTo." AND e.buy_price >= 1" : ""
                )
                )) ." AND e.currency_id = ".$currencyId.") OR (".
                ($plf && $plt ? "e.renter_long_price >= ".$priceLocaleFrom." AND e.renter_long_price <= ".$priceLocaleTo : (
                $plf ? "e.renter_long_price >= ".$priceLocaleFrom." AND e.renter_long_price <= 99999999999" : (
                $plt ? "e.renter_long_price <= ".$priceLocaleTo." AND e.renter_long_price >= 1" : ""
                )
                )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                ($pf && $pt ? "e.renter_long_price >= ".$priceFrom." AND e.renter_long_price <= ".$priceTo : (
                $pf ? "e.renter_long_price >= ".$priceFrom." AND e.renter_long_price <= 99999999999" : (
                $pt ? "e.renter_long_price <= ".$priceTo." AND e.renter_long_price >= 1" : ""
                )
                )) ." AND e.currency_id = ".$currencyId.") OR (".
                ($plf && $plt ? "e.renter_short_price >= ".$priceLocaleFrom." AND e.renter_short_price <= ".$priceLocaleTo : (
                $plf ? "e.renter_short_price >= ".$priceLocaleFrom." AND e.renter_short_price <= 99999999999" : (
                $plt ? "e.renter_short_price <= ".$priceLocaleTo." AND e.renter_short_price >= 1" : ""
                )
                )) ." AND e.currency_id = ".$currencyLocaleId.") OR (".
                ($pf && $pt ? "e.renter_short_price >= ".$priceFrom." AND e.renter_short_price <= ".$priceTo : (
                $pf ? "e.renter_short_price >= ".$priceFrom." AND e.renter_short_price <= 99999999999" : (
                $pt ? "e.renter_short_price <= ".$priceTo." AND e.renter_short_price >= 1" : ""
                )
                )) ." AND e.currency_id = ".$currencyId."))";
        }

        if($body->commerceBase || $body->commerceBuilding || $body->commerceButik || $body->commerceMake || $body->commerceOffice || $body->commerceOther || $body->commercePublicKitchen || $body->commerceSalon || $body->commerceTemporaryBuilding){
            $filterCommerce = " AND (false" .
                ($body->commerceBase ? " OR e.commerce_base = 1" : "") .
                ($body->commerceBuilding ? " OR e.commerce_building = 1" : "") .
                ($body->commerceButik ? " OR e.commerce_butik = 1" : "") .
                ($body->commerceMake ? " OR e.commerce_make = 1" : "") .
                ($body->commerceOffice ? " OR e.commerce_office = 1" : "") .
                ($body->commerceOther ? " OR e.commerce_other = 1" : "") .
                ($body->commercePublicKitchen ? " OR e.commerce_public_kitchen = 1" : "") .
                ($body->commerceSalon ? " OR e.commerce_salon = 1" : "") .
                ($body->commerceTemporaryBuilding ? " OR e.commerce_temporary_building = 1" : "") .
                ")";
        }

        $filter = ($body->favorite ? " AND e.favorite = 1" : "") .
            ($body->objectId ? " AND e.id = ".$body->objectId : "") .
            ($body->type ? " AND e.type = '".$body->type."'" : "") .
            ($body->regionId ? " AND e.region_id = '".$body->regionId."'" : "") .
            ($body->metroId ? " AND e.underground_id = '".$body->metroId."'" : "") .
            ($body->owner ? " AND e.owner = '".$body->owner."'" : "") .
            ($body->landAreaFrom*1 > 0 && $body->landAreaTo*1 > 0 ? " AND (e.land_area >= ".($body->landAreaFrom*1)." AND e.land_area <= ".($body->landAreaTo*1).")" : (
            $body->landAreaFrom*1 > 0 ? " AND (e.land_area >= ".($body->landAreaFrom*1)." AND e.land_area <= 999999999)" : (
            $body->landAreaTo*1 > 0 ? " AND (e.land_area >= 1 AND e.land_area <= ".($body->landAreaTo*1).")" : ""
            )
            )) .
            ($body->totalAreaFrom*1 > 0 && $body->totalAreaTo*1 > 0 ? " AND (e.area_total >= ".($body->totalAreaFrom*1)." AND e.area_total <= ".($body->totalAreaTo*1).")" : (
            $body->totalAreaFrom*1 > 0 ? " AND (e.area_total >= ".($body->totalAreaFrom*1)." AND e.area_total <= 999999999)" : (
            $body->totalAreaTo*1 > 0 ? " AND (e.area_total >= 1 AND e.area_total <= ".($body->totalAreaTo*1).")" : ""
            )
            )) .
            ($body->floorFrom*1 > 0 && $body->floorTo*1 > 0 ? " AND (e.floor >= ".($body->floorFrom*1)." AND e.floor <= ".($body->floorTo*1).")" : (
            $body->floorFrom*1 > 0 ? " AND (e.floor >= ".($body->floorFrom*1)." AND e.floor <= 999999999)" : (
            $body->floorTo*1 > 0 ? " AND (e.floor >= 1 AND e.floor <= ".($body->floorTo*1).")" : ""
            )
            )) .
            ($body->floorCountFrom*1 > 0 && $body->floorCountTo*1 > 0 ? " AND (e.floor_count >= ".($body->floorCountFrom*1)." AND e.floor_count <= ".($body->floorCountTo*1).")" : (
            $body->floorCountFrom*1 > 0 ? " AND (e.floor_count >= ".($body->floorCountFrom*1)." AND e.floor_count <= 999999999)" : (
            $body->floorCountTo*1 > 0 ? " AND (e.floor_count >= 1 AND e.floor_count <= ".($body->floorCountTo*1).")" : ""
            )
            )) .
            ($body->roomCountFrom*1 > 0 && $body->roomCountTo*1 > 0 ? " AND (e.room_count >= ".($body->roomCountFrom*1)." AND e.room_count <= ".($body->roomCountTo*1).")" : (
            $body->roomCountFrom*1 > 0 ? " AND (e.room_count >= ".($body->roomCountFrom*1)." AND e.room_count <= 999999999)" : (
            $body->roomCountTo*1 > 0 ? " AND (e.room_count >= 1 AND e.room_count <= ".($body->roomCountTo*1).")" : ""
            )
            )) .
            ($body->objectTimeType ? " AND e.object_time_type = '".$body->objectTimeType."'" : "") .
            ($body->objectCondition ? " AND e.object_condition = '".$body->objectCondition."'" : "") .
            ($body->preference->buy ? " AND e.buy = 1" : "") .
            ($body->preference->renter_long == true || $body->preference->renter_short == true ? " AND (e.renter_long = 1 OR e.renter_short = 1)" : "") .
            ($body->preference->barter ? " AND e.barter = 1" : "") .
            ($body->object_from_owner ? " AND e.object_from_owner = 1" : " AND e.object_from_owner = 0") .
            ($body->goal ? " AND e.goal = '".$body->goal."'" : "") .
            ($body->userId ? " AND u.id = ".$body->userId."" : "") .
            ($body->objectId ? " AND e.id = ".$body->objectId."" : "") .
            ($body->userPhone ? " AND u.phone LIKE '%".$body->userPhone."%'" : "") .
            (" AND e.status = 'free'") .
            (" AND e.show = 1") .
            ($body->onlyWithoutImage ? " AND (e.image2 IS NOT NULL OR e.image3 IS NOT NULL OR e.image4 IS NOT NULL OR e.image5 IS NOT NULL OR e.image6 IS NOT NULL OR e.image7 IS NOT NULL OR e.image8 IS NOT NULL OR e.image9 IS NOT NULL OR e.image10 IS NOT NULL OR e.image11 IS NOT NULL) " : "") .
            $filterAccessType .
            $filterDistrict .
            $filterQuarter .
            $filterCommerce .
            ($body->keyword ? " AND (e.comment LIKE '%".$body->keyword."%' OR e.id LIKE '%".$body->keyword."%' OR u.phone LIKE '%".$body->keyword."%' OR u.fullname LIKE '%".$body->keyword."%' OR re.name LIKE '%".$body->keyword."%' OR di.name LIKE '%".$body->keyword."%' OR qu.name LIKE '%".$body->keyword."%')" : "").
            $filterPrice;
        
        if($body->object_from_owner) {
            $filter .= " AND e.object_from_owner = 1";
        } else {
            $filter .= " AND u.id <> ".$userId;
        }

        $countRes = $db->query(
            "SELECT
                    COUNT(e.id) AS count
                 FROM estates e
                  JOIN clients c ON c.id = e.owner
                  JOIN users u ON u.id = c.user_id
                  JOIN regions re ON re.id = e.region_id
                  JOIN districts di ON di.id = e.district_id
                  JOIN quarters qu ON qu.id = e.quarter_id".
            $whereIds.
            $filter,
            $parameters
        );

        $count = $countRes->fetch();

        $res = $db->query(
            "SELECT
                    e.*
                 FROM estates e
                  JOIN clients c ON c.id = e.owner
                  JOIN users u ON u.id = c.user_id
                  JOIN regions re ON re.id = e.region_id
                  JOIN districts di ON di.id = e.district_id
                  JOIN quarters qu ON qu.id = e.quarter_id".
            $whereIds.
            $filter .
            $sort .
            $pagination,
            $parameters
        );

        if ($res) {
            $estates = [];
            while ($estate = $res->fetch()) {
                $preference = [
                    "buy" => $estate['buy'] ? true : false,
                    "buy_price" => $estate['buy_price'],
                    "buy_type" => $estate['buy_type'],
                    "renter_long" => $estate['renter_long'] ? true : false,
                    "renter_long_price" => $estate['renter_long_price'],
                    "renter_long_type" => $estate['renter_long_type'],
                    "renter_short" => $estate['renter_short'] ? true : false,
                    "renter_short_price" => $estate['renter_short_price'],
                    "renter_short_type" => $estate['renter_short_type'],
                    "barter" => $estate['barter'] ? true : false
                ];
                $advenced = [
                    "adven1" => $estate['adven1'] ? true : false,
                    "adven2" => $estate['adven2'] ? true : false,
                    "adven3" => $estate['adven3'] ? true : false,
                    "adven4" => $estate['adven4'] ? true : false,
                    "adven5" => $estate['adven5'] ? true : false,
                    "adven6" => $estate['adven6'] ? true : false,
                    "adven7" => $estate['adven7'] ? true : false,
                    "adven8" => $estate['adven8'] ? true : false,
                    "adven9" => $estate['adven9'] ? true : false,
                    "adven10" => $estate['adven10'] ? true : false,
                    "adven11" => $estate['adven11'] ? true : false,
                    "adven12" => $estate['adven12'] ? true : false,
                    "adven13" => $estate['adven13'] ? true : false,
                    "adven14" => $estate['adven14'] ? true : false,
                    "adven15" => $estate['adven15'] ? true : false,
                    "adven16" => $estate['adven16'] ? true : false,
                    "adven17" => $estate['adven17'] ? true : false,
                    "adven18" => $estate['adven18'] ? true : false,
                    "adven19" => $estate['adven19'] ? true : false,
                    "adven20" => $estate['adven20'] ? true : false,
                    "adven21" => $estate['adven21'] ? true : false,
                    "adven22" => $estate['adven22'] ? true : false,
                    "adven23" => $estate['adven23'] ? true : false
                ];
                $images = [
                    "image" => null,
                    "image1" => null,
                    "image2" => $estate['image2'] && $estate['image2'] != "deleted" ? $estate['image2'] : null,
                    "image3" => $estate['image3'] && $estate['image3'] != "deleted" ? $estate['image3'] : null,
                    "image4" => $estate['image4'] && $estate['image4'] != "deleted" ? $estate['image4'] : null,
                    "image5" => $estate['image5'] && $estate['image5'] != "deleted" ? $estate['image5'] : null,
                    "image6" => $estate['image6'] && $estate['image6'] != "deleted" ? $estate['image6'] : null,
                    "image7" => $estate['image7'] && $estate['image7'] != "deleted" ? $estate['image7'] : null,
                    "image8" => $estate['image8'] && $estate['image8'] != "deleted" ? $estate['image8'] : null,
                    "image9" => $estate['image9'] && $estate['image9'] != "deleted" ? $estate['image9'] : null,
                    "image10" => $estate['image10'] && $estate['image10'] != "deleted" ? $estate['image10'] : null,
                    "image11" => $estate['image11'] && $estate['image11'] != "deleted" ? $estate['image11'] : null
                ];
                if($estate['object_from_owner']) {
                    $estate['clientName'] = CLIENT::getName($estate['owner']);
                    $estate['clientPhone'] = CLIENT::getPhone($estate['owner']);
                }
                $estate['image'] = null;
                $estate['image1'] = null;
                $estate['address'] = null;
                $estate['owner'] = null;
                $estate['favorite'] = $estate['favorite'] ? true : false;
                $estate['object_from_owner'] = $estate['object_from_owner'] ? true : false;
                $estate['commerceBase'] = $estate['commerce_base'] ? true : false;
                $estate['commerceBuilding'] = $estate['commerce_building'] ? true : false;
                $estate['commerceButik'] = $estate['commerce_butik'] ? true : false;
                $estate['commerceMake'] = $estate['commerce_make'] ? true : false;
                $estate['commerceOffice'] = $estate['commerce_office'] ? true : false;
                $estate['commerceOther'] = $estate['commerce_other'] ? true : false;
                $estate['commercePublicKitchen'] = $estate['commerce_public_kitchen'] ? true : false;
                $estate['commerceSalon'] = $estate['commerce_salon'] ? true : false;
                $estate['commerceTemporaryBuilding'] = $estate['commerce_temporary_building'] ? true : false;
                $estate['regionId'] = $estate['region_id'];
                $estate['districtId'] = $estate['district_id'];
                $estate['quarterId'] = $estate['quarter_id'];
                $estate['metroId'] = $estate['underground_id'];
                $estate['roomCount'] = $estate['room_count'];
                $estate['objectTimeType'] = $estate['object_time_type'];
                $estate['objectMaterialType'] = $estate['object_material_type'];
                $estate['objectCondition'] = $estate['object_condition'];
                $estate['accessType'] = $estate['access_type'];
                $estate['areaHome'] = $estate['area_home'];
                $estate['buildMaterial'] = $estate['build_material'];
                $estate['buildPlan'] = $estate['build_plan'];
                $estate['buildYear'] = $estate['build_year'];
                $estate['currencyId'] = $estate['currency_id'];
                $estate['doneArea'] = $estate['done_area'];
                $estate['areaTotal'] = $estate['area_total'];
                $estate['floorCount'] = $estate['floor_count'];
                $estate['landArea'] = $estate['land_area'];
                $estate['maxPage'] = $estate['max_page'];
                $estate['region'] = REGION::getName($estate['region_id']);
                $estate['district'] = DISTRICT::getName($estate['district_id']);
                $estate['quarter'] = QUARTER::getName($estate['quarter_id']);
                $estate['underground'] = UNDERGROUND::getName($estate['underground_id']);
                $estate['advenced'] = $advenced;
                $estate['preference'] = $preference;
                $estate['images'] = $images;
                // $estate['interesting_clients'] = explode(',', $estate['interesting_clients']);
                array_push($estates, $estate);
            }
            $result['count'] = $count['count'];
            $result['items'] = $estates;
            return $result;
        } else return false;
    }

    static public function create($userId) {
        global $db, $body, $files, $const, $fun;
        $time = $fun->time();
        $true = true;
        $parameters = array($body->owner, $body->address, $body->regionId, $body->districtId, $body->quarterId, $body->type, $time, $time);
        if ($body->accessType) array_push($parameters, $body->accessType);
        if ($body->objectTimeType) array_push($parameters, $body->objectTimeType);
        if ($body->metroId) array_push($parameters, $body->metroId);
        if ($body->roomCount) array_push($parameters, $body->roomCount);
        if ($body->floor) array_push($parameters, $body->floor);
        if ($body->floorCount) array_push($parameters, $body->floorCount);
        if ($body->maxPage) array_push($parameters, $body->maxPage);
        if ($body->areaHome) array_push($parameters, $body->areaHome);
        if ($body->areaTotal) array_push($parameters, $body->areaTotal);
        if ($body->comment) array_push($parameters, $body->comment);
        if ($body->buildYear) array_push($parameters, $body->buildYear);
        if ($body->favorite) array_push($parameters, $body->favorite);
        if ($body->landArea) array_push($parameters, $body->landArea);
        if ($body->currencyId) array_push($parameters, $body->currencyId);
        if ($body->price) array_push($parameters, $body->price);
        if ($body->preference->buy) array_push($parameters, $body->preference->buy);
        if ($body->preference->buy_price) array_push($parameters, $body->preference->buy_price);
        if ($body->preference->buy_type) array_push($parameters, $body->preference->buy_type);
        if ($body->preference->renter_long) array_push($parameters, $body->preference->renter_long);
        if ($body->preference->renter_long_price) array_push($parameters, $body->preference->renter_long_price);
        if ($body->preference->renter_long_type) array_push($parameters, $body->preference->renter_long_type);
        if ($body->preference->renter_short) array_push($parameters, $body->preference->renter_short);
        if ($body->preference->renter_short_price) array_push($parameters, $body->preference->renter_short_price);
        if ($body->preference->renter_short_type) array_push($parameters, $body->preference->renter_short_type);
        if ($body->preference->barter) array_push($parameters, $body->preference->barter);
        if ($body->buildMaterial) array_push($parameters, $body->buildMaterial);
        if ($body->buildPlan) array_push($parameters, $body->buildPlan);
        if ($body->doneArea) array_push($parameters, $body->doneArea);
        if ($body->goal) array_push($parameters, $body->goal);
        if ($body->objectCondition) array_push($parameters, $body->objectCondition);
        if ($body->objectMaterialType) array_push($parameters, $body->objectMaterialType);
        if ($body->commerceBase) array_push($parameters, $body->commerceBase);
        if ($body->commerceBuilding) array_push($parameters, $body->commerceBuilding);
        if ($body->commerceButik) array_push($parameters, $body->commerceButik);
        if ($body->commerceMake) array_push($parameters, $body->commerceMake);
        if ($body->commerceOffice) array_push($parameters, $body->commerceOffice);
        if ($body->commerceOther) array_push($parameters, $body->commerceOther);
        if ($body->commercePublicKitchen) array_push($parameters, $body->commercePublicKitchen);
        if ($body->commerceSalon) array_push($parameters, $body->commerceSalon);
        if ($body->commerceTemporaryBuilding) array_push($parameters, $body->commerceTemporaryBuilding);
        if ($body->advenced->adven1) array_push($parameters, $body->advenced->adven1);
        if ($body->advenced->adven2) array_push($parameters, $body->advenced->adven2);
        if ($body->advenced->adven3) array_push($parameters, $body->advenced->adven3);
        if ($body->advenced->adven4) array_push($parameters, $body->advenced->adven4);
        if ($body->advenced->adven5) array_push($parameters, $body->advenced->adven5);
        if ($body->advenced->adven6) array_push($parameters, $body->advenced->adven6);
        if ($body->advenced->adven7) array_push($parameters, $body->advenced->adven7);
        if ($body->advenced->adven8) array_push($parameters, $body->advenced->adven8);
        if ($body->advenced->adven9) array_push($parameters, $body->advenced->adven9);
        if ($body->advenced->adven10) array_push($parameters, $body->advenced->adven10);
        if ($body->advenced->adven11) array_push($parameters, $body->advenced->adven11);
        if ($body->advenced->adven12) array_push($parameters, $body->advenced->adven12);
        if ($body->advenced->adven13) array_push($parameters, $body->advenced->adven13);
        if ($body->advenced->adven14) array_push($parameters, $body->advenced->adven14);
        if ($body->advenced->adven15) array_push($parameters, $body->advenced->adven15);
        if ($body->advenced->adven16) array_push($parameters, $body->advenced->adven16);
        if ($body->advenced->adven17) array_push($parameters, $body->advenced->adven17);
        if ($body->advenced->adven18) array_push($parameters, $body->advenced->adven18);
        if ($body->advenced->adven19) array_push($parameters, $body->advenced->adven19);
        if ($body->advenced->adven20) array_push($parameters, $body->advenced->adven20);
        if ($body->advenced->adven21) array_push($parameters, $body->advenced->adven21);
        if ($body->advenced->adven22) array_push($parameters, $body->advenced->adven22);
        if ($body->advenced->adven23) array_push($parameters, $body->advenced->adven23);
        if ($body->images->image) array_push($parameters, $body->images->image);
        if ($body->images->image1) array_push($parameters, $body->images->image1);
        if ($body->images->image2) array_push($parameters, $body->images->image2);
        if ($body->images->image3) array_push($parameters, $body->images->image3);
        if ($body->images->image4) array_push($parameters, $body->images->image4);
        if ($body->images->image5) array_push($parameters, $body->images->image5);
        if ($body->images->image6) array_push($parameters, $body->images->image6);
        if ($body->images->image7) array_push($parameters, $body->images->image7);
        if ($body->images->image8) array_push($parameters, $body->images->image8);
        if ($body->images->image9) array_push($parameters, $body->images->image9);
        if ($body->images->image10) array_push($parameters, $body->images->image10);
        if ($body->images->image11) array_push($parameters, $body->images->image11);
        if ($body->object_from_owner) array_push($parameters, $body->object_from_owner);
        if ($body->accessType != 'public' || $body->object_from_owner) array_push($parameters, $true);

        $res = $db->query(
            "INSERT INTO estates SET
                        owner=?,
                        address=?,
                        region_id=?,
                        district_id=?,
                        quarter_id=?,
                        `type`=?,
                        date_update=?,
                        date_create=?" .
            ($body->accessType ? ", access_type=?" : "") .
            ($body->objectTimeType ? ", object_time_type=?" : "") .
            ($body->metroId ? ", underground_id=?" : "") .
            ($body->roomCount ? ", room_count=?" : "") .
            ($body->floor ? ", floor=?" : "") .
            ($body->floorCount ? ", floor_count=?" : "") .
            ($body->maxPage ? ", max_page=?" : "") .
            ($body->areaHome ? ", area_home=?" : "") .
            ($body->areaTotal ? ", area_total=?" : "") .
            ($body->comment ? ", comment=?" : "") .
            ($body->buildYear ? ", build_year=?" : "") .
            ($body->favorite ? ", favorite=?" : "") .
            ($body->landArea ? ", land_area=?" : "") .
            ($body->currencyId ? ", currency_id=?" : "") .
            ($body->price ? ", price=?" : "") .
            ($body->preference->buy ? ", buy=?" : "") .
            ($body->preference->buy_price ? ", buy_price=?" : "") .
            ($body->preference->buy_type ? ", buy_type=?" : "") .
            ($body->preference->renter_long ? ", renter_long=?" : "") .
            ($body->preference->renter_long_price ? ", renter_long_price=?" : "") .
            ($body->preference->renter_long_type ? ", renter_long_type=?" : "") .
            ($body->preference->renter_short ? ", renter_short=?" : "") .
            ($body->preference->renter_short_price ? ", renter_short_price=?" : "") .
            ($body->preference->renter_short_type ? ", renter_short_type=?" : "") .
            ($body->preference->barter ? ", barter=?" : "") .
            ($body->buildMaterial ? ", build_material=?" : "") .
            ($body->buildPlan ? ", build_plan=?" : "") .
            ($body->doneArea ? ", done_area=?" : "") .
            ($body->goal ? ", goal=?" : "") .
            ($body->objectCondition ? ", object_condition=?" : "") .
            ($body->objectMaterialType ? ", object_material_type=?" : "") .
            ($body->commerceBase ? ", commerce_base=?" : "") .
            ($body->commerceBuilding ? ", commerce_building=?" : "") .
            ($body->commerceButik ? ", commerce_butik=?" : "") .
            ($body->commerceMake ? ", commerce_make=?" : "") .
            ($body->commerceOffice ? ", commerce_office=?" : "") .
            ($body->commerceOther ? ", commerce_other=?" : "") .
            ($body->commercePublicKitchen ? ", commerce_public_kitchen=?" : "") .
            ($body->commerceSalon ? ", commerce_salon=?" : "") .
            ($body->commerceTemporaryBuilding ? ", commerce_temporary_building=?" : "") .
            ($body->advenced->adven1 ? ", adven1=?" : "") .
            ($body->advenced->adven2 ? ", adven2=?" : "") .
            ($body->advenced->adven3 ? ", adven3=?" : "") .
            ($body->advenced->adven4 ? ", adven4=?" : "") .
            ($body->advenced->adven5 ? ", adven5=?" : "") .
            ($body->advenced->adven6 ? ", adven6=?" : "") .
            ($body->advenced->adven7 ? ", adven7=?" : "") .
            ($body->advenced->adven8 ? ", adven8=?" : "") .
            ($body->advenced->adven9 ? ", adven9=?" : "") .
            ($body->advenced->adven10 ? ", adven10=?" : "") .
            ($body->advenced->adven11 ? ", adven11=?" : "") .
            ($body->advenced->adven12 ? ", adven12=?" : "") .
            ($body->advenced->adven13 ? ", adven13=?" : "") .
            ($body->advenced->adven14 ? ", adven14=?" : "") .
            ($body->advenced->adven15 ? ", adven15=?" : "") .
            ($body->advenced->adven16 ? ", adven16=?" : "") .
            ($body->advenced->adven17 ? ", adven17=?" : "") .
            ($body->advenced->adven18 ? ", adven18=?" : "") .
            ($body->advenced->adven19 ? ", adven19=?" : "") .
            ($body->advenced->adven20 ? ", adven20=?" : "") .
            ($body->advenced->adven21 ? ", adven21=?" : "") .
            ($body->advenced->adven22 ? ", adven22=?" : "") .
            ($body->advenced->adven23 ? ", adven23=?" : "") .
            ($body->images->image ? ($body->images->image == 'deleted' ? ", image=NULL" : ", image=?") : "") .
            ($body->images->image1 ? ($body->images->image1 == 'deleted' ? ", image1=NULL" : ", image1=?") : "") .
            ($body->images->image2 ? ($body->images->image2 == 'deleted' ? ", image2=NULL" : ", image2=?") : "") .
            ($body->images->image3 ? ($body->images->image3 == 'deleted' ? ", image3=NULL" : ", image3=?") : "") .
            ($body->images->image4 ? ($body->images->image4 == 'deleted' ? ", image4=NULL" : ", image4=?") : "") .
            ($body->images->image5 ? ($body->images->image5 == 'deleted' ? ", image5=NULL" : ", image5=?") : "") .
            ($body->images->image6 ? ($body->images->image6 == 'deleted' ? ", image6=NULL" : ", image6=?") : "") .
            ($body->images->image7 ? ($body->images->image7 == 'deleted' ? ", image7=NULL" : ", image7=?") : "") .
            ($body->images->image8 ? ($body->images->image8 == 'deleted' ? ", image8=NULL" : ", image8=?") : "") .
            ($body->images->image9 ? ($body->images->image9 == 'deleted' ? ", image9=NULL" : ", image9=?") : "") .
            ($body->images->image10 ? ($body->images->image10 == 'deleted' ? ", image10=NULL" : ", image10=?") : "") .
            ($body->images->image11 ? ($body->images->image11 == 'deleted' ? ", image11=NULL" : ", image11=?") : "") .
            ($body->object_from_owner ? ", object_from_owner=?" : "") .
            (($body->accessType != 'public' || $body->object_from_owner) ? ", `show`=?" : ""),
            $parameters
        );

        if ($res) $res = $db->query("SELECT id FROM estates ORDER BY id DESC LIMIT 1");
        $estate = $res->fetch();
        if($estate) {
            switch ($body->accessType) {
                case 'public' :
                    $user = USER::getById($userId);
                    $groups = FRIEND::getAll($userId, 'group', 'confirm');
                    foreach($groups as $k => $group){
                        NOTIFICATION::create($group['friend'], 'Новый объект (№'.$estate['id'].')', 'От: '.$user['fullname'], 'other_estate', $estate['id']);
                    }
                    $friends = FRIEND::getAll($userId, 'friend', 'confirm');
                    foreach($friends as $k => $friend){
                        NOTIFICATION::create($friend['friend'], 'Новый объект (№'.$estate['id'].')', 'От: '.$user['fullname'], 'other_estate', $estate['id']);
                    }
                    break;
                case 'group' :
                    $user = USER::getById($userId);
                    $groups = FRIEND::getAll($userId, 'group', 'confirm');
                    foreach($groups as $k => $group){
                        NOTIFICATION::create($group['friend'], 'Новый объект (№'.$estate['id'].')', 'От: '.$user['fullname'], 'other_estate', $estate['id']);
                    }
                    $friends = FRIEND::getAll($userId, 'friend', 'confirm');
                    foreach($friends as $k => $friend){
                        NOTIFICATION::create($friend['friend'], 'Новый объект (№'.$estate['id'].')', 'От: '.$user['fullname'], 'other_estate', $estate['id']);
                    }
                    break;
                case 'friend' :
                    $user = USER::getById($userId);
                    $friends = FRIEND::getAll($userId, 'friend', 'confirm');
                    foreach($friends as $k => $friend){
                        NOTIFICATION::create($friend['friend'], 'Новый объект (№'.$estate['id'].')', 'От: '.$user['fullname'], 'other_estate', $estate['id']);
                    }
                    break;
            }
            
            return $estate['id'];
        }
        else return false;
    }
    static public function update() {
        global $db, $body, $files, $req, $const, $fun;
        $id = $req['estateId'];
        $time = $fun->time();

        $parameters = array($time);

        if ($body->owner) array_push($parameters, $body->owner);
        else array_push($parameters, 0);
        if ($body->address) array_push($parameters, $body->address);
        else array_push($parameters, null);
        if ($body->regionId) array_push($parameters, $body->regionId);
        else array_push($parameters, 0);
        if ($body->districtId) array_push($parameters, $body->districtId);
        else array_push($parameters, 0);
        if ($body->quarterId) array_push($parameters, $body->quarterId);
        else array_push($parameters, 0);
        if ($body->type) array_push($parameters, $body->type);
        else array_push($parameters, null);
        if ($body->accessType) array_push($parameters, $body->accessType);
        else array_push($parameters, null);
        if ($body->objectTimeType) array_push($parameters, $body->objectTimeType);
        else array_push($parameters, null);
        if ($body->metroId) array_push($parameters, $body->metroId);
        else array_push($parameters, 0);
        if ($body->roomCount) array_push($parameters, $body->roomCount);
        else array_push($parameters, 0);
        if ($body->floor) array_push($parameters, $body->floor);
        else array_push($parameters, 0);
        if ($body->floorCount) array_push($parameters, $body->floorCount);
        else array_push($parameters, 0);
        if ($body->maxPage) array_push($parameters, $body->maxPage);
        else array_push($parameters, 0);
        if ($body->areaHome) array_push($parameters, $body->areaHome);
        else array_push($parameters, 0);
        if ($body->areaTotal) array_push($parameters, $body->areaTotal);
        else array_push($parameters, 0);
        if ($body->comment) array_push($parameters, $body->comment);
        else array_push($parameters, null);
        if ($body->buildYear) array_push($parameters, $body->buildYear);
        else array_push($parameters, 0);
        if ($body->favorite) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->landArea) array_push($parameters, $body->landArea);
        else array_push($parameters, 0);
        if ($body->currencyId) array_push($parameters, $body->currencyId);
        else array_push($parameters, 1);
        if ($body->price) array_push($parameters, $body->price);
        else array_push($parameters, 0);
        if ($body->preference->buy) array_push($parameters, $body->preference->buy);
        else array_push($parameters, 0);
        if ($body->preference->buy_price) array_push($parameters, $body->preference->buy_price);
        else array_push($parameters, 0);
        if ($body->preference->buy_type) array_push($parameters, $body->preference->buy_type);
        else array_push($parameters, null);
        if ($body->preference->renter_long) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->preference->renter_long_price) array_push($parameters, $body->preference->renter_long_price);
        else array_push($parameters, 0);
        if ($body->preference->renter_long_type) array_push($parameters, $body->preference->renter_long_type);
        else array_push($parameters, null);
        if ($body->preference->renter_short) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->preference->renter_short_price) array_push($parameters, $body->preference->renter_short_price);
        else array_push($parameters, 0);
        if ($body->preference->renter_short_type) array_push($parameters, $body->preference->renter_short_type);
        else array_push($parameters, null);
        if ($body->preference->barter) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->buildMaterial) array_push($parameters, $body->buildMaterial);
        else array_push($parameters, 0);
        if ($body->buildPlan) array_push($parameters, $body->buildPlan);
        else array_push($parameters, 0);
        if ($body->doneArea) array_push($parameters, $body->doneArea);
        else array_push($parameters, 0);
        if ($body->goal) array_push($parameters, $body->goal);
        else array_push($parameters, 0);
        if ($body->objectCondition) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->objectMaterialType) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->commerceBase) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->commerceBuilding) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->commerceButik) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->commerceMake) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->commerceOffice) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->commerceOther) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->commercePublicKitchen) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->commerceSalon) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->commerceTemporaryBuilding) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven1) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven2) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven3) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven4) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven5) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven6) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven7) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven8) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven9) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven10) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven11) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven12) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven13) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven14) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven15) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven16) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven17) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven18) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven19) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven20) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven21) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven22) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->advenced->adven23) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->images->image) array_push($parameters, $body->images->image);
        else array_push($parameters, null);
        if ($body->images->image1) array_push($parameters, $body->images->image1);
        else array_push($parameters, null);
        if ($body->images->image2) array_push($parameters, $body->images->image2);
        else array_push($parameters, null);
        if ($body->images->image3) array_push($parameters, $body->images->image3);
        else array_push($parameters, null);
        if ($body->images->image4) array_push($parameters, $body->images->image4);
        else array_push($parameters, null);
        if ($body->images->image5) array_push($parameters, $body->images->image5);
        else array_push($parameters, null);
        if ($body->images->image6) array_push($parameters, $body->images->image6);
        else array_push($parameters, null);
        if ($body->images->image7) array_push($parameters, $body->images->image7);
        else array_push($parameters, null);
        if ($body->images->image8) array_push($parameters, $body->images->image8);
        else array_push($parameters, null);
        if ($body->images->image9) array_push($parameters, $body->images->image9);
        else array_push($parameters, null);
        if ($body->images->image10) array_push($parameters, $body->images->image10);
        else array_push($parameters, null);
        if ($body->images->image11) array_push($parameters, $body->images->image11);
        else array_push($parameters, null);
        if ($body->object_from_owner) array_push($parameters, 1);
        else array_push($parameters, 0);
        if ($body->accessType != 'public' || $body->object_from_owner) array_push($parameters, 1);
        else array_push($parameters, 0);

        array_push($parameters, $id);

        $res = $db->query(
            "UPDATE estates SET date_update=?, owner=?, address=?, region_id=?, district_id=?, quarter_id=?,
            `type`=?, access_type=?, object_time_type=?, underground_id=?, room_count=?, floor=?, floor_count=?,
            max_page=?, area_home=?, area_total=?, comment=?, build_year=?, favorite=?, land_area=?, currency_id=?,
            price=?, buy=?, buy_price=?, buy_type=?, renter_long=?, renter_long_price=?, renter_long_type=?,
            renter_short=?, renter_short_price=?, renter_short_type=?, barter=?, build_material=?, build_plan=?,
            done_area=?, goal=?, object_condition=?, object_material_type=?, commerce_base=?, commerce_building=?,
            commerce_butik=?, commerce_make=?, commerce_office=?, commerce_other=?, commerce_public_kitchen=?,
            commerce_salon=?, commerce_temporary_building=?, adven1=?, adven2=?, adven3=?, adven4=?, adven5=?,
            adven6=?, adven7=?, adven8=?, adven9=?, adven10=?, adven11=?, adven12=?, adven13=?, adven14=?,
            adven15=?, adven16=?, adven17=?, adven18=?, adven19=?, adven20=?, adven21=?, adven22=?, adven23=?,
            image=?, image1=?, image2=?, image3=?, image4=?, image5=?, image6=?, image7=?, image8=?, image9=?,
            image10=?, image11=?, `object_from_owner`=?, `show`=?" .
            ($body->is_request == true ? ", is_request=1" : ", is_request=0") .
            " WHERE id=?",
            $parameters
        );
        if($res) return $req['estateId'];
        else return false;
    }

    static public function removeImage($id, $image) {
        global $db;
        $res = $db->query("SELECT * FROM estates WHERE id=?", [$id]);
        $file = $res->fetch();
        unlink('file/'. $file[$image]);
    }

    static public function getRenters() {
        global $db;
        $res = $db->query("SELECT id, status, stopTime, owner FROM estates WHERE status='rent'");
        return $res->fetchAll();
    }

    static public function show($id = 0) {
        global $db;
        $res = $db->query("UPDATE estates SET `show`=? WHERE id=?", [1, $id]);
        return $res ? true : false;
    }

    static public function hide($id = 0) {
        global $db;
        $res = $db->query("UPDATE estates SET `show`=? WHERE id=?", [0, $id]);
        return $res ? true : false;
    }

    static public function share($id) {
        global $db;
        $time = time();
        $res = $db->query("INSERT INTO share SET estateId=?, createdDate=?", [$id, $time]);
        return $res ? true : false;
    }

    static public function checkByUserId($id, $userId) {
        global $db;
        $res = $db->query(
            "SELECT e.id, e.owner
            FROM estates e
            WHERE e.id=?",
            [$id]
        );
        $estate = $res->fetch();
        if($estate && CLIENT::checkByUserId($estate['owner'], $userId)) return true;
        return false;
    }

}
