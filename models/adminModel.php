<?php
class ADMIN {

    static public function getByToken($token) {
        global $db;
        $res = $db->query("SELECT a.id, a.fullname, a.phone, a.token FROM admins a WHERE a.token=?", [$token]);
        $admin = $res->fetch();
        return $admin ? $admin : false;
    }

    static public function getById($id) {
        global $db;
        $res = $db->query("SELECT a.id, a.fullname, a.phone, a.token FROM admins a WHERE a.id=?", [$id]);
        $admin = $res->fetch();
        return $admin ? $admin : false;
    }

    static public function getByPhone($phone) {
        global $db;
        $res = $db->query("SELECT a.id, a.fullname, a.phone, a.token FROM admins a WHERE a.phone=?", [$phone]);
        $admin = $res->fetch();
        return $admin ? $admin : false;
    }

    static public function login($phone, $password) {
        global $db;
        $res = $db->query("SELECT a.id, a.fullname, a.phone, a.token FROM admins a WHERE a.phone=? AND a.password=?", [$phone, $password]);
        $admin = $res->fetch();
        return $admin ? $admin : false;
    }

    static public function setToken($id, $token) {
        global $db;
        $res = $db->query("UPDATE admins SET token=? WHERE id=?", [$token, $id]);
        return $res ? true : false;
    }


}
