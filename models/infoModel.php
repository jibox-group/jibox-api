<?php
class INFO {

    static public function all() {
        global $db;
        $res = $db->query("
            SELECT
                (SELECT COUNT(cs.id) FROM customers cs) AS countCustomers,
                (SELECT COUNT(c.id) FROM clients c) AS countClients,
                (SELECT COUNT(e.id) FROM estates e WHERE e.status<>'delete') AS countEstates,
                (SELECT COUNT(u.id) FROM users u WHERE u.deleted=0 AND registered=1) AS countUsers,
                (SELECT COUNT(r.id) FROM requests r) AS countRequests,
                (SELECT COUNT(a.id) FROM admins a) AS countAdmins,
                (SELECT COUNT(o.id) FROM offers o) AS countOffers,
                (SELECT COUNT(c.id) FROM companies c) AS countCompanies,
                (SELECT COUNT(t.id) FROM tariffs t WHERE t.status='active') AS countTariffs
            ");
        $info = $res->fetch();
        return $info ? $info : false;
    }

}
