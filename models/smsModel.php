<?php
class SMS {
    
    static public function getToken() {
        global $config;
        $params = [
            "email" => $config['sms']['email'],
            "password" => $config['sms']['password'],
        ];
        $res = json_decode(json_encode(self::request($config['sms']['url']."auth/login", $params)), true);
        $jsonIterator = new RecursiveIteratorIterator(
            new RecursiveArrayIterator(json_decode($res, TRUE)),
            RecursiveIteratorIterator::SELF_FIRST);
        $token = '';
        foreach ($jsonIterator as $key => $val) {
            if(!is_array($val)) {
                if($key == 'token') {
                    $token = $val;
                    return $token;
                }
            }
        }
        return $token;
    }

    static public function send($to, $msg = "Тестовое сообщение") {
        global $config;
        $token = self::getToken();
        $params = [
            "mobile_phone" => $to,
            "message" => $msg
        ];
        $headers = [
            "Authorization: Bearer ".$token
        ];
        return self::request($config['sms']['url']."message/sms/send", $params, $headers);
    }

    static private function request($url, $params, $headers = []) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HTTP_VERSION  , CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST , "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responce = curl_exec($ch);
        curl_close ($ch);
        return $responce;
    }

}
