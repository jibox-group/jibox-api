<?php
class CURRENCY {

    static public function getRatio($id) {
        global $db;
        $res = $db->query(
            "SELECT c.ratio
            FROM currencies c
            WHERE c.id=?",
            [$id]
        );
        $currency = $res->fetch();
        if($currency) return $currency['ratio'] ? $currency['ratio'] : 1;
        return 1;
    }

}