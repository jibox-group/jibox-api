<?php
class USER {

    static public function delete($id) {
        global $db;
        $res = $db->query("DELETE FROM users WHERE id=?", [$id]);
        return $res ? true : false;
    }

    static public function checkLimitByClient($user) {
        $engage = ENGAGE::getActiveByOwner($user['id']);
        $tariff = TARIFF::getById($user['tariff_id']);
        return CLIENT::getCount($user['id']) < $tariff['clients'];
    }

    static public function checkLimitByEstate($user) {
        $engage = ENGAGE::getActiveByOwner($user['id']);
        $tariff = TARIFF::getById($user['tariff_id']);
        return $engage && ESTATE::getCount($user['id']) < $tariff['estates'];
    }

    static public function checkLimitByRequests($user) {
        $engage = ENGAGE::getActiveByOwner($user['id']);
        $tariff = TARIFF::getById($user['tariff_id']);
        return $engage && REQUEST::getCount($user['id']) < $tariff['requests'];
    }

    static public function isLimited($user) {
        return $user['status'] == 'limited';
    }

    static public function isBlocked($user) {
        return $user['status'] == 'block';
    }

    static public function isActive($user) {
        return $user['status'] == 'active';
    }

    static public function getByToken($token, $status = true) {
        global $db;
        $whereStatus = "";
        if($status) {
            $whereStatus = " AND (u.status='active' OR u.status='limited')";
        }
        $res = $db->query(
            "SELECT u.*,
                          (SELECT COUNT(c.id) FROM clients c WHERE c.user_id=u.id) AS countClients,
                          (SELECT COUNT(e.id) FROM estates e JOIN clients c2 ON c2.id=e.owner WHERE c2.user_id=u.id AND e.status<>'delete') AS countEstates,
                          (SELECT COUNT(e.id) FROM estates e JOIN clients c2 ON c2.id=e.owner WHERE c2.user_id=u.id AND e.status='archive') AS countArchives,
                          (SELECT COUNT(e.id) FROM estates e JOIN clients c2 ON c2.id=e.owner WHERE c2.user_id=u.id AND e.status<>'delete' AND e.status<>'archive') AS countActives,
                          (SELECT t.title FROM tariffs t WHERE t.id=u.tariff_id) AS tariffName
            FROM users u
            WHERE u.token=? AND u.deleted=?".$whereStatus,
            [$token, 0]
        );
        $user = $res->fetch();
        if($user) {
            $user['registered'] = $user['registered'] ? true : false;
            $user['licensed'] = $user['licensed'] ? true : false;
            $user['is_watermark'] = $user['is_watermark'] ? true : false;
            $user['districtId'] = explode(',', $user['districtId']);
            foreach($user['districtId'] as $i => $district){
                $user['districtId'][$i] = $district * 1;
            }
            return $user;
        }
        else return false;
    }

    static public function getById($id) {
        global $db, $header;
        $res = $db->query(
            "SELECT u.*,
                          u.tariff_id as tariffId,
                          (SELECT COUNT(c.id) FROM clients c WHERE c.user_id=u.id) AS countClients,
                          (SELECT COUNT(e.id) FROM estates e JOIN clients c2 ON c2.id=e.owner WHERE c2.user_id=u.id AND e.status<>'delete') AS countEstates,
                          (SELECT COUNT(e.id) FROM estates e JOIN clients c2 ON c2.id=e.owner WHERE c2.user_id=u.id AND e.status='archive') AS countArchives,
                          (SELECT COUNT(e.id) FROM estates e JOIN clients c2 ON c2.id=e.owner WHERE c2.user_id=u.id AND e.status<>'delete' AND e.status<>'archive') AS countActives,
                          (SELECT t.title FROM tariffs t WHERE t.id=u.tariff_id) AS tariffName
            FROM users u
            WHERE u.id=? AND u.deleted=?",
            [$id, 0]
        );
        $user = $res->fetch();
        
        if($user) {
            $expire = 1;
            $settings = SETTING::get();
            $version = $settings['last_version'];
            $current_versions = explode('.', $header['version-name']);
            $last_versions = explode('.', $version);
            if(
                (($current_versions[0] * 1) >= ($last_versions[0] * 1) == 1) &&
                (($current_versions[1] * 1) >= ($last_versions[1] * 1) == 1) &&
                (($current_versions[2] * 1) >= ($last_versions[2] * 1) == 1)
            ) {
                $expire = 0;
            }
            $user['registered'] = $user['registered'] ? true : false;
            $user['licensed'] = $user['licensed'] ? true : false;
            $user['is_watermark'] = $user['is_watermark'] ? true : false;
            $user['status'] = $expire ? 'need_update' : $user['status'];
            $user['districtId'] = explode(',', $user['districtId']);
            foreach($user['districtId'] as $i => $district){
                $user['districtId'][$i] = intval($district);
            }
            return $user;
        }
        else return false;
    }

    static public function getByPhone($phone) {
        global $db;
        $res = $db->query(
            "SELECT
                u.*
            FROM
                users u
            WHERE
                u.phone=? AND u.deleted=?",
            [$phone, 0]
        );
        $user = $res->fetch();
        if($user) {
            $user['registered'] = $user['registered'] ? true : false;
            $user['licensed'] = $user['licensed'] ? true : false;
            $user['is_watermark'] = $user['is_watermark'] ? true : false;
            $user['districtId'] = explode(',', $user['districtId']);
            foreach($user['districtId'] as $i => $district){
                $user['districtId'][$i] = intval($district);
            }
            return $user;
        }
        else return false;
    }

    static public function getAll() {
        global $db;
        $res = $db->query("SELECT u.* FROM users u WHERE u.deleted=? AND u.registered=?", [0, 1]);
        if($res){
            return $res->fetchAll();
        } else {
            return false;
        }
    }

    static public function createEmpty($phone) {
        global $db, $fun;
        $token = $fun->generateToken();
        $time = $fun->time();
        $res = $db->query(
            "INSERT INTO users(phone, fullname, token, date_create)
                VALUES(?, ?, ?, ?)",
            [$phone, '', $token, $time]
        );

        if($res) $res = $db->query("SELECT id FROM users ORDER BY id DESC LIMIT 1");
        $user = $res->fetch();
        return [
            'id' => $user['id'],
            'phone' => $phone,
            'token' => $token,
            'gender' => '',
            'photo' => '',
            'is_watermark' => false,
            'watermark' => '',
            'date_birth' => 0,
            'date_create' => $time,
            'registered' => false
        ];
    }

    static public function update($id) {
        global $db, $body, $files, $const, $fun;
        $parameters = array(1);
        $time = $fun->time();

        if($body->fullname) array_push($parameters, $body->fullname);
        if($body->gender) array_push($parameters, $body->gender);
        if($body->date_birth) array_push($parameters, $body->date_birth);
        if($body->watermark) array_push($parameters, $body->watermark);
        if($body->regionId) array_push($parameters, $body->regionId);
        if($body->districtId) array_push($parameters, implode(',', $body->districtId));

        if ($files['image']){
            $filename = FILE::upload($files['image']);
            if($filename) {
                array_push($parameters, $filename);
            }
            else new Errors($const['fileNotUploaded']);
        }
        array_push($parameters, $id);

        $res = $db->query("UPDATE users SET registered=?"
            .($body->fullname ? ", fullname=?" : "")
            .($body->gender ? ", gender=?" : "")
            .($body->date_birth ? ", date_birth=?" : "")
            .($body->watermark ? ", watermark=?" : "")
            .($body->regionId ? ", regionId=?" : "")
            .($body->districtId ? ", districtId=?" : "")
            .($body->is_watermark == true ? ", is_watermark=1" : ($body->is_watermark == false ? ", is_watermark=0" : ""))
            .($files['image'] ? ", image=?" : "")
            ." WHERE id=?", $parameters);
        if($res && !ENGAGE::hasStart($id)) {
            ENGAGE::createStart($id);
        }
        return $res ? true : false;
    }

    static public function setToken($id, $token="") {
        global $db, $fun;
        $res = $db->query("UPDATE users SET `token`=? WHERE id=?", [$token, $id]);
        return $res ? true : false;
    }

    static public function setFCMToken($id, $fcm_token="") {
        global $db, $fun;
        $res = $db->query("UPDATE users SET `fcm_token`=? WHERE id=?", [$fcm_token, $id]);
        return $res ? true : false;
    }

    static public function setUserCompany($id, $companyId=NULL) {
        global $db, $fun;
        $res = $db->query("UPDATE users SET `companyId`=? WHERE id=?", [$companyId, $id]);
        return $res ? true : false;
    }

    static public function changeStatus($id, $status = 'limited') {
        global $db, $fun;
        $res = $db->query("UPDATE users SET `status`=? WHERE id=? AND deleted=?", [$status, $id, 0]);
        return $res ? true : false;
    }

    static public function changeLicense($id, $license = false) {
        global $db, $fun;
        $res = $db->query("UPDATE users SET `licensed`=? WHERE id=? AND deleted=?", [$license?1:0, $id, 0]);
        return $res ? true : false;
    }

    static public function changeCanOwner($id, $can_add_from_owner = false) {
        global $db, $fun;
        $res = $db->query("UPDATE users SET `can_add_from_owner`=? WHERE id=? AND deleted=?", [$can_add_from_owner?1:0, $id, 0]);
        return $res ? true : false;
    }

    static public function changeCanAddFromOwner($id, $can_add_from_owner = false) {
        global $db, $fun;
        $res = $db->query("UPDATE users SET `can_add_from_owner`=? WHERE id=? AND deleted=?", [$can_add_from_owner?1:0, $id, 0]);
        return $res ? true : false;
    }

    static public function changeTariff($id, $tariffId = 1) {
        global $db, $fun;
        $res = $db->query("UPDATE users SET `tariff_id`=? WHERE id=? AND deleted=?", [$tariffId, $id, 0]);
        return $res ? true : false;
    }

    static public function removeImage($id, $image) {
        global $db;
        $res = $db->query("SELECT * FROM users WHERE id=? AND deleted=?", [$id, 0]);
        $file = $res->fetch();
        unlink('file/'. $file[$image]);
    }

    static public function getAllUsers() {
        global $db, $req, $fun;

        $where = " WHERE u.registered=1 AND u.deleted=0";
        $pagination = "";

        if($req['keyword']){
            $where .= " AND (u.phone LIKE '%".$req['keyword']."%' OR u.fullname LIKE '%".$req['keyword']."%' OR u.watermark LIKE '%".$req['keyword']."%' OR u.date_birth LIKE '%".$req['keyword']."%')";
        }
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }
        $resCount = $db->query("SELECT COUNT(u.id) AS count FROM users u".$where);
        $count = $resCount->fetch();
        $res = $db->query("SELECT 
                          u.id,
                          u.status,
                          u.balance,
                          u.licensed,
                          u.fullname,
                          u.phone,
                          u.gender,
                          u.companyId,
                          u.can_add_from_owner,
                          u.tariff_id AS tariffId,
                          u.is_watermark AS isWatermark,
                          u.watermark,
                          u.regionId,
                          u.districtId,
                          u.registered,
                          u.image,
                          u.date_create AS dateCreate,
                          u.date_birth AS dateBirth,
                          u.deleted,
                          (SELECT COUNT(c.id) FROM clients c WHERE c.user_id=u.id) AS countClients,
                          (SELECT COUNT(e.id) FROM estates e JOIN clients c2 ON c2.id=e.owner WHERE c2.user_id=u.id AND e.status<>'delete') AS countEstates,
                          (SELECT COUNT(e.id) FROM estates e JOIN clients c2 ON c2.id=e.owner WHERE c2.user_id=u.id AND e.status='archive') AS countArchives,
                          (SELECT COUNT(e.id) FROM estates e JOIN clients c2 ON c2.id=e.owner WHERE c2.user_id=u.id AND e.status<>'delete' AND e.status<>'archive') AS countActives,
                          (SELECT t.title FROM tariffs t WHERE t.id=u.tariff_id) AS tariffName
                          FROM users u" .
                $where .
                $pagination
            );
        if($res) {
            $result = [
                "count" => $count['count'],
                "items" => $res->fetchAll()
            ];
            return $result;
        } else {
            return false;
        }
    }

}
