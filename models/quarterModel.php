<?php
class QUARTER {

    static public function create($name, $district_id) {
        global $db;
        $res = $db->query(
            "INSERT INTO quarters SET
                `name`=?, `district_id`=?",
            [$name, $district_id]
        );
        if($res) {
            $r = $db->query(
                "SELECT q.id
            FROM quarters q
            ORDER BY id DESC LIMIT 0, 1"
            );
            $quarter = $r->fetch();
            return $quarter['id'];
        }
        else return false;
    }

    static public function getAll() {
        global $db, $fun, $req;
        $where = " WHERE 1=1";
        $pagination = "";

        if($req['keyword']){
            $where .= " AND q.name LIKE '%".$req['keyword']."%'";
        }
        if($req['district_id']){
            $where .= " AND q.district_id=".$req['district_id'];
        }
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }
        $resCount = $db->query("SELECT COUNT(id) as count FROM quarters q".$where);
        $count = $resCount->fetch();
        $res = $db->query("SELECT q.* FROM quarters q".$where.$pagination);
        $quarters = $res->fetchAll();
        foreach ($quarters as $i => $quarter) {
            $district = DISTRICT::get($quarter['district_id']);
            $quarters[$i]['regionName'] = REGION::getName($district['region_id']);
            $quarters[$i]['regionId'] = $district['region_id'];
            $quarters[$i]['districtName'] = $district['name'];
        }
        $result = [
            "count" => $count['count'],
            "items" => $quarters
        ];
        return $result;
    }

    static public function getName($id) {
        global $db;
        $res = $db->query(
            "SELECT q.name
            FROM quarters q
            WHERE q.id=?",
            [$id]
        );
        $quarter = $res->fetch();
        if($quarter) {
            return $quarter['name'];
        }
        else return null;
    }

    static public function delete($id) {
        global $db;
        $res = $db->query(
            "DELETE FROM quarters
            WHERE id=?",
            [$id]
        );
        return $res ? true : false;
    }

    static public function update($id, $districtId, $name) {
        global $db;
        $res = $db->query(
            "UPDATE quarters SET name=?, district_id=?
            WHERE id=?",
            [$name, $districtId, $id]
        );
        return $res ? true : false;
    }

}
