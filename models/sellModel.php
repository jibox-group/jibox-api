<?php
class SELL {

    static public function getStatistics($owner) {
        global $db, $statistics;
        $res = $statistics;

        $temp = [12][31];
        $r = $db->query(
            "SELECT
                    s.*
                 FROM sells s
                  WHERE s.owner=?", [$owner]
        );
        $sells = $r->fetchAll();
        foreach($sells as $i => $sell) {
            $time = floor($sell['date_create'] / 1000);
            $date = explode(" ", date("Y m d", $time));
            if($date[0] == date("Y")){
                $m = $date[1] * 1;
                $d = $date[2] * 1;
                $temp[$m][$d] += 1;
            }
        }
        foreach ($temp as $i => $item) {
            foreach ($item as $j => $value) {
                switch ($i) {
                    case 1 : $res['januaryCount'] += $value; $res['january'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 2 : $res['februaryCount'] += $value; $res['february'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 3 : $res['marchCount'] += $value; $res['march'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 4 : $res['aprilCount'] += $value; $res['april'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 5 : $res['mayCount'] += $value; $res['may'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 6 : $res['juneCount'] += $value; $res['june'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 7 : $res['julyCount'] += $value; $res['july'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 8 : $res['augustCount'] += $value; $res['august'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 9 : $res['septemberCount'] += $value; $res['september'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 10 : $res['octoberCount'] += $value; $res['october'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 11 : $res['novemberCount'] += $value; $res['november'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 12 : $res['decemberCount'] += $value; $res['december'][$j-1] = ["day" => $j, "count" => $value]; break;
                }
            }
        }
        return $res;
    }

    static public function create($estateId, $owner, $customer) {
        global $db, $fun;
        $time = $fun->time();
        $parameters = array($estateId, $owner, $customer, $time);

        $res = $db->query(
            "INSERT INTO sells SET
                        target=?,
                        owner=?,
                        customer=?,
                        date_create=?",
            $parameters
        );

        if ($res) $res = $db->query("SELECT id FROM sells ORDER BY id DESC LIMIT 1");
        $sell = $res->fetch();
        if($sell) return $sell['id'];
        else return false;
    }

}
