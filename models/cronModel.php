<?php
class CRON {

    static public function getActiveUsers() {
        global $db;
        $res = $db->query("SELECT * FROM users WHERE registered=1 AND deleted=0 AND (status='active' OR status='start' OR status='limited')");
        return $res->fetchAll();
    }

    static public function pay($user, $price, $comment = '') {
        global $db;
        $pay = $user['balance'] + $price;
        if($pay >= 0) {
            $res = $db->query("UPDATE users SET balance=? WHERE id=? AND deleted=?", [$pay, $user['id'], 0]);
            if($res) {
                TRANSACTION::create($user['id'], $price, $comment);
                NOTIFICATION::create($user['id'], $comment, 'Ваш баланс: '.$pay.' сум', 'system');
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
