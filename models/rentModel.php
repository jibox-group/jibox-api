<?php
class RENT {

    static public function getStatistics($owner) {
        global $db, $statistics;
        $res = $statistics;

        $temp = [12][31];
        $r = $db->query(
            "SELECT
                    r.*
                 FROM rents r
                  WHERE r.owner=?", [$owner]
        );
        $rents = $r->fetchAll();
        foreach($rents as $i => $rent) {
            $time = floor($rent['date_create'] / 1000);
            $date = explode(" ", date("Y m d", $time));
            if($date[0] == date("Y")){
                $m = $date[1] * 1;
                $d = $date[2] * 1;
                $temp[$m][$d] += 1;
            }
        }
        foreach ($temp as $i => $item) {
            foreach ($item as $j => $value) {
                switch ($i) {
                    case 1 : $res['januaryCount'] += $value; $res['january'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 2 : $res['februaryCount'] += $value; $res['february'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 3 : $res['marchCount'] += $value; $res['march'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 4 : $res['aprilCount'] += $value; $res['april'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 5 : $res['mayCount'] += $value; $res['may'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 6 : $res['juneCount'] += $value; $res['june'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 7 : $res['julyCount'] += $value; $res['july'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 8 : $res['augustCount'] += $value; $res['august'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 9 : $res['septemberCount'] += $value; $res['september'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 10 : $res['octoberCount'] += $value; $res['october'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 11 : $res['novemberCount'] += $value; $res['november'][$j-1] = ["day" => $j, "count" => $value]; break;
                    case 12 : $res['decemberCount'] += $value; $res['december'][$j-1] = ["day" => $j, "count" => $value]; break;
                }
            }
        }
        return $res;
    }

    static public function create($estateId, $owner, $customer, $expire) {
        global $db, $fun;
        $time = $fun->time();
        $parameters = array($estateId, $owner, $customer, $time, $expire);

        $res = $db->query(
            "INSERT INTO rents SET
                        target=?,
                        owner=?,
                        customer=?,
                        date_create=?,
                        date_expire=?",
            $parameters
        );

        if ($res) $res = $db->query("SELECT id FROM rents ORDER BY id DESC LIMIT 1");
        $rent = $res->fetch();
        if($rent) return $rent['id'];
        else return false;
    }

}
