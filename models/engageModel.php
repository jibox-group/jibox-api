<?php
class ENGAGE {

    static public function expire($id) {
        global $db, $fun;
        $time = $fun->time();
        $res = $db->query("UPDATE engages SET status='expire', end_time=? WHERE id=?", [$time, $id]);
        return $res ? true : false;
    }

    static public function getActiveByOwner($owner) {
        global $db;
        $res = $db->query("SELECT * FROM engages WHERE owner=? AND status=?", [$owner, 'active']);
        $engage = $res->fetch();
        return $engage ? $engage : false;
    }

    static public function hasStart($owner) {
        global $db;
        $res = $db->query("SELECT * FROM engages WHERE owner=? AND tariff_id=1", [$owner]);
        $engage = $res->fetch();
        return $engage ? true : false;
    }

    static public function createStart($owner) {
        global $fun, $db;
        $time = $fun->time();
        $tariff_id = 1;
        $status = 'active';
        $user = USER::getById($owner);
        if($user['registered'] == 1) {
            $db->query("INSERT INTO engages SET owner=?, tariff_id=?, status=?, start_date=?",
                [$owner, $tariff_id, $status, $time]);
        }
    }
    static public function createDefault($engage) {
        global $fun, $db;
        $time = $fun->time();
        $user = USER::getById($engage['owner']);
        $status = 'active';
        if($user['registered'] == 1) {
            ENGAGE::expire($engage['id']);
            return $db->query("INSERT INTO engages SET owner=?, tariff_id=?, status=?, start_date=?",
                [$user['id'], $engage['tariff_id'], $status, $time]);
        }
    }

}
