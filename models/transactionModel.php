<?php
class TRANSACTION {
    static public function create($owner, $price, $comment = '') {
        global $fun, $db;
        $time = $fun->time();
        $user = USER::getById($owner);
        if($user['registered'] == 1) {
            $res = $db->query("INSERT INTO transactions SET owner=?, price=?, comment=?, create_date=?",
                [$owner, $price, $comment, $time]);
            return $res ? true : false;
        }
    }
    static public function getByOwner($owner) {
        global $fun, $db, $req;

        $where = " WHERE t.owner=".$owner;
        $pagination = "";
        $sort = " ORDER BY create_date DESC";

        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }
        $resCount = $db->query("SELECT COUNT(t.id) as count FROM transactions t".$where);
        $count = $resCount->fetch();
        $items = $db->query("SELECT * FROM transactions t".$where.$sort.$pagination);
        $res = [
          "count" => $count['count'],
          "items" => $items->fetchAll()
        ];
        return $count && $items ? $res : false;
    }
    
}
