<?php
class WHITELIST {

    static public function getAll() {
        global $db;
    
        global $db, $req, $fun, $body;
        $pagination = '';
        $sort = '';
    
        if ($body->sort && $body->direction) {
            $sort = " ORDER BY " . $body->sort . " " . $body->direction;
        } else {
            $sort = " ORDER BY id DESC";
        }
        if ($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT ' . $offset . ', ' . $req['limit'];
        }
    
        $res = $db->query(
            "SELECT
                    COUNT(w.id) AS count
                 FROM whitelist w WHERE 1" .
            ($req['keyword'] ? " AND (w.phone LIKE '%" . $req['keyword'] . "%' OR w.comment LIKE '%" . $req['keyword'] . "%')" : "")
        );
    
        $count = $res->fetch();
    
        $res = $db->query(
            "SELECT
                    w.*
                 FROM whitelist w WHERE 1" .
            ($req['keyword'] ? " AND (w.phone LIKE '%" . $req['keyword'] . "%' OR w.comment LIKE '%" . $req['keyword'] . "%')" : "").
            $sort .
            $pagination
        );
        
        $result = [
            'items' => $res->fetchAll(),
            'count' => $count['count']
        ];

        return $res ? $result : false;
    }

    static public function getByPhone($phone) {
        global $db;

        $res = $db->query(
            "SELECT * FROM whitelist WHERE phone=?",
            [$phone]
        );
        $whitelist = $res->fetch();

        return $whitelist ? $whitelist : false;
    }

    static public function create($phone, $comment = '') {
        global $db;
        $parameters = array($phone, $comment);

        $res = $db->query(
            "INSERT INTO whitelist SET
                        phone=?,
                        comment=?",
            $parameters
        );

        return $res ? true : false;
    }

    static public function delete($id) {
        global $db;
        $parameters = array($id);
        $res = $db->query(
            "DELETE FROM whitelist WHERE id=?",
            $parameters
        );

        return $res ? true : false;
    }

}
