<?php
class FRIEND {

    static public function detail($id, $id1) {
        global $db;
        $res = $db->query(
            "SELECT f.*
            FROM friends f
            WHERE (f.owner=? AND f.friend=?) OR (f.friend=? AND f.owner=?)",
            [$id, $id1, $id, $id1]
        );
        $friend = $res->fetch();
        if($friend){
            $res = $db->query("SELECT * FROM users WHERE id=?", [$id1]);
            $frnd = $res->fetch();
            $friend['owner'] = $friend['owner'] == $id;
            $friend['friend'] = $id1;
            $friend['friendPhone'] = $frnd['phone'];
            $friend['friendName'] = $frnd['fullname'];
            $friend['friendImage'] = $frnd['image'];
            return $friend;
        }
        else return false;
    }

    static public function get($id, $id1) {
        global $db;
        $res = $db->query(
            "SELECT f.*
            FROM friends f
            WHERE (f.owner=? AND f.friend=?) OR (f.friend=? AND f.owner=?)",
            [$id, $id1, $id, $id1]
        );
        $friend = $res->fetch();
        return $friend ? $friend : false;
    }

    static public function getAll($id, $type = 'all', $status = 'all') {
        global $db;
        $params = [$id, $id];
        $typeRequest = '';
        $statusRequest = '';
        if($type != 'all'){
            $typeRequest = ' AND f.type=?';
            array_push($params, $type);
        }
        if($status != 'all'){
            $statusRequest = ' AND f.status=?';
            array_push($params, $status);
        }
        $res = $db->query(
            "SELECT f.*
            FROM friends f
            WHERE (f.owner=? OR f.friend=?)".
            $typeRequest.
            $statusRequest.
            " ORDER BY f.time DESC",
            $params
        );
        $friends = [];
        while($friend = $res->fetch()){
            $friendId = $friend['owner'] == $id ? $friend['friend'] : $friend['owner'];
            $result = $db->query("SELECT * FROM users WHERE id=?", [$friendId]);
            $frnd = $result->fetch();
            $friend['owner'] = $friend['owner'] == $id;
            $friend['friend'] = $friendId;
            $friend['friendName'] = $frnd['fullname'];
            $friend['friendImage'] = $frnd['image'];
            $friend['owner'] = $friend['owner'] == $id;
            array_push($friends, $friend);
        }
        return $res ? $friends : false;
    }

    static public function add($owner, $friend, $type) {
        global $db, $fun;
        $time = $fun->time();
        $status = 'request';
        $res = $db->query(
            "INSERT INTO friends SET
              owner=?, friend=?, `time`=?, `type`=?, `status`=?",
            [$owner, $friend, $time, $type, $status]
        );
        $frnd = USER::getById($owner);
        $type = 'offer';
        $title = 'Связаться с другом';
        $text = 'Запрос дружбы от '.$frnd['fullname'].', чтобы поделиться вашей информацией';
        if($res) NOTIFICATION::create($friend, $title, $text, $type, $owner);
        return $res ? true : false;
    }

    static public function confirm($friend, $owner) {
        global $db, $fun;
        $time = $fun->time();
        $status = 'confirm';
        $res = $db->query(
            "UPDATE friends SET
              `status`=? WHERE owner=? AND friend=?",
            [$status, $owner, $friend]
        );
        return $res ? true : false;
    }

    static public function remove($friend, $owner) {
        global $db;
        $res = $db->query(
            "DELETE FROM friends
              WHERE (owner=? AND friend=?) OR (friend=? AND owner=?)",
            [$owner, $friend, $owner, $friend]
        );
        return $res ? true : false;
    }

}
