<?php
class TOKEN {

    static public function deleteById($id) {
        global $db;
        $res = $db->query("DELETE FROM tokens WHERE id=?", [$id]);
        return $res ? true : false;
    }
    
    static public function deleteLastLogin($owner) {
        global $db;
        $res = $db->query("SELECT id FROM tokens WHERE owner=? ORDER BY created_at ASC LIMIT 0, 1", [$owner]);
        $token = $res->fetch();
        $res = $db->query("DELETE FROM tokens WHERE id=?", [$token['id']]);
        return $res ? true : false;
    }

    static public function getByOwnerId($id) {
        global $db;
        $res = $db->query(
            "SELECT *
            FROM tokens t
            WHERE t.owner=?",
            [$id]
        );
        $tokens = $res->fetchAll();
        return $tokens ? $tokens : false;
    }
    
    static public function getByToken($token) {
        global $db, $header;
        $res = $db->query(
            "SELECT *
            FROM tokens t
            WHERE t.auth=?",
            [$token]
        );
        $tokens = $res->fetch();
        TOKEN::setInfoByToken($token, $header['version-code'], $header['version-name'], $header['device']);
        return $tokens ? $tokens : false;
    }
    
    static public function getAll($userId) {
        global $db, $req, $fun, $body;
        $parameters = array();
        $pagination = '';
        
        if($body->sort && $body->direction) {
            $sort = " ORDER BY ".$body->sort." ".$body->direction;
        } else {
            $sort = " ORDER BY t.last_seen DESC";
        }
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }
        array_push($parameters, $userId);
        
        $res = $db->query(
            "SELECT
                    COUNT(t.id) AS count
                 FROM tokens t WHERE t.owner=?",
            $parameters
        );
        
        $count = $res->fetch();
        
        $res = $db->query(
            "SELECT
                    t.*
                 FROM tokens t WHERE t.owner=?" .
            $sort .
            $pagination,
            $parameters
        );
        
        if ($res) {
            $tokens = [];
            while ($token = $res->fetch()) {
                array_push($tokens, $token);
            }
            $result['count'] = $count['count'];
            $result['items'] = $tokens;
            return $result;
        } else return false;
    }

    static public function create($owner) {
        global $db, $fun;
        $token = $fun->generateToken();
        $res = $db->query(
            "INSERT INTO tokens(auth, owner)
                VALUES(?, ?)",
            [$token, $owner]
        );
        self::checkLimit($owner);

        if($res) $res = $db->query("SELECT id, auth, created_at FROM tokens ORDER BY id DESC LIMIT 1");
        $token = $res->fetch();
        return [
            'id' => $token['id'],
            'owner' => $owner,
            'auth' => $token['auth'],
            'fcm' => '',
            'created_at' => $token['created_at'],
        ];
    }

    static public function setFCMToken($id, $fcm_token="") {
        global $db, $fun;
        $res = $db->query("UPDATE tokens SET `fcm`=? WHERE id=?", [$fcm_token, $id]);
        return $res ? true : false;
    }

    static public function setInfoByToken($token, $version_code=NULL, $version_name=NULL, $device=NULL) {
        global $db, $fun;
        $time = $fun->time();
        $res = $db->query("UPDATE tokens SET `version_code`=?, `version_name`=?, `device`=?, `ip`=?, `last_seen`=? WHERE auth=?", [$version_code, $version_name, $device, $_SERVER['REMOTE_ADDR'], $time, $token]);
        return $res ? true : false;
    }
    
    static private function checkLimit($owner) {
        global $db, $fun, $config;
        $res = $db->query("SELECT id FROM tokens WHERE owner=?", [$owner]);
        if($res) {
            $tokens = $res->fetchAll();
            if(count($tokens) > $config['maxCountLogin']) self::deleteLastLogin($owner);
        }
    }

}
