<?php
class NOTIFICATION {

    static public function getAll($userId) {
        global $db, $req, $fun, $body;
        $parameters = array();
        $pagination = '';

        if($body->sort && $body->direction) {
            $sort = " ORDER BY ".$body->sort." ".$body->direction;
        } else {
            $sort = " ORDER BY time DESC";
        }
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }
        array_push($parameters, $userId);

        $res = $db->query(
            "SELECT 
                    COUNT(n.id) AS count
                 FROM notifications n WHERE n.owner=?" .
            ($body->keyword ? " AND n.title LIKE '%".$body->keyword."%'" : "").
            ($req['type'] ? " AND n.type='".$req['type']."'" : ""),
            $parameters
        );

        $count = $res->fetch();

        $res = $db->query(
            "SELECT 
                    n.*
                 FROM notifications n WHERE n.owner=?" .
            ($body->keyword ? " AND n.title LIKE '%".$body->keyword."%'" : "") .
            ($req['type'] ? " AND n.type='".$req['type']."'" : "") .
            $sort .
            $pagination,
            $parameters
        );

        if ($res) {
            $notifications = [];
            while ($notification = $res->fetch()) {
                $notification['readed'] = $notification['readed'] == 1;
                array_push($notifications, $notification);
            }
            $result['count'] = $count['count'];
            $result['items'] = $notifications;
            return $result;
        } else return false;
    }

    static public function create($owner, $title, $text, $type = 'admin', $extraId = 0) {
        global $db, $fun;
        $time = $fun->time();
        $tokens = TOKEN::getByOwnerId($owner);
        $db->query(
            "INSERT INTO notifications SET
                `owner`=?, `type`=?, `title`=?, `text`=?, `extraId`=?, `time`=?",
            [$owner, $type, $title, $text, $extraId, $time]
        );
        $sendTokens = [];
        foreach ($tokens as $i => $token){
            if(!in_array($token['fcm'], $sendTokens)){
                $to = $token['fcm'];
                $notification = [
                    "body" => $text,
                    "title" => $title,
                    "priority" => "high",
                    'vibrate' => 1,
                    'sound' => 1
                ];
                $data = [
                    "type" => $type,
                    "extraId" => $extraId,
                ];
                FCM::send($to, $notification, $data);
                array_push($sendTokens, $token['fcm']);
            }
        }
        return true;
    }

    static public function get($id, $owner) {
        global $db;
        $res = $db->query(
            "SELECT * FROM notifications WHERE id=? AND owner=?",
            [$id, $owner]
        );
        $notification = $res->fetch();
        if($notification) {
            $db->query("UPDATE notifications SET readed=1 WHERE id=?", [$notification['id']]);
            return $notification;
        }
        else return [];
    }

}
