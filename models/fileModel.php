<?php
class FILE {

    static public function upload($file) {
        global $fun;
        $time = $fun->time();
        if ($file){
            $filename = $time.'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
            if(move_uploaded_file($file['tmp_name'], 'file/'.$filename)){
                //$f = FILE::compressImage(DIR.'/file/'.$filename, DIR.'/file/'.$filename, 80);
                if (file_exists('file/'.$filename)) {
                    return $filename;
                }
                return false;
            }
        }
        return false;
    }

    static public function uploadAd($file) {
        if ($file){
            $filename = 'default.gif';
            if(move_uploaded_file($file['tmp_name'], 'ad/'.$filename)){
                if (file_exists('ad/'.$filename)) {
                    return $filename;
                }
                return false;
            }
        }
        return false;
    }

    static public function compressImage($src, $dst, $quality = 80) {
        $info = getimagesize($src);

        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($src);
        elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($src);
        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($src);

        $width = $info[0];
        $height = $info[1];

        if($width > 1280 || $height > 720){
            $percent = $width / 1280;
            $newwidth = $width / $percent;
            $newheight = $height / $percent;

            $thumb = imagecreatetruecolor($newwidth, $newheight);
            $source = imagecreatefromjpeg($image);

            imagecopyresized($image, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

            imagejpeg($image, $dst, $quality);
        }
        return $dst;
    }

}
