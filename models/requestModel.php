<?php
class REQUEST {

    static public function get($userId, $id) {
        global $db, $fun;
        $parameters = array();
        array_push($parameters, $id);

        $res = $db->query("SELECT r.* FROM requests r WHERE r.id=?", $parameters);
        $request = $res->fetch();
    
        $request['request_from_owner'] = $request['request_from_owner'] ? true : false;
        $request['readUsers'] = $request['readUsers'] ? explode(',', $request['readUsers']) : [];
        array_push($request['readUsers'], $userId);
        
        $db->query("UPDATE requests r SET r.readUsers = ? WHERE r.id=?", [implode(',', $request['readUsers']), $id]);
        $request['read'] = true;

        return $request ? $request : false;
    }

    static public function getCount($userId) {
        global $db;
        $res = $db->query(
            "SELECT COUNT(r.id) AS count FROM requests r WHERE r.owner=?",
            [$userId]
        );
        $estates = $res->fetch();
        return $estates ? $estates['count'] : 0;
    }

    static public function getAll($userId) {
        global $db, $req, $fun, $body;
        $parameters = array();
        $pagination = '';
        $filterQuarter = '';
        $filterDistrict = '';
        $where = " WHERE r.owner<>$userId";

        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }

        if($body->get == 'all' || $body->request_from_owner){
            $where = " WHERE 1=1";
        } else if($body->get == 'my'){
            $where = " WHERE r.owner=$userId";
        } else if($body->get == 'other'){
            $where = " WHERE r.owner<>$userId";
        }

        if($body->marketType && $body->marketType != 'all'){
            $where .= " AND r.marketType='".$body->marketType."'";
        }

        if($body->type && $body->type != 'all'){
            $where .= " AND r.type='".$body->type."'";
        }

        if($body->metroId){
            $where .= " AND r.metroId=".$body->metroId;
        }

        if($body->regionId){
            $where .= " AND r.regionId=".$body->regionId;
        }

        if($body->request_from_owner) {
            $where .= " AND r.request_from_owner=1";
        } else {
            $where .= " AND r.request_from_owner=0";
            if(count($body->quarterId) > 0){
                $filterQuarter = " AND (1<>1 OR";
                foreach($body->quarterId as $key => $quarterId){
                    $filterQuarter .= " r.quarterId LIKE '%,".$quarterId."%' OR r.quarterId=".$quarterId;
                    if(count($body->quarterId) != $key + 1){
                        $filterQuarter .= " OR";
                    }
                }
                $filterQuarter .= ")";
            }
            if(count($body->districtId) > 0 && $filterQuarter == ''){
                $filterDistrict = " AND (1<>1 OR";
                foreach($body->districtId as $key => $districtId){
                    $filterDistrict .= " r.districtId LIKE '%,".$districtId."%' OR r.districtId=".$districtId;
                    if(count($body->districtId) != $key + 1){
                        $filterDistrict .= " OR";
                    }
                }
                $filterDistrict .= ")";
            }
        }

        $countRes = $db->query(
            "SELECT
                    COUNT(r.id) AS count
                 FROM requests r
                 JOIN users u ON u.id = r.owner".
            $where.
            $filterQuarter.
            $filterDistrict.
            ($body->keyword ? " AND (r.comment LIKE '%".$body->keyword."%' OR r.id LIKE '%".$body->keyword."%' OR u.fullname LIKE '%".$body->keyword."%' OR u.phone LIKE '%".$body->keyword."%')" : ""),
            $parameters
        );
        $count = $countRes->fetch();

        $res = $db->query(
            "SELECT r.*,
                    u.id AS userId,
                    u.phone AS userPhone,
                    u.fullname AS userName,
                    u.image AS userImage
                  FROM requests r
                  JOIN users u ON u.id = r.owner" .
            $where.
            $filterQuarter.
            $filterDistrict.
            ($body->keyword ? " AND (r.comment LIKE '%".$body->keyword."%' OR r.id LIKE '%".$body->keyword."%' OR u.fullname LIKE '%".$body->keyword."%' OR u.phone LIKE '%".$body->keyword."%')" : "") .
            " ORDER BY dateUpdate DESC" .
            $pagination,
            $parameters
        );

        if ($res) {
            $requests = [];
            while ($request = $res->fetch()) {
                $districts = "";
                $quarters = "";
                $request['districtId'] = explode(',', $request['districtId']);
                $request['quarterId'] = explode(',', $request['quarterId']);
                $request['metro'] = UNDERGROUND::getName($request['metroId']);
                $request['readUsers'] = $request['readUsers'] ? explode(',', $request['readUsers']) : [];
                $request['own'] = $userId == $request['userId'];
                $request['read'] = $request['own'];
                $request['request_from_owner'] = $request['request_from_owner'] ? true : false;
                foreach($request['readUsers'] as $i => $owner){
                    if($owner == $userId) $request['read'] = true;
                }

                foreach($request['districtId'] as $k => $districtId){
                    $request['districtId'][$k] = $districtId * 1;
                    $districts .= DISTRICT::getName($districtId * 1);
                    if(count($request['districtId']) != $k + 1){
                        $districts .= ", ";
                    }
                }
                $request['districts'] = $districts;

                foreach($request['quarterId'] as $j => $quarterId){
                    $request['quarterId'][$j] = $quarterId * 1;
                    $quarters .= QUARTER::getName($quarterId * 1);
                    if(count($request['$quarterId']) != $j + 1 && $request['quarterId'][$j + 1] != ""){
                        $quarters .= ", ";
                    }
                }
                $request['quarters'] = $quarters;

                array_push($requests, $request);
            }
            $result['count'] = $count['count'];
            $result['items'] = $requests;
            return $result;
        } else return false;
    }

    static public function getRequests() {
        global $db, $req, $fun, $body;
        $parameters = array();
        $pagination = '';
        $filterQuarter = '';
        $filterDistrict = '';
        $where = " WHERE 1=1";

        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }

        if($body->marketType && $body->marketType != 'all'){
            $where .= " AND r.marketType='".$body->marketType."'";
        }

        if($body->type && $body->type != 'all'){
            $where .= " AND r.type='".$body->type."'";
        }

        if($body->metroId){
            $where .= " AND r.metroId=".$body->metroId;
        }

        if($body->regionId){
            $where .= " AND r.regionId=".$body->regionId;
        }

        if($body->request_from_owner) {
            $where .= " AND r.request_from_owner=1";
        } else {
            $where .= " AND r.request_from_owner=0";
        }

        if(count($body->quarterId ? $body->quarterId : []) > 0){
            $filterQuarter = " AND (1<>1 OR";
            foreach($body->quarterId as $key => $quarterId){
                $filterQuarter .= " r.quarterId LIKE '%,".$quarterId."%' OR r.quarterId=".$quarterId;
                if(count($body->quarterId) != $key + 1){
                    $filterQuarter .= " OR";
                }
            }
            $filterQuarter .= ")";
        }
        if(count($body->districtId ? $body->districtId : []) > 0 && $filterQuarter == ''){
            $filterDistrict = " AND (1<>1 OR";
            foreach($body->districtId as $key => $districtId){
                $filterDistrict .= " r.districtId LIKE '%,".$districtId."%' OR r.districtId=".$districtId;
                if(count($body->districtId) != $key + 1){
                    $filterDistrict .= " OR";
                }
            }
            $filterDistrict .= ")";
        }

        $where .= ($req['keyword'] ? " AND (r.comment LIKE '%".$req['keyword']."%' OR r.id LIKE '%".$req['keyword']."%' OR u.fullname LIKE '%".$req['keyword']."%' OR u.phone LIKE '%".$req['keyword']."%')" : "");

        $countRes = $db->query(
            "SELECT
                    COUNT(r.id) AS count
                 FROM requests r
                 JOIN users u ON u.id = r.owner".
            $where.
            $filterQuarter.
            $filterDistrict
        );
        $count = $countRes->fetch();

        $res = $db->query(
            "SELECT r.*,
                    u.id AS userId,
                    u.phone AS userPhone,
                    u.fullname AS userName,
                    u.image AS userImage
                  FROM requests r
                  JOIN users u ON u.id = r.owner" .
            $where.
            $filterQuarter.
            $filterDistrict.
            " ORDER BY dateUpdate DESC" .
            $pagination
        );

        if ($res) {
            $requests = [];
            while ($request = $res->fetch()) {
                array_push($requests, $request);
            }
            $result['count'] = $count['count'];
            $result['items'] = $requests;
            return $result;
        } else return false;
    }

    static public function create($owner) {
        global $db, $req, $body, $files, $const, $fun;
        $time = $fun->time();
        $parameters = array($owner, $time, $time);
        if($body->type) array_push($parameters, $body->type);
        if($body->marketType) array_push($parameters, $body->marketType);
        if($body->ownerPhone) array_push($parameters, $body->ownerPhone);
        if($body->ownerName) array_push($parameters, $body->ownerName);
        if($body->comment) array_push($parameters, $body->comment);
        if($body->regionId) array_push($parameters, $body->regionId);
        if($body->districtId) array_push($parameters, implode(',', $body->districtId));
        if($body->quarterId) array_push($parameters, implode(',', $body->quarterId));
        if($body->metroId) array_push($parameters, $body->metroId);
        if($body->request_from_owner) array_push($parameters, $body->request_from_owner);

        $res = $db->query(
            "INSERT INTO requests SET
                owner=?,
                dateUpdate=?,
                dateCreate=?".
            ($body->type ? ", type=?" : "").
            ($body->marketType ? ", marketType=?" : "").
            ($body->ownerPhone ? ", ownerPhone=?" : "").
            ($body->ownerName ? ", ownerName=?" : "").
            ($body->comment ? ", comment=?" : "").
            ($body->regionId ? ", regionId=?" : "").
            ($body->districtId ? ", districtId=?" : "").
            ($body->quarterId ? ", quarterId=?" : "").
            ($body->metroId ? ", metroId=?" : "").
            ($body->request_from_owner ? ", request_from_owner=?" : ""),
            $parameters
        );
        return $res ? true : false;
    }
    static public function update($clientId, $id) {
        global $db, $req, $files, $const, $fun;
        $time = $fun->time();
        $parameters = array($time);
        if($req['address']) array_push($parameters, $req['address']);
        if($req['estateType']) array_push($parameters, $req['estateType']);
        if($req['objectTimeType']) array_push($parameters, $req['objectTimeType']);
        if($req['room']) array_push($parameters, $req['room']);
        if($req['roomCount']) array_push($parameters, $req['roomCount']);
        if($req['floor']) array_push($parameters, $req['floor']);
        if($req['floorCount']) array_push($parameters, $req['floorCount']);
        if($req['price']) array_push($parameters, $req['price']);
        if($req['areaHome']) array_push($parameters, $req['areaHome']);
        if($req['areaTotal']) array_push($parameters, $req['areaTotal']);
        if($req['maxPage']) array_push($parameters, $req['maxPage']);
        if($req['comment']) array_push($parameters, $req['comment']);
        if($req['buildYear']) array_push($parameters, $req['buildYear']);
        if($req['objectCondition']) array_push($parameters, $req['objectCondition']);
        if($req['buildMaterial']) array_push($parameters, $req['buildMaterial']);
        if($req['buildPlan']) array_push($parameters, $req['buildPlan']);
        if($req['materialType']) array_push($parameters, $req['materialType']);

        if ($files['image']){
            $filename = $time.'.'.pathinfo($files['image']['name'], PATHINFO_EXTENSION);
            if(move_uploaded_file($files['image']['tmp_name'], '../file/'.$filename)){
                self::removeImage($id,'image');
                array_push($parameters, $filename);
            }
            else new Errors($const['fileNotUploaded']);
        }
        array_push($parameters, $id);

        $res = $db->query(
            "UPDATE requests SET date_update=?" .
            ($req['address'] ? ", address=?" : "").
            ($req['estateType'] ? ", estate_type=?" : "").
            ($req['objectTimeType'] ? ", object_time_type=?" : "").
            ($req['room'] ? ", room=?" : "").
            ($req['roomCount'] ? ", room_count=?" : "").
            ($req['floor'] ? ", floor=?" : "").
            ($req['floorCount'] ? ", floor_count=?" : "").
            ($req['price'] ? ", price=?" : "").
            ($req['areaHome'] ? ", area_home=?" : "").
            ($req['areaTotal'] ? ", area_total=?" : "").
            ($req['maxPage'] ? ", max_page=?" : "").
            ($req['comment'] ? ", comment=?" : "").
            ($req['buildYear'] ? ", build_year=?" : "").
            ($req['objectCondition'] ? ", object_condition=?" : "").
            ($req['buildMaterial'] ? ", build_material=?" : "").
            ($req['buildPlan'] ? ", build_plan=?" : "").
            ($req['materialType'] ? ", object_material_type=?" : "").
            ($files['image'] ? ", image=?" : "").
            " WHERE id=?",
            $parameters
        );
        return $res ? true : false;
    }

    static public function delete($id, $owner = null) {
        global $db;
        $res = $db->query("DELETE FROM requests WHERE id=? AND owner=?", [$id, $owner]);
        return $res ? true : false;
    }

    static public function removeImage($id, $image) {
        global $db;
        $res = $db->query("SELECT * FROM requests WHERE id=?", [$id]);
        $file = $res->fetch();
        unlink('file/'. $file[$image]);
    }

}
