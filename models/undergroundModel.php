<?php
class UNDERGROUND {
    
    static public function create($name) {
        global $db;
        $res = $db->query(
            "INSERT INTO undergrounds SET
                `name`=?",
            [$name]
        );
        if($res) {
            $r = $db->query(
                "SELECT u.id
            FROM undergrounds u
            ORDER BY id DESC LIMIT 0, 1"
            );
            $underground = $r->fetch();
            return $underground['id'];
        }
        else return false;
    }
    
    static public function getAll() {
        global $db, $fun, $req;
        $where = " WHERE 1=1";
        $pagination = "";
        
        if($req['keyword']){
            $where .= " AND u.name LIKE '%".$req['keyword']."%'";
        }
        if($req['limit'] && $req['page']) {
            $offset = $fun->createOffset($req['limit'], $req['page']);
            $pagination = ' LIMIT '.$offset.', '.$req['limit'];
        }
        $resCount = $db->query("SELECT COUNT(id) as count FROM undergrounds u".$where);
        $count = $resCount->fetch();
        $res = $db->query("SELECT * FROM undergrounds u".$where.$pagination);
        $result = [
            "count" => $count['count'],
            "items" => $res->fetchAll()
        ];
        return $result;
    }
    
    static public function get($id) {
        global $db;
        $res = $db->query(
            "SELECT *
            FROM undergrounds u
            WHERE u.id=?",
            [$id]
        );
        $region = $res->fetch();
        if($region) {
            return $region;
        }
        return false;
    }
    
    static public function delete($id) {
        global $db;
        $res = $db->query(
            "DELETE FROM undergrounds
            WHERE id=?",
            [$id]
        );
        return $res ? true : false;
    }
    
    static public function update($id, $name) {
        global $db;
        $res = $db->query(
            "UPDATE undergrounds SET name=?
            WHERE id=?",
            [$name, $id]
        );
        return $res ? true : false;
    }

    static public function getName($id) {
        global $db;
        $res = $db->query(
            "SELECT u.name
            FROM undergrounds u
            WHERE u.id=?",
            [$id]
        );
        $underground = $res->fetch();
        if($underground) {
            return $underground['name'];
        }
        else return null;
    }

}
