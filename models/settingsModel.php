<?php
class SETTING {

    static public function get() {
        global $db;
        $res = $db->query(
            "SELECT s.*
            FROM settings s
            WHERE s.id=?",
            [1]
        );
        $settings = $res->fetch();
        if($settings) return $settings;
        return 1;
    }

    static public function update($last_version, $max_login_count) {
        global $db;
        $res = $db->query(
            "UPDATE settings SET last_version=?, max_login_count=?",
            [$last_version, $max_login_count]
        );
        return $res ? true : false;
    }

}
